///
/// Microsoft Public License (Ms-PL) - Copyright (c) 2022 Vega Authors
/// This file is subject to the terms and conditions of the Microsoft Public License, the text of which can be found in 
/// the 'LICENSE' file at the root of this repository, or online at <https://opensource.org/licenses/MS-PL>.
///

parser grammar VSL;
options {
    tokenVocab=VSLLexer;
}


/////
// Top-level file unit
file
    : shaderTypeStatement shaderTopLevelStatement* EOF
    ;

// Shader type statement
shaderTypeStatement
    : '@shader' type=IDENTIFIER ';'
    ;

// Shader top level statements
shaderTopLevelStatement
    : structDefinition
    | blendStateDefinition
    | uniformDeclaration
    | pushConstantDeclaration
    | bindingDeclaration
    | pixelInputDeclaration
    | vertInputDeclaration
    | fragOutputDeclaration
    | localDeclaration
    | shaderStageFunction
    ;

// Shader stage function
shaderStageFunction
    : stage=('@vert' | '@frag') statementBlock
    ;

// Struct type definition
structDefinition
    : '@struct' name=IDENTIFIER '{' (fields+=variableDeclaration ';')+ '}' ';'
    ;

// Blending state definition
blendStateDefinition
    : '@blend' name=IDENTIFIER '{' 
        srcc=IDENTIFIER cop=IDENTIFIER dstc=IDENTIFIER ',' 
        srca=IDENTIFIER aop=IDENTIFIER dsta=IDENTIFIER 
      '}' ';'
    ;

// Shader uniform declaration
uniformDeclaration
    : 'uniform' variableDeclaration ';'
    ;

// Shader push constant declaration
pushConstantDeclaration
    : 'push' variableDeclaration ';'
    ;

// Shader resource binding
bindingDeclaration
    : 'bind' '(' slot=IDENTIFIER ')' KW_CONST? variableDeclaration ';'
    ;

// Vertex input declaration
vertInputDeclaration
    : 'in' '(' semantic=VERTEX_SEMANTIC ')' variableDeclaration ';'
    ;

// Fragment output declaration
fragOutputDeclaration
    : 'out' '(' index=INTEGER_LITERAL (',' blend=IDENTIFIER)? ')' variableDeclaration ';'
    ;

// Pixel input declaration
pixelInputDeclaration
    : 'pixelInput' '(' index=INTEGER_LITERAL ')' variableDeclaration ';'
    ;

// Inter-stage local declaration
localDeclaration
    : 'local' 'flat'? variableDeclaration ';'
    ;


/////
// Statements
statement
    : variableDeclaration ';'
    | variableDefinition ';'
    | variableAssignment ';'
    | ifStatement
    ;
statementBlock 
    : '{' statement* '}'
    ;

// Variable declaration
variableDeclaration
    : baseType=IDENTIFIER ('<' typeArg=IDENTIFIER '>')? ('[' arraySize=INTEGER_LITERAL ']')* name=IDENTIFIER 
    ;

// Variable definition
variableDefinition
    : 'const'? variableDeclaration '=' expression
    ;

// Variable assignment
variableAssignment
    : lvalue op=('='|'+='|'&='|'|='|'^='|'/='|'<<='|'%='|'*='|'>>='|'-=') expression
    ;
lvalue
    : '(' lvalue ')'                   #GroupLValue
    | lvalue '.' member=IDENTIFIER     #MemberLValue
    | lvalue '[' index=expression ']'  #IndexLValue
    | name=IDENTIFIER                  #NameLValue
    ;

// If/elif/else blocks
ifStatement 
    : 'if' '(' cond=expression ')' statementBlock elifStatement* elseStatement?
    ;
elifStatement 
    : 'elif' '(' cond=expression ')' statementBlock
    ;
elseStatement
    : 'else' statementBlock
    ;


/////
// Expressions
expression
    : atom #AtomExpr
    // Unary Operators
    | op=('-'|'!'|'~') val=expression  #NegateExpr
    // Binary Operators (enforces order of precedence)
    | left=expression op=('*'|'/'|'%')       right=expression  #MulDivModExpr
    | left=expression op=('+'|'-')           right=expression  #AddSubExpr
    | left=expression op=('<<'|'>>')         right=expression  #ShiftExpr
    | left=expression op=('<'|'>'|'<='|'>=') right=expression  #RelationExpr
    | left=expression op=('=='|'!=')         right=expression  #EqualityExpr
    | left=expression op=('&'|'|'|'^')       right=expression  #BitwiseExpr
    | left=expression op=('&&'|'||'|'^^')    right=expression  #LogicalExpr
    // Ternary Operator
    | cond=expression '?' texpr=expression ':' fexpr=expression  #TernaryExpr
    ;

// Atom - smallest atomic expression type
atom
    : '(' expression ')'             #GroupAtom
    | atom '[' index=expression ']'  #IndexAtom
    | atom '.' IDENTIFIER            #MemberAtom
    | functionCall                   #CallAtom
    | scalarLiteral                  #LiteralAtom
    | IDENTIFIER                     #NameAtom
    ;

// Function or constructor call
functionCall
    : name=IDENTIFIER '(' args+=functionArgument (',' args+=functionArgument)* ')'
    ;
functionArgument
    : ref=('in' | 'inout' | 'out')? expression
    ;

// Scalar literal (number or bool)
scalarLiteral
    : INTEGER_LITERAL
    | FLOAT_LITERAL
    | BOOLEAN_LITERAL
    ;
