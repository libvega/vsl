#!/bin/sh
#
# Microsoft Public License (Ms-PL) - Copyright (c) 2022 Vega Authors
# This file is subject to the terms and conditions of the Microsoft Public License, the text of which can be found in 
# the 'LICENSE' file at the root of this repository, or online at <https://opensource.org/licenses/MS-PL>.
#

# VSL ANTLR syntax parser/lexer generator script for *nix.

java                                \
    -jar ./antlr-4.9.3-complete.jar \
    -no-listener                    \
    -visitor                        \
    -o ../VSL/Grammar               \
    -package VSL.Grammar            \
    -Xexact-output-dir              \
    -Dlanguage=CSharp               \
    VSLLexer.g4

java                                \
    -jar ./antlr-4.9.3-complete.jar \
    -no-listener                    \
    -visitor                        \
    -o ../VSL/Grammar               \
    -package VSL.Grammar            \
    -Xexact-output-dir              \
    -Dlanguage=CSharp               \
    VSL.g4
