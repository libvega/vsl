# Vega Shader Language

**Archived**

This repo has been merged into [VSL](https://gitlab.com/libvega/vega). All current and future development is done there.

## Legal

VSL is licensed under the [Microsoft Public License (Ms-PL)](https://choosealicense.com/licenses/ms-pl/). This is a permissive OSS license close to the MIT license, but with an extra clause that modifications to the Vega source (*if also made open source*) need to be under a license compatible with the Ms-PL.

All third-party libraries and projects are utilized within their original licenses, and, where applicible, are rehosted under the original licenses as well. These licenses are availble in at the links above, and are additionally available in the `Licenses/` folder in this repo. These third party libraries belong solely to their original authors - the VSL authors make no ownership claims over them.
