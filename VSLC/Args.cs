﻿/*
 * Microsoft Public License (Ms-PL) - Copyright (c) 2022 Vega Authors
 * This file is subject to the terms and conditions of the Microsoft Public License, the text of which can be found in
 * the 'LICENSE' file at the root of this repository, or online at <https://opensource.org/licenses/MS-PL>.
 */

using System;
using System.Collections.Generic;
using System.Linq;
using VSL;


// Handles command line parsing and help text prints
static class Args
{
	#region Fields
	// User requested help text (top priority)
	public static bool Help { get; private set; } = false;

	// User requested check for Vulkan SDK and ShaderC (top priority under Help)
	public static bool SDKCheck { get; private set; } = false;

	// If the compiler should run in debug mode (creates debug messages and intermediate compiler outputs)
	public static bool Debug { get; private set; } = false;

	// If the app is loading and checking compiled files
	public static bool Load { get; private set; } = false;

	// List of input shader files
	public static IReadOnlyList<string> InputFiles => _InputFiles;
	private static readonly List<string> _InputFiles = new();
	#endregion // Fields

	public static bool Parse(string[] rawargs)
	{
		// Normalize flags to be all lower case and start with '-'
		static string normalize_(string arg)
		{
			var isparam = (arg[0] == '-');
			var iswide = (arg.Length > 1) && (arg[1] == '-');
			if ((isparam && arg.Length == 1) || (iswide && arg.Length == 2)) {
				return String.Empty;
			}
			return isparam ? $"-{arg.Substring(iswide ? 2 : 1).ToLowerInvariant()}" : arg;
		}

		// Normalize arguments
		var args = rawargs.Select(normalize_).Where(s => !string.IsNullOrWhiteSpace(s)).ToArray();
		if (args.Length == 0) {
			return true;
		}

		// Check for help and check flags, return immediately
		if (args.Contains("-h") || args.Contains("-help") || args.Contains("-?")) {
			Help = true;
			return true;
		}
		if (args.Contains("-sdk")) {
			SDKCheck = true;
			return true;
		}

		// Loop over the arguments and perform logic
		for (uint ai = 0; ai < args.Length; ++ai) {
			var arg = args[ai];
			var isLast = (ai == (args.Length - 1));

			if (arg == "-d" || arg == "-debug") {
				Debug = true;
			}
			else if (arg == "-l" || arg == "-load") {
				Load = true;
			}
			else if (arg[0] == '-') {
				// Ignore unknown parameters/flags
			}
			else {
				// Anything not associated with a parameter/flag is assumed to be an input file
				_InputFiles.Add(arg);
			}
		}

		return true;
	}

	public static void PrintHelp()
	{
		Console.WriteLine();
		Console.WriteLine(
			"Usage: vslc.exe [args] <input_files>\n" +
			"<input_files> is one or more .vsl shader source files to compile\n" +
			"[args] can be specified with `-` or `--`, and is one or more of:\n" +
			"  h;help;?              - Print this help text and exit.\n" +
			"  sdk                   - Check that the Vulkan SDK is installed\n" +
			"                          and ShaderC is loaded, then exit.\n" +
			"  d;debug               - Run the compiler in debug mode (used\n" +
			"                          mainly for compiler development)\n" +
			"  l;load                - Loads the given compiled VBC file (used\n" +
			"                          to check the file and give a report)"
		);
		Console.WriteLine();
	}

	public static void PrintSDKCheck()
	{
		var sdkFound = ShaderC.VulkanSDKFound;
		var isLoaded = ShaderC.IsLoaded;
		Console.WriteLine($"Vulkan SDK: {(sdkFound ? "" : "Not ")}Found   ShaderC: {(isLoaded ? "" : "Not ")}Loaded");
	}
}
