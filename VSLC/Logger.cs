﻿/*
 * Microsoft Public License (Ms-PL) - Copyright (c) 2022 Vega Authors
 * This file is subject to the terms and conditions of the Microsoft Public License, the text of which can be found in
 * the 'LICENSE' file at the root of this repository, or online at <https://opensource.org/licenses/MS-PL>.
 */

using System;


// Color-supporting logger functions
static class Logger
{
	private static readonly ConsoleColor DefaultColor;

	// Debug log
	public static void LogDebug(string msg)
	{
		Console.ForegroundColor = ConsoleColor.DarkGray;
		Console.WriteLine(msg);
		Console.ForegroundColor = DefaultColor;
	}

	// Info log
	public static void LogInfo(string msg)
	{
		Console.WriteLine(msg);
	}

	// Warning log
	public static void LogWarn(string msg)
	{
		Console.ForegroundColor = ConsoleColor.DarkYellow;
		Console.WriteLine(msg);
		Console.ForegroundColor = DefaultColor;
	}

	// Error log
	public static void LogError(string msg)
	{
		Console.ForegroundColor = ConsoleColor.Red;
		Console.Error.WriteLine(msg);
		Console.ForegroundColor = DefaultColor;
	}

	static Logger()
	{
		DefaultColor = Console.ForegroundColor;
	}
}
