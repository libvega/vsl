﻿/*
 * Microsoft Public License (Ms-PL) - Copyright (c) 2022 Vega Authors
 * This file is subject to the terms and conditions of the Microsoft Public License, the text of which can be found in
 * the 'LICENSE' file at the root of this repository, or online at <https://opensource.org/licenses/MS-PL>.
 */

global using static Logger;
using System;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using VSL;

static class Program
{
	#region Fields
	// The output path of the shader currently being processed
	private static string? _CurrentOutputPath = null;
	#endregion // Fields

	static int Main(string[] args)
	{
		// Parse args and print help/check if requested
		if (!Args.Parse(args)) {
			return -1;
		}
		if (Args.Help) {
			Args.PrintHelp();
			return 0;
		}
		if (Args.SDKCheck) {
			Args.PrintSDKCheck();
			return 0;
		}

		// Check that the Vulkan SDK and ShaderC are available
		if (!ShaderC.IsLoaded) {
			if (ShaderC.VulkanSDKFound) {
				LogError("Error: Vulkan SDK is installed, but ShaderC cannot be loaded");
			}
			else {
				LogError("Error: Vulkan SDK must be installed to run vslc");
			}
			return -2;
		}

		// Check for at least one input file
		if (Args.InputFiles.Count == 0) {
			LogError("Error: At least one input file must be specified.");
			return -3;
		}

		// Run in load mode if requested
		if (Args.Load) {
			foreach (var inputFile in Args.InputFiles) {
				// Try to load the shader
				var inputPath = inputFile;
				if (!Path.IsPathRooted(inputPath)) {
					inputPath = Path.Combine(Directory.GetCurrentDirectory(), inputPath);
				}
				LogInfo($"Shader bytecode file '{inputPath}'");
				if (!ShaderProgram.TryLoadFromFile(inputPath, out var program, out var version, out var err)) {
					LogError($"Failed to load - {err}");
					continue;
				}

				// Report the shader
				ReportShader(program, version);
			}

			return 0;
		}

		// Create the compiler object and iterate over the input files
		if (Args.Debug) {
			LogDebug("Running with debug mode enabled");
		}
		var compiler = new ShaderCompiler();
		compiler.GLSLEmit = GLSLCallback;
		compiler.SPIRVEmit = SPIRVCallback;
		foreach (var inputFile in Args.InputFiles) {
			// Calculate the full input and output paths
			var inputPath = inputFile;
			if (!Path.IsPathRooted(inputPath)) {
				inputPath = Path.Combine(Directory.GetCurrentDirectory(), inputPath);
			}
			var outputPath = Path.ChangeExtension(inputPath, "vbc"); // vbc = "vega bytecode"
			if (inputPath == outputPath) {
				LogError($"Error: Input file '{inputPath}' has the same name as its output file");
				return -4;
			}
			_CurrentOutputPath = outputPath;

			// Try to compile the input file
			var result = compiler.CompileFile(inputPath);
			if (result is CompileResult.Failure fail) {
				LogError($"Compilation Failed: {fail.Description}");
				return -5;
			}

			// Write the shader to the output file
			var success = result as CompileResult.Success;
			if (!success!.Program.TrySaveToFile(outputPath, out var writeError)) {
				LogError($"Shader File Write Failed: {writeError}");
				return -6;
			}
		}
		_CurrentOutputPath = null;

		// Success
		return 0;
	}

	static void GLSLCallback(ShaderCompiler compiler, string? fileName, ShaderStage stage, string glsl)
	{
		if (Args.Debug) {
			var outPath = Path.ChangeExtension(_CurrentOutputPath, $".{stage.GetStageCode()}.glsl");
			try {
				File.WriteAllText(outPath!, glsl, Encoding.UTF8);
			}
			catch (Exception ex) {
				LogWarn($"Could not save intermediate GLSL file - {ex}");
			}
		}
	}

	static void SPIRVCallback(ShaderCompiler compiler, string? fileName, ShaderStage stage, in ReadOnlySpan<uint> spirv)
	{
		if (Args.Debug) {
			var outPath = Path.ChangeExtension(_CurrentOutputPath, $".{stage.GetStageCode()}.spirv");
			try {
				using var writer = File.Open(outPath!, FileMode.Create, FileAccess.Write, FileShare.Read);
				writer.Write(MemoryMarshal.Cast<uint, byte>(spirv));
			}
			catch (Exception ex) {
				LogWarn($"Cound not save intermediate SPIRV file - {ex}");
			}
		}
	}

	static void ReportShader(ShaderProgram shader, byte version)
	{
		LogInfo($"  File Version:  {version}");
		LogInfo($"  Stage Mask:    {String.Join('|', shader.StageMask.EnumerateStages())}");
		LogInfo($"  Uniform:       {(shader.UniformInfo is not null ? $"{shader.UniformInfo.Size} bytes" : "None")}");
		LogInfo($"  Push Constant: " +
			$"{(shader.PushConstantInfo is not null ? $"{shader.PushConstantInfo.Size} bytes" : "None")}");

		{
			LogInfo($"  Bindings:      {shader.Bindings.Count}");
			var maxNameLen = shader.Bindings.Max(static b => b.Name.Length);
			foreach (var bind in shader.Bindings) {
				string spacing = new(' ', maxNameLen - bind.Name.Length + 1);
				LogInfo($"    [{bind.Slot}] {bind.Name}:{spacing}{bind.Type}");
			}
		}

		if (shader is ShaderProgram.Graphics graphics) {
			LogInfo($"  Inputs:        {graphics.Inputs.Count}");
			var maxNameLen = graphics.Inputs.Max(static i => i.Use.ToString().Length);
			foreach (var inp in graphics.Inputs) {
				string spacing = new(' ', maxNameLen - inp.Use.ToString().Length + 1);
				LogInfo($"    [#{inp.Use}]{spacing}{inp.Type}");
			}

			LogInfo($"  Outputs:       {graphics.Outputs.Count}");
			foreach (var outp in graphics.Outputs) {
				LogInfo($"    [{outp.Index}] {outp.Type}");
			}

			LogInfo($"  PixelInputs:   {graphics.PixelInputs.Count}");
			foreach (var inp in graphics.PixelInputs) {
				LogInfo($"    [{inp.Index}] {inp.Type}");
			}
		}
		else {
			LogError("Unknown shader kind");
		}
	}
}
