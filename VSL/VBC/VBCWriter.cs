﻿/*
 * Microsoft Public License (Ms-PL) - Copyright (c) 2022 Vega Authors
 * This file is subject to the terms and conditions of the Microsoft Public License, the text of which can be found in
 * the 'LICENSE' file at the root of this repository, or online at <https://opensource.org/licenses/MS-PL>.
 */

using System;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;

namespace VSL;


// Writer for the most recent version of VBC files
internal sealed class VBCWriter : IDisposable
{
	// Current writer version
	public const byte VERSION = 1;


	public VBCWriter() { }
	~VBCWriter()
	{
		dispose(false);
	}

	// Writes the shader to the given path
	public void WriteShader(ShaderProgram shader, string path)
	{
		// Try to open the file for writing
		using var writer = new BinaryWriter(File.Open(path, FileMode.Create, FileAccess.Write, FileShare.Read));

		// Write the common header to all versions
		writer.Write((byte)'V');
		writer.Write((byte)'B');
		writer.Write((byte)'C');
		writer.Write(VERSION);

		// Write the stage mask (2 bytes)
		writer.Write((ushort)(shader.StageMask.Value & 0xFFFF));

		// Write the uniform and push constant information (2 bytes each, the struct/numeric type sizes)
		writer.Write((ushort)(shader.UniformInfo?.Size ?? 0));
		writer.Write((ushort)(shader.PushConstantInfo?.Size ?? 0));

		// Write the bindings, prepended by the count (1 + (3 + s + ??) bytes each)
		// Each binding has:
		//   - 1 byte binding slot index
		//   - 1 byte base type
		//   - 1 byte name length (in bytes)
		//   - s byte name string (utf8)
		//   - One of the following, based on types:
		//     - Sampler: rank (1 byte), texel type (1 byte)
		//     - Image: rank (1 byte), texel format (1 byte)
		//     - Buffer: readonly (1 byte 0/1), data type size (2 bytes)
		//     - TexelBuffer: readonly (1 byte 0/1), texel format (1 byte)
		writer.Write((byte)shader.Bindings.Count);
		Span<byte> nameBytes = stackalloc byte[(int)ShaderProgram.MAX_NAME_LENGTH * 4];
		foreach (var binding in shader.Bindings) {
			writer.Write((byte)binding.Slot);
			writer.Write((byte)(binding.Type switch {
				ShaderType.Sampler     => VBCReader.BindingKind.Sampler,
				ShaderType.Image       => VBCReader.BindingKind.Image,
				ShaderType.TexelBuffer => VBCReader.BindingKind.TexelBuffer,
				ShaderType.Buffer      => VBCReader.BindingKind.Buffer,
				_ => throw new NotImplementedException()
			}));
			var len = Encoding.UTF8.GetBytes(binding.Name.AsSpan(), nameBytes);
			writer.Write((byte)len);
			writer.Write(nameBytes.Slice(0, len));

			if (binding.Type is ShaderType.Sampler sampType) {
				writer.Write((byte)sampType.Rank);
				writer.Write((byte)sampType.Format);
			}
			else if (binding.Type is ShaderType.Image imType) {
				writer.Write((byte)imType.Rank);
				writer.Write((byte)imType.Format);
			}
			else if (binding.Type is ShaderType.TexelBuffer tbufType) {
				writer.Write((byte)(tbufType.ReadOnly ? 1 : 0));
				writer.Write((byte)tbufType.Format);
			}
			else if (binding.Type is ShaderType.Buffer bufType) {
				writer.Write((byte)(bufType.ReadOnly ? 1 : 0));
				writer.Write((ushort)bufType.ElementType.Size);
			}
			else {
				throw new NotImplementedException("Unhandled binding type");
			}
		}

		// Switch on the shader type (only graphics supported currently)
		if (shader is ShaderProgram.Graphics graphics) {
			// Write the inputs, prepended by the input semantic mask (4 + 5 bytes each)
			// Input order is given by the value of the semantic use
			// Each input is ordered as (slot (1), base type (1), row count (1), col count (1), array size (1))
			writer.Write(graphics.InputMask.Value);
			foreach (var input in graphics.Inputs.OrderBy(i => (uint)i.Use)) {
				var arrType = input.Type as ShaderType.Array;
				var numType = (arrType?.BaseElementType ?? input.Type) as ShaderType.Numeric;
				writer.Write((byte)input.Slot);
				writer.Write((byte)numType!.Type);
				writer.Write((byte)numType.Rows);
				writer.Write((byte)numType.Cols);
				writer.Write((byte)(arrType?.FullLength ?? 1)); // Any input array is max MAX_VERTEX_SLOTS < 256
			}

			// Write the outputs, prepended by the output count (1 + 8 bytes each)
			// Order is given by the output index
			// Each output is ordered as (base type (1 byte), vector size (1 byte), default blend (6 bytes))
			// Each default blend is ordered as (srcc, dstc, cop, srca, dsta, aop (1 byte each))
			writer.Write((byte)graphics.Outputs.Count);
			foreach (var output in graphics.Outputs) {
				writer.Write((byte)output.Type.Type);
				writer.Write((byte)output.Type.Rows);
				writer.Write((byte)output.Blend.SrcColor);
				writer.Write((byte)output.Blend.DstColor);
				writer.Write((byte)output.Blend.ColorOp);
				writer.Write((byte)output.Blend.SrcAlpha);
				writer.Write((byte)output.Blend.DstAlpha);
				writer.Write((byte)output.Blend.AlphaOp);
			}

			// Write the pixel inputs, prepended by the count (1 + 1 bytes each)
			// Each pixel input is only the base type (all other info is constant/known)
			writer.Write((byte)graphics.PixelInputs.Count);
			foreach (var output in graphics.PixelInputs.OrderBy(si => si.Index)) {
				writer.Write((byte)output.Type);
			}
		}
		else {
			throw new NotImplementedException($"Unhandled shader program type {shader.Kind}");
		}

		// Write each bytecode, prepended by the bytecode length (in instructions) (4 + N bytes each)
		// Order is given by the value of the stage enum, in ascending order
		foreach (var pair in shader.Bytecode.OrderBy(p => (uint)p.Key)) {
			writer.Write((uint)pair.Value.Length);
			writer.Write(MemoryMarshal.Cast<uint, byte>(pair.Value.AsSpan()));
		}

		writer.Flush();
	}

	#region IDisposable
	public void Dispose()
	{
		dispose(true);
		GC.SuppressFinalize(this);
	}

	private void dispose(bool disposing)
	{
		
	}
	#endregion // IDisposable
}
