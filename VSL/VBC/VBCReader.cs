﻿/*
 * Microsoft Public License (Ms-PL) - Copyright (c) 2022 Vega Authors
 * This file is subject to the terms and conditions of the Microsoft Public License, the text of which can be found in
 * the 'LICENSE' file at the root of this repository, or online at <https://opensource.org/licenses/MS-PL>.
 */

using System;
using System.Collections.Generic;
using System.IO;
using System.Text;

namespace VSL;

internal abstract partial class VBCReader : IDisposable
{
	// The registry of reader types
	private static readonly Dictionary<byte, Func<byte, VBCReader>> _ReaderFactories = new();

	#region Fields
	// The file version for the reader
	public readonly byte Version;
	#endregion // Fields

	protected VBCReader(byte version)
	{
		Version = version;
	}
	~VBCReader()
	{
		dispose(false);
	}

	// Override to implement the read logic
	protected abstract ShaderProgram readShader(BinaryReader reader);

	#region IDisposable
	public void Dispose()
	{
		dispose(true);
		GC.SuppressFinalize(this);
	}

	private void dispose(bool disposing)
	{
		onDispose(disposing);
	}

	protected virtual void onDispose(bool disposing) { }
	#endregion // IDisposable

	// Reads the stream for version selection, then loads the shader
	public static (ShaderProgram Program, byte Version) LoadShader(Stream inStream)
	{
		// Open the reader
		using var reader = new BinaryReader(inStream, Encoding.UTF8, true);

		// Load and validate the common header
		Span<byte> header = stackalloc byte[4];
		if (reader.Read(header) != header.Length) {
			throw new Exception("File unexpectedly ended - magic number too short");
		}
		if (header[0] != (byte)'V' || header[1] != (byte)'B' || header[2] != (byte)'C') {
			throw new Exception("Magic number invalid - file is not VSL bytecode");
		}
		var version = header[3];

		// Lookup the reader to use
		if (!_ReaderFactories.TryGetValue(version, out var func)) {
			throw new Exception($"Unsupported shader file version {version}, use a newer VSL loader version");
		}
		using var vbcReader = func(version);

		// Call the reader
		return (vbcReader.readShader(reader), version);
	}

	static VBCReader()
	{
		_ReaderFactories.Add(1, (version) => new V1());
	}


	// Special internal ShaderType.DataType which just represents an unstructured sized block
	private sealed record SizedBlock(uint BlockSize) : ShaderType.DataType
	{
		public override uint Alignment => throw new NotImplementedException();
		public override uint SlotCount => throw new NotImplementedException();
		public override string Name => $"Size={Size}";
		public override uint Size => BlockSize;
		internal override string GLName => throw new NotImplementedException();
		internal override bool CanAssignTo(ShaderType target) => throw new NotImplementedException();
	}
}
