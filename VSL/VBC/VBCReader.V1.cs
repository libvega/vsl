﻿/*
 * Microsoft Public License (Ms-PL) - Copyright (c) 2022 Vega Authors
 * This file is subject to the terms and conditions of the Microsoft Public License, the text of which can be found in
 * the 'LICENSE' file at the root of this repository, or online at <https://opensource.org/licenses/MS-PL>.
 */

using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;

namespace VSL;

internal abstract partial class VBCReader
{
	public enum BindingKind : byte { Sampler = 0, Image, TexelBuffer, Buffer }

	// Implements the read logic for file V1
	private sealed class V1 : VBCReader
	{
		public V1() : base(1) { }

		protected override ShaderProgram readShader(BinaryReader reader)
		{
			// Read the stage mask
			ShaderStageMask stageMask = new(reader.ReadUInt16());
			ShaderKind kind = (stageMask & ShaderStageMask.AllGraphics).Any
				? ShaderKind.Graphics
				: throw new Exception($"Unsupported shader staged in mask {stageMask.Value:X8}");

			// Read the uniform and pc sizes
			var uniformSize = reader.ReadUInt16();
			var pcSize = reader.ReadUInt16();

			// Read the bindings
			var bindCount = reader.ReadByte();
			Span<byte> nameBytes = stackalloc byte[(int)ShaderProgram.MAX_NAME_LENGTH * 4];
			List<ShaderProgram.Binding> bindings = new(bindCount);
			for (uint bi = 0; bi < bindCount; ++bi) {
				// Read common binding info
				var slot = reader.ReadByte();
				var bindType = (BindingKind)reader.ReadByte();
				var nameLen = reader.ReadByte();
				reader.Read(nameBytes.Slice(0, nameLen));
				var name = Encoding.UTF8.GetString(nameBytes.Slice(0, nameLen));

				// Read type-specific binding info
				ShaderType.Binding bindingType;
				if (bindType == BindingKind.Sampler) {
					var rank = (TexelRank)reader.ReadByte();
					var format = (TexelFormat)reader.ReadByte();
					bindingType = ShaderType.Sampler.Get(rank, format)!;
					if (bindingType is null) {
						throw new Exception($"Invalid sampler type (rank={rank}, format={format})");
					}
				}
				else if (bindType == BindingKind.Image) {
					var rank = (TexelRank)reader.ReadByte();
					var format = (TexelFormat)reader.ReadByte();
					if (!Enum.IsDefined(rank) || !Enum.IsDefined(format)) {
						throw new Exception($"Invalid image type (rank={rank}, format={format})");
					}
					bindingType = new ShaderType.Image(rank, format);
				}
				else if (bindType == BindingKind.TexelBuffer) {
					var ro = reader.ReadByte() != 0;
					var format = (TexelFormat)reader.ReadByte();
					bindingType = new ShaderType.TexelBuffer(format, ro);
				}
				else if (bindType == BindingKind.Buffer) {
					var ro = reader.ReadByte() != 0;
					var typeSize = reader.ReadUInt16();
					bindingType = new ShaderType.Buffer(new SizedBlock(typeSize), ro);
				}
				else {
					throw new Exception($"Invalid binding kind value '{(byte)bindType}'");
				}

				// Add the binding
				bindings.Add(new(name, bindingType, slot));
			}

			// Graphics-specific parsing
			if (kind == ShaderKind.Graphics) {
				// Read the inputs
				VertexUseMask inputMask = new(reader.ReadUInt32());
				var inUses = inputMask.EnumerateUses().ToArray();
				List<ShaderProgram.Graphics.Input> inputs = new((int)inputMask.Count);
				for (uint ii = 0; ii < inputMask.Count; ++ii) {
					var slot = reader.ReadByte();
					var numType = (ShaderType.NumericType)reader.ReadByte();
					var rows = reader.ReadByte();
					var cols = reader.ReadByte();
					var arrSz = reader.ReadByte();

					var inNumType = ShaderType.Numeric.Get(numType, rows, cols);
					if (inNumType is null) {
						throw new Exception($"Invalid input numeric type ({numType}, {rows}x{cols})");
					}
					if (arrSz == 1) {
						inputs.Add(new(inNumType, inUses[ii], slot));
					}
					else {
						ShaderType.Array arrType = new(inNumType, arrSz);
						inputs.Add(new(arrType, inUses[ii], slot));
					}
				}

				// Read the outputs
				var outCount = reader.ReadByte();
				List<ShaderProgram.Graphics.Output> outputs = new(outCount);
				for (uint oi = 0; oi < outCount; ++oi) {
					var numType = (ShaderType.NumericType)reader.ReadByte();
					var rows = reader.ReadByte();
					var sc = (BlendState.Factor)reader.ReadByte();
					var dc = (BlendState.Factor)reader.ReadByte();
					var co = (BlendState.Op)reader.ReadByte();
					var sa = (BlendState.Factor)reader.ReadByte();
					var da = (BlendState.Factor)reader.ReadByte();
					var ao = (BlendState.Op)reader.ReadByte();

					var outNumType = ShaderType.Numeric.Get(numType, rows, 1);
					if (outNumType is null) {
						throw new Exception($"Invalid output numeric type ({numType}, {rows}x1)");
					}
					outputs.Add(new(outNumType, oi, new(sc, dc, co, sa, da, ao)));
				}

				// Read the pixel inputs
				var pixCount = reader.ReadByte();
				List<ShaderProgram.Graphics.PixelInput> pixInputs = new(pixCount);
				for (uint si = 0; si < pixCount; ++si) {
					var numType = (ShaderType.NumericType)reader.ReadByte();
					pixInputs.Add(new(numType, si));
				}

				// Create the shader
				return new ShaderProgram.Graphics(
					(uniformSize > 0) ? new ShaderProgram.Uniform(uniformSize) : null,
					(pcSize > 0) ? new ShaderProgram.PushConstant(pcSize) : null,
					bindings,
					inputs,
					outputs,
					pixInputs,
					ReadBytecodes(stageMask, reader)
				);
			}
			else {
				throw new NotImplementedException($"Shader loading not implemented for kind '{kind}'");
			}
		}

		private static Dictionary<ShaderStage, uint[]> ReadBytecodes(ShaderStageMask mask, BinaryReader reader)
		{
			Dictionary<ShaderStage, uint[]> codes = new();
			foreach (var stage in mask.EnumerateStages()) {
				var codeLen = reader.ReadUInt32();
				var code = new uint[codeLen];
				reader.Read(MemoryMarshal.Cast<uint, byte>(code.AsSpan()));
				codes[stage] = code;
			}

			// Validate
			if (codes.Count == 0) {
				throw new Exception("No bytecode was found in the shader file");
			}
			return codes;
		}
	}
}
