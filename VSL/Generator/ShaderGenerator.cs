﻿/*
 * Microsoft Public License (Ms-PL) - Copyright (c) 2022 Vega Authors
 * This file is subject to the terms and conditions of the Microsoft Public License, the text of which can be found in
 * the 'LICENSE' file at the root of this repository, or online at <https://opensource.org/licenses/MS-PL>.
 */

using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;

namespace VSL;


// Performs GLSL generation for a specific VSL shader and all of the stages it contains
internal class ShaderGenerator
{
	// Constants
	public const string GLOBALS_BINDING_NAME = "_G_";
	public const string IMAGE_TABLE_SIZE_NAME = "_IMAGE_TABLE_SIZE";
	public const string TBUFFER_TABLE_SIZE_NAME = "_TBUFFER_TABLE_SIZE";

	#region Fields
	// Contains the generation for the shader header sources
	private readonly StringBuilder _header = new(1024);

	// Used to generate the full source when requested, taking advantage of caching
	private readonly StringBuilder _fullSourceGen = new(4096);

	// Cache of the shader stages present in the generator
	private readonly Dictionary<ShaderStage, StageGenerator> _stages = new();

	// Used to make sure certain features are not double-emitted
	private readonly HashSet<string> _usedTables = new();
	#endregion // Fields

	public ShaderGenerator()
	{
		// Append the shared global types
		EmitStructDefinition(ShaderType.Struct.GLOBAL_CAMERA_TYPE);
		EmitStructDefinition(ShaderType.Struct.GLOBAL_VIEWPORT_TYPE);
		EmitStructDefinition(ShaderType.Struct.GLOBAL_TARGET_TYPE);
		EmitStructDefinition(ShaderType.Struct.GLOBAL_TIME_TYPE);
		EmitStructDefinition(ShaderType.Struct.GLOBAL_TYPE);

		// Emit the binding size constants
		_header.Append("layout(constant_id = 0) const uint ");
		_header.Append(IMAGE_TABLE_SIZE_NAME);
		_header.AppendLine(" = 64;");
		_header.Append("layout(constant_id = 1) const uint ");
		_header.Append(TBUFFER_TABLE_SIZE_NAME);
		_header.AppendLine(" = 64;");

		// Write the globals binding uniform
		_header.Append("layout(set = 0, binding = 0, scalar) uniform _GLOBALS_ { ");
		_header.Append(ShaderType.Struct.GLOBAL_TYPE.GLName);
		_header.Append(' ');
		_header.Append(GLOBALS_BINDING_NAME);
		_header.AppendLine("; };");
		_header.AppendLine();
	}

	#region Emit
	// Emits a struct definition
	public void EmitStructDefinition(ShaderType.Struct type)
	{
		_header.Append("struct "); _header.AppendLine(type.GLName);
		_header.AppendLine("{");
		foreach (var field in type.Fields) {
			_header.Append('\t'); _header.Append(field.Type.GLName); _header.Append(' '); _header.Append(field.Name);
			if (field.Type is ShaderType.Array arrType) {
				foreach (var size in arrType.EnumerateLengths().Reverse()) {
					_header.Append('['); _header.Append(size); _header.Append(']');
				}
			}
			_header.AppendLine(";");
		}
		_header.AppendLine("};");
		_header.AppendLine();
	}

	// Emits a binding
	public void EmitBinding(string name, ShaderInfo.Binding binding)
	{
		// Check if already emitted
		var type = binding.Type;
		var tableName = type.TableName;
		if (_usedTables.Contains(tableName)) {
			return;
		}
		_usedTables.Add(tableName);

		// Emit based on the type
		if (type is ShaderType.Sampler) {
			_header.Append("layout(set = 1, binding = 2) uniform ");
			_header.Append(type.GLName);
			_header.Append(' ');
			_header.Append(tableName);
			_header.AppendLine("[];");
		}
		else if (type is ShaderType.Image imageType) {
			_header.Append("layout(set = 1, binding = 0, ");
			_header.Append(imageType.Format.GetSourceName().GLSL);
			_header.Append(") uniform ");
			_header.Append(type.GLName);
			_header.Append(' ');
			_header.Append(tableName);
			_header.Append('[');
			_header.Append(IMAGE_TABLE_SIZE_NAME);
			_header.AppendLine("];");
		}
		else if (type is ShaderType.TexelBuffer texBufferType) {
			_header.Append("layout(set = 1, binding = 1, ");
			_header.Append(texBufferType.Format.GetSourceName().GLSL);
			_header.Append(") uniform ");
			_header.Append(type.GLName);
			_header.Append(' ');
			_header.Append(tableName);
			_header.Append('[');
			_header.Append(TBUFFER_TABLE_SIZE_NAME);
			_header.AppendLine("];");
		}
		else if (type is ShaderType.Buffer bufferType) {
			// Buffers are handled as reference types loaded from pointers, so we emit the reference type
			_header.Append("layout(buffer_reference, scalar, buffer_reference_align = 4) buffer ");
			_header.Append(binding.Type.GLName);
			_header.AppendLine(" {");
			_header.Append('\t');
			_header.Append(bufferType.ElementType.GLName);
			_header.AppendLine(" data;");
			_header.AppendLine("};");
		}
		else {
			Trace.Fail($"Invalid binding shader type {type}");
		}
		_header.AppendLine();
	}

	// Emits the uniform
	public void EmitUniform(string name, ShaderType.Struct type)
	{
		_header.Append("layout(set = 0, binding = 1, scalar) uniform _UNIFORMS_ { ");
		_header.Append(type.GLName);
		_header.Append(' ');
		_header.Append(name);
		_header.AppendLine("; };");
		_header.AppendLine();
	}

	// Emits the binding index push constants and user push constant if present
	public void EmitPushConstants(IReadOnlyList<ShaderInfo.Binding> bindings, ShaderInfo.PushConstant? pcInfo)
	{
		if (bindings.Count == 0 && pcInfo is null) {
			return;
		}

		var maxTexelSlot = bindings.Count > 0
			? bindings.Where(b => b.Type is not ShaderType.Buffer).Max(b => b.Slot)
			: 0;
		var idxCount = (maxTexelSlot + 2) / 2;

		_header.AppendLine("layout(push_constant, scalar) uniform _PUSH_CONSTANTS_ {");
		foreach (var buffer in bindings.Where(b => b.Type is ShaderType.Buffer)) {
			_header.Append("\tuvec2 _b");
			_header.Append(buffer.Slot);
			_header.AppendLine("addr_;");
		}
		for (uint fi = 0; fi < idxCount; ++fi) {
			_header.Append("\tuint _tidx");
			_header.Append(fi);
			_header.AppendLine("_;");
		}
		if (pcInfo is not null) {
			_header.Append('\t');
			_header.Append(pcInfo.Type.GLName);
			_header.Append(' ');
			_header.Append(pcInfo.Name);
			_header.AppendLine(";");
		}
		_header.AppendLine("};");
		_header.AppendLine();
	}
	#endregion // Emit

	// Gets or creates the generator for the given stage
	public StageGenerator GetOrCreateStage(ShaderStage stage)
	{
		if (_stages.TryGetValue(stage, out var gen)) {
			return gen;
		}
		gen = new(stage);
		_stages.Add(stage, gen);
		return gen;
	}

	// Combines the source generators for the given stage into the final GLSL for the stage
	public string GetFullSource(ShaderStage stage)
	{
		// Append the header for all shaders
		_fullSourceGen.Clear();
		_fullSourceGen.AppendLine("/// Shader generated by VSL, do not edit by hand");
		_fullSourceGen.AppendLine("///");
		_fullSourceGen.AppendLine("#version 460");
		_fullSourceGen.AppendLine("#extension GL_EXT_scalar_block_layout : require");
		_fullSourceGen.AppendLine("#extension GL_EXT_nonuniform_qualifier : require");
		_fullSourceGen.AppendLine("#extension GL_EXT_buffer_reference2 : require");
		_fullSourceGen.AppendLine("#extension GL_EXT_buffer_reference_uvec2 : require");
		_fullSourceGen.AppendLine();

		// Append the shared header
		_fullSourceGen.Append(_header);
		_fullSourceGen.AppendLine();

		// Get the source for the specific stage
		_stages[stage].AppendSource(_fullSourceGen);

		// Convert full source to a string object
		return _fullSourceGen.ToString();
	}
}
