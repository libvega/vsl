﻿/*
 * Microsoft Public License (Ms-PL) - Copyright (c) 2022 Vega Authors
 * This file is subject to the terms and conditions of the Microsoft Public License, the text of which can be found in
 * the 'LICENSE' file at the root of this repository, or online at <https://opensource.org/licenses/MS-PL>.
 */

using System;

namespace VSL;


/// <summary>
/// Contains the results of a <see cref="ShaderCompiler"/> compilation task, either a success or failure.
/// </summary>
#if VSL_API_INTERNAL
internal
#else
public
#endif
abstract class CompileResult
{
	private CompileResult() { }


	/// <summary>
	/// Represents a successful shader compilation and contains the compiled shader program.
	/// </summary>
	public sealed class Success : CompileResult
	{
		#region Fields
		/// <summary>The compiled shader program.</summary>
		public readonly ShaderProgram Program;
		#endregion // Fields

		internal Success(ShaderProgram program) => Program = program;
	}


	/// <summary>
	/// Represents a failed compilation and contains error information about the failure.
	/// </summary>
	public sealed class Failure : CompileResult
	{
		#region Fields
		/// <summary>A human-readable description of the failure.</summary>
		public readonly string Description;

		/// <summary>The exception associated with the failure, if any.</summary>
		public readonly Exception? Exception = null;
		#endregion // Fields

		internal Failure(string description) => Description = description;

		internal Failure(string description, Exception ex)
		{
			Description = description;
			Exception = ex;
		}
	}
}
