﻿/*
 * Microsoft Public License (Ms-PL) - Copyright (c) 2022 Vega Authors
 * This file is subject to the terms and conditions of the Microsoft Public License, the text of which can be found in
 * the 'LICENSE' file at the root of this repository, or online at <https://opensource.org/licenses/MS-PL>.
 */

using System;

namespace VSL;


/// <summary>
/// Describes a set of operations to perform on fragment attachment outputs to blend attachment data.
/// </summary>
/// <param name="SrcColor">Source color factor.</param>
/// <param name="DstColor">Destination color factor.</param>
/// <param name="ColorOp">Color blending op.</param>
/// <param name="SrcAlpha">Source alpha factor.</param>
/// <param name="DstAlpha">Destination alpha factor.</param>
/// <param name="AlphaOp">Alpha blending op.</param>
#if VSL_API_INTERNAL
internal
#else
public
#endif
readonly record struct BlendState(
	BlendState.Factor SrcColor, BlendState.Factor DstColor, BlendState.Op ColorOp,
	BlendState.Factor SrcAlpha, BlendState.Factor DstAlpha, BlendState.Op AlphaOp
)
{
	#region Pre-Defined
	/// <summary>Disable blending (opaque).</summary>
	public static readonly BlendState None = 
		new(Factor.One, Factor.Zero, Op.Add, Factor.One, Factor.Zero, Op.Add);
	/// <summary>Traditional alpha blending.</summary>
	public static readonly BlendState Alpha = 
		new(Factor.SrcA, Factor.ISrcA, Op.Add, Factor.One, Factor.ISrcA, Op.Add);
	/// <summary>Traditional additive blending.</summary>
	public static readonly BlendState Add =
		new(Factor.SrcA, Factor.One, Op.Add, Factor.One, Factor.One, Op.Add);
	/// <summary>Direct multiplicitive blending.</summary>
	public static readonly BlendState Mul =
		new(Factor.DstC, Factor.Zero, Op.Add, Factor.DstA, Factor.Zero, Op.Add);
	/// <summary>Maximum value replacement blending.</summary>
	public static readonly BlendState Max =
		new(Factor.One, Factor.One, Op.Max, Factor.One, Factor.One, Op.Max);
	/// <summary>Minimum value replacement blending.</summary>
	public static readonly BlendState Min =
		new(Factor.One, Factor.One, Op.Min, Factor.One, Factor.One, Op.Min);
	#endregion // Pre-Defined

	// Returns the factor with the given name
	internal static Factor? ParseFactor(string name) => name.ToLowerInvariant() switch {
		"zero"     => Factor.Zero,
		"one"      => Factor.One,
		"srcc"     => Factor.SrcC,
		"isrcc"    => Factor.ISrcC,
		"dstc"     => Factor.DstC,
		"idstc"    => Factor.IDstC,
		"srca"     => Factor.SrcA,
		"isrca"    => Factor.ISrcA,
		"dsta"     => Factor.DstA,
		"idsta"    => Factor.IDstA,
		"refc"     => Factor.RefC,
		"irefc"    => Factor.IRefC,
		"refa"     => Factor.RefA,
		"irefa"    => Factor.IRefA,
		"saturate" => Factor.Saturate,
		_ => null
	};

	// Returns the op with the given name
	internal static Op? ParseOp(string name) => name.ToLowerInvariant() switch {
		"add"    => Op.Add,
		"sub"    => Op.Sub,
		"revsub" => Op.RevSub,
		"min"    => Op.Min,
		"max"    => Op.Max,
		_ => null
	};


	/// <summary>
	/// Multiplicative blending factors.
	/// </summary>
	public enum Factor : byte
	{
		Zero     = 0,
		One      = 1,
		SrcC     = 2,
		ISrcC    = 3,
		DstC     = 4,
		IDstC    = 5,
		SrcA     = 6,
		ISrcA    = 7,
		DstA     = 8,
		IDstA    = 9,
		RefC     = 10,
		IRefC    = 11,
		RefA     = 12,
		IRefA    = 13,
		Saturate = 14
	}

	/// <summary>
	/// Blending operations.
	/// </summary>
	public enum Op : byte
	{
		Add    = 0,
		Sub    = 1,
		RevSub = 2,
		Min    = 3,
		Max    = 4
	}
}
