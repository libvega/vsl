﻿/*
 * Microsoft Public License (Ms-PL) - Copyright (c) 2022 Vega Authors
 * This file is subject to the terms and conditions of the Microsoft Public License, the text of which can be found in
 * the 'LICENSE' file at the root of this repository, or online at <https://opensource.org/licenses/MS-PL>.
 */

using System;
using System.Collections.Generic;
using System.Numerics;

namespace VSL;


/// <summary>
/// Supported shader stages.
/// </summary>
#if VSL_API_INTERNAL
internal
#else
public
#endif
enum ShaderStage : uint
{
	/// <summary>Vertex stage in <see cref="ShaderKind.Graphics"/> shaders.</summary>
	Vertex   = 0,
	/// <summary>Fragment stage in <see cref="ShaderKind.Graphics"/> shaders.</summary>
	Fragment = 4
}


/// <summary>
/// Represents a mask of <see cref="ShaderStage"/> values.
/// </summary>
#if VSL_API_INTERNAL
internal
#else
public
#endif
readonly record struct ShaderStageMask
{
	/// <summary>A mask of no shader stages.</summary>
	public static readonly ShaderStageMask None = new();
	/// <summary>A mask of all <see cref="ShaderKind.Graphics"/> shader stages.</summary>
	public static readonly ShaderStageMask AllGraphics = new(0x00000011);

	#region Fields
	/// <summary>The raw mask value.</summary>
	public readonly uint Value;

	/// <summary>Gets the number of stages in the mask.</summary>
	public readonly uint Count => (uint)BitOperations.PopCount(Value);

	/// <summary>Gets if any stages are present in the mask.</summary>
	public readonly bool Any => Value != 0;
	#endregion // Fields

	/// <summary>Construct a default mask of no stages.</summary>
	public ShaderStageMask() => Value = 0;
	/// <summary>Construct a mask with the given stage.</summary>
	public ShaderStageMask(ShaderStage stage) => Value = (1u << (int)stage);
	internal ShaderStageMask(uint value) => Value = value;

	public readonly override string ToString() => Value.ToString("X8");

	/// <summary>Gets if the stage is present in the mask.</summary>
	public readonly bool Has(ShaderStage stage) => (Value & (1u << (int)stage)) != 0;

	/// <summary>Enumerates over the stages present in the mask.</summary>
	public readonly IEnumerable<ShaderStage> EnumerateStages()
	{
		for (uint shift = 0, mask = Value; mask != 0; ++shift, mask >>= 1) {
			if ((mask & 0x1) != 0) {
				yield return (ShaderStage)shift;
			}
		}
	}

	#region Operators
	public static implicit operator ShaderStageMask (ShaderStage stage) => new(stage);

	public static ShaderStageMask operator | (ShaderStageMask l, ShaderStage r) => new(l.Value | (1u << (int)r));

	public static ShaderStageMask operator | (ShaderStageMask l, ShaderStageMask r) => new(l.Value | r.Value);

	public static ShaderStageMask operator & (ShaderStageMask l, ShaderStageMask r) => new(l.Value & r.Value);
	#endregion // Operators
}


/// <summary>
/// Contains extensions for <see cref="ShaderStage"/> values.
/// </summary>
#if VSL_API_INTERNAL
internal
#else
public
#endif
static class ShaderStageExtensions
{
	/// <summary>Gets the <see cref="ShaderKind"/> value that the stage appears in.</summary>
	public static ShaderKind GetShaderKind(this ShaderStage stage) => stage switch {
		ShaderStage.Vertex   => ShaderKind.Graphics,
		ShaderStage.Fragment => ShaderKind.Graphics,
		_ => throw new ArgumentOutOfRangeException(nameof(stage), "Unknown shader stage")
	};

	/// <summary>Gets the four-character name for the stage.</summary>
	public static string GetStageCode(this ShaderStage stage) => stage switch {
		ShaderStage.Vertex   => "vert",
		ShaderStage.Fragment => "frag",
		_ => throw new ArgumentOutOfRangeException(nameof(stage), "Unknown shader stage")
	};

	// Attempts to convert the name into a shader stage value (null if invalid)
	internal static ShaderStage? FromStageCode(string code) => code switch {
		"vert" => ShaderStage.Vertex,
		"frag" => ShaderStage.Fragment,
		_ => null
	};
}
