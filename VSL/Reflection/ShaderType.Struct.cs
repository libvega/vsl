﻿/*
 * Microsoft Public License (Ms-PL) - Copyright (c) 2022 Vega Authors
 * This file is subject to the terms and conditions of the Microsoft Public License, the text of which can be found in
 * the 'LICENSE' file at the root of this repository, or online at <https://opensource.org/licenses/MS-PL>.
 */

using System;
using System.Collections.Generic;

namespace VSL;


// Defines a struct shader type
#if VSL_API_INTERNAL
internal
#else
public
#endif
abstract partial record ShaderType
{
	/// <summary>
	/// Represents a user-defined compound POD type.
	/// </summary>
	/// <param name="StructName">The name of the struct type.</param>
	/// <param name="Fields">The fields of the struct.</param>
	/// <param name="TypeSize">The total size of the struct, in bytes.</param>
	/// <param name="TypeAlignment">The alignment requirements of the struct, in bytes.</param>
	public sealed record Struct(string StructName, IReadOnlyList<Struct.Field> Fields, uint TypeSize, uint TypeAlignment) 
		: DataType
	{
		/// <summary>
		/// Describes a field of a <see cref="Struct"/> shader type.
		/// </summary>
		/// <param name="Name">The name of the field.</param>
		/// <param name="Type">The field type.</param>
		/// <param name="Offset">The offset of the field into the containing type, in bytes.</param>
		public sealed record Field(string Name, DataType Type, uint Offset);

		/// <summary>The maximum size a struct type can be, in bytes.</summary>
		public const uint MAX_STRUCT_SIZE = 1024;

		#region Fields
		public override string Name => StructName;
		internal override string GLName => $"{StructName}_t";

		public override uint Size => TypeSize;
		public override uint Alignment => TypeAlignment;
		public override uint SlotCount => UInt32.MaxValue; // Not a feature of structs since they cannot take up slots
		#endregion // Fields

		internal override bool CanAssignTo(ShaderType target) => (target is Struct structType) &&
			structType.StructName == StructName;

		internal override (ShaderType Type, string Format)? GetMember(string name, bool write, out string? err)
		{
			var field = GetField(name);
			if (field is null) {
				err = $"Struct type {StructName} has no field '{name}'";
				return null;
			}
			err = null;
			return (field.Type, $"$expr.{field.Name}");
		}

		/// <summary>Gets the field with the given name, or <c>null</c> if not found.</summary>
		public Field? GetField(string name)
		{
			foreach (var f in Fields) {
				if (f.Name == name) {
					return f;
				}
			}
			return null;
		}

		/// <summary>Gets if a field with the given name is in the struct.</summary>
		public bool HasField(string name) => GetField(name) is not null;


		/// <summary>
		/// Type for progressively building and validating a <see cref="Struct"/> type in a fluent style.
		/// </summary>
		public sealed class Builder
		{
			#region Fields
			/// <summary>The running size of the struct type being built.</summary>
			public uint Size { get; private set; } = 0;
			/// <summary>The running alignment requirements of the struct type being built.</summary>
			public uint Alignment { get; private set; } = 0;

			/// <summary>The list of currently added fields.</summary>
			public IReadOnlyList<Field> Fields => _fields;
			private List<Field> _fields = new();
			#endregion // Fields

			public Builder() { }

			/// <summary>Attempts to add a field to the type, throwing an exception if invalid.</summary>
			/// <exception cref="ArgumentException">Adding the field would cause invalid state.</exception>
			public Builder AddField(string name, ShaderType type)
			{
				// Check if name is taken
				if (_fields.Find(f => f.Name == name) is not null) {
					throw new ArgumentException(
						$"A field with the name '{name}' already exists in the struct", nameof(name));
				}

				// Validate the type
				if (type is not DataType dataType) {
					throw new ArgumentException(
						$"Struct fields must be data types (actual {type})", nameof(type));
				}

				// Calculate the potential new size and validate
				var offset = Size;
				if ((offset % dataType.Alignment) != 0) {
					offset += (dataType.Alignment - (offset % dataType.Alignment));
				}
				var newSize = offset + dataType.Size;
				if ((newSize % dataType.Alignment) != 0) {
					newSize += (dataType.Alignment - (newSize % dataType.Alignment));
				}
				if (newSize > MAX_STRUCT_SIZE) {
					throw new ArgumentException(
						$"Struct size limit exceeded with field '{type} {name}' ({newSize})", nameof(type));
				}

				// Add the field and update state
				_fields.Add(new(name, dataType, offset));
				if (dataType.Alignment > Alignment) {
					Alignment = dataType.Alignment;
				}
				Size = newSize;

				return this;
			}

			/// <summary>Builds the current state into a struct type with the given name.</summary>
			/// <remarks>The state of the builder is reset after this call.</remarks>
			public Struct Build(string name)
			{
				if (Size == 0) {
					throw new InvalidOperationException("Cannot construct an empty struct type");
				}
				var type = new Struct(name, _fields, Size, Alignment);
				Reset();
				return type;
			}

			/// <summary>Resets the state of the builder to start building a new type.</summary>
			public void Reset()
			{
				_fields = new();
				Size = 0;
				Alignment = 0;
			}
		}


		// Initialize the known struct types
		static Struct()
		{
			var builder = new Builder();

			// Camera type
			GLOBAL_CAMERA_TYPE = builder
				.AddField("pos", Vector.Float3)
				.AddField("dir", Vector.Float3)
				.Build("_Globals_Camera");

			// Viewport type
			GLOBAL_VIEWPORT_TYPE = builder
				.AddField("left", Scalar.Float)
				.AddField("top", Scalar.Float)
				.AddField("width", Scalar.Float)
				.AddField("height", Scalar.Float)
				.AddField("minDepth", Scalar.Float)
				.AddField("maxDepth", Scalar.Float)
				.Build("_Globals_Viewport");

			// Target type
			GLOBAL_TARGET_TYPE = builder
				.AddField("width", Scalar.Uint)
				.AddField("height", Scalar.Uint)
				.Build("_Globals_Target");

			// Time type
			GLOBAL_TIME_TYPE = builder
				.AddField("elapsed", Scalar.Float)
				.AddField("delta", Scalar.Float)
				.AddField("deltaUnscaled", Scalar.Float)
				.Build("_Globals_Time");

			// Overall type
			GLOBAL_TYPE = builder
				.AddField("viewProj", Matrix.Float4x4)
				.AddField("invViewProj", Matrix.Float4x4)
				.AddField("camera", GLOBAL_CAMERA_TYPE)
				.AddField("viewport", GLOBAL_VIEWPORT_TYPE)
				.AddField("target", GLOBAL_TARGET_TYPE)
				.AddField("time", GLOBAL_TIME_TYPE)
				.Build("_Globals");
		}

		// The struct types for the global builtins "$G"
		internal static readonly Struct GLOBAL_CAMERA_TYPE;
		internal static readonly Struct GLOBAL_VIEWPORT_TYPE;
		internal static readonly Struct GLOBAL_TARGET_TYPE;
		internal static readonly Struct GLOBAL_TIME_TYPE;
		internal static readonly Struct GLOBAL_TYPE;
	}
}
