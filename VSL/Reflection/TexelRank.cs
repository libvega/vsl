﻿/*
 * Microsoft Public License (Ms-PL) - Copyright (c) 2022 Vega Authors
 * This file is subject to the terms and conditions of the Microsoft Public License, the text of which can be found in
 * the 'LICENSE' file at the root of this repository, or online at <https://opensource.org/licenses/MS-PL>.
 */

using System;

namespace VSL;


/// <summary>
/// Supported texel ranks (number of dimensions and "array-ness" of texel types).
/// </summary>
#if VSL_API_INTERNAL
internal
#else
public
#endif
enum TexelRank : byte
{
	/// <summary>Texel data has one dimension.</summary>
	E1D = 0,
	/// <summary>Texel data has two dimensions.</summary>
	E2D = 1,
	/// <summary>Texel data has three dimensions.</summary>
	E3D = 2,
	/// <summary>Texel data is an array of two-dimensional data planes.</summary>
	E2DArray = 3
}


// Contains extensions for TexelRank values.
internal static class TexelRankExtensions
{
	// Gets the number of addressable dimensions for the rank
	public static uint GetDimensionCount(this TexelRank rank) => rank switch {
		TexelRank.E1D      => 1,
		TexelRank.E2D      => 2,
		TexelRank.E3D      => 3,
		TexelRank.E2DArray => 3,
		_ => throw new ArgumentOutOfRangeException(nameof(rank))
	};

	// Gets the source name of the rank
	public static string GetSourceName(this TexelRank rank) => rank switch {
		TexelRank.E1D      => "1D",
		TexelRank.E2D      => "2D",
		TexelRank.E3D      => "3D",
		TexelRank.E2DArray => "2DArray",
		_ => throw new ArgumentOutOfRangeException(nameof(rank))
	};

	// Parses the source name of the rank
	public static TexelRank? FromSourceName(in ReadOnlySpan<char> rank)
	{
		if (rank.SequenceEqual("1D")) return TexelRank.E1D;
		if (rank.SequenceEqual("2D")) return TexelRank.E2D;
		if (rank.SequenceEqual("3D")) return TexelRank.E3D;
		if (rank.SequenceEqual("2DArray")) return TexelRank.E2DArray;
		return null;
	}
}
