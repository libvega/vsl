﻿/*
 * Microsoft Public License (Ms-PL) - Copyright (c) 2022 Vega Authors
 * This file is subject to the terms and conditions of the Microsoft Public License, the text of which can be found in
 * the 'LICENSE' file at the root of this repository, or online at <https://opensource.org/licenses/MS-PL>.
 */

using System;

namespace VSL;


/// <summary>
/// Supported texel data formats.
/// </summary>
#if VSL_API_INTERNAL
internal
#else
public
#endif
enum TexelFormat : byte
{
	/// <summary>Scalar one-byte unsigned normalized (channels: R).</summary>
	U8Norm   = 0,
	/// <summary>Vector one-byte unsigned normalized (channels: RG).</summary>
	U8Norm2  = 1,
	/// <summary>Vector one-byte unsigned normalized (channels: RGBA).</summary>
	U8Norm4  = 2,
	/// <summary>Scalar one-byte signed normalized (channels: R).</summary>
	S8Norm   = 3,
	/// <summary>Vector one-byte signed normalized (channels: RG).</summary>
	S8Norm2  = 4,
	/// <summary>Vector one-byte signed normalized (channels: RGBA).</summary>
	S8Norm4  = 5,
	/// <summary>Scalar two-byte unsigned normalized (channels: R).</summary>
	U16Norm  = 6,
	/// <summary>Vector two-byte unsigned normalized (channels: RG).</summary>
	U16Norm2 = 7,
	/// <summary>Vector two-byte unsigned normalized (channels: RGBA).</summary>
	U16Norm4 = 8,
	/// <summary>Scalar two-byte signed normalized (channels: R).</summary>
	S16Norm  = 9,
	/// <summary>Vector two-byte signed normalized (channels: RG).</summary>
	S16Norm2 = 10,
	/// <summary>Vector two-byte signed normalized (channels: RGBA).</summary>
	S16Norm4 = 11,
	/// <summary>Scalar four-byte unsigned integer (channels: R).</summary>
	UInt     = 12,
	/// <summary>Vector four-byte unsigned integer (channels: RG).</summary>
	UInt2    = 13,
	/// <summary>Vector four-byte unsigned integer (channels: RGBA).</summary>
	UInt4    = 14,
	/// <summary>Scalar four-byte signed integer (channels: R).</summary>
	Int      = 15,
	/// <summary>Vector four-byte signed integer (channels: RG).</summary>
	Int2     = 16,
	/// <summary>Vector four-byte signed integer (channels: RGBA).</summary>
	Int4     = 17,
	/// <summary>Scalar four-byte floating point (channels: R).</summary>
	Float    = 18,
	/// <summary>Vector four-byte floating point (channels: RG).</summary>
	Float2   = 19,
	/// <summary>Vector four-byte floating point (channels: RGBA).</summary>
	Float4   = 20
}


/// <summary>
/// Contains extensions for <see cref="TexelFormat"/> values.
/// </summary>
#if VSL_API_INTERNAL
internal
#else
public
#endif
static class TexelFormatExtensions
{
	/// <summary>Gets the number of color components in the format.</summary>
	public static uint GetComponentCount(this TexelFormat format) => format switch {
		TexelFormat.U8Norm   => 1,
		TexelFormat.U8Norm2  => 2,
		TexelFormat.U8Norm4  => 4,
		TexelFormat.S8Norm   => 1,
		TexelFormat.S8Norm2  => 2,
		TexelFormat.S8Norm4  => 4,
		TexelFormat.U16Norm  => 1,
		TexelFormat.U16Norm2 => 2,
		TexelFormat.U16Norm4 => 4,
		TexelFormat.S16Norm  => 1,
		TexelFormat.S16Norm2 => 2,
		TexelFormat.S16Norm4 => 4,
		TexelFormat.UInt     => 1,
		TexelFormat.UInt2    => 2,
		TexelFormat.UInt4    => 4,
		TexelFormat.Int      => 1,
		TexelFormat.Int2     => 2,
		TexelFormat.Int4     => 4,
		TexelFormat.Float    => 1,
		TexelFormat.Float2   => 2,
		TexelFormat.Float4   => 4,
		_ => throw new ArgumentOutOfRangeException(nameof(format))
	};

	/// <summary>Gets the numeric shader type compatible with the given texel format.</summary>
	public static ShaderType.Numeric GetDataType(this TexelFormat format) => format switch {
		TexelFormat.U8Norm   => ShaderType.Scalar.Float,
		TexelFormat.U8Norm2  => ShaderType.Vector.Float2,
		TexelFormat.U8Norm4  => ShaderType.Vector.Float4,
		TexelFormat.S8Norm   => ShaderType.Scalar.Float,
		TexelFormat.S8Norm2  => ShaderType.Vector.Float2,
		TexelFormat.S8Norm4  => ShaderType.Vector.Float4,
		TexelFormat.U16Norm  => ShaderType.Scalar.Float,
		TexelFormat.U16Norm2 => ShaderType.Vector.Float2,
		TexelFormat.U16Norm4 => ShaderType.Vector.Float4,
		TexelFormat.S16Norm  => ShaderType.Scalar.Float,
		TexelFormat.S16Norm2 => ShaderType.Vector.Float2,
		TexelFormat.S16Norm4 => ShaderType.Vector.Float4,
		TexelFormat.UInt     => ShaderType.Scalar.Uint,
		TexelFormat.UInt2    => ShaderType.Vector.Uint2,
		TexelFormat.UInt4    => ShaderType.Vector.Uint4,
		TexelFormat.Int      => ShaderType.Scalar.Int,
		TexelFormat.Int2     => ShaderType.Vector.Int2,
		TexelFormat.Int4     => ShaderType.Vector.Int4,
		TexelFormat.Float    => ShaderType.Scalar.Float,
		TexelFormat.Float2   => ShaderType.Vector.Float2,
		TexelFormat.Float4   => ShaderType.Vector.Float4,
		_ => throw new ArgumentOutOfRangeException(nameof(format))
	};

	// Parses the format as it appears in VSL source
	internal static TexelFormat? FromVSLFormat(in ReadOnlySpan<char> format)
	{
		if (format.SequenceEqual("u8norm"))   return TexelFormat.U8Norm;
		if (format.SequenceEqual("u8norm2"))  return TexelFormat.U8Norm2;
		if (format.SequenceEqual("u8norm4"))  return TexelFormat.U8Norm4;
		if (format.SequenceEqual("s8norm"))   return TexelFormat.S8Norm;
		if (format.SequenceEqual("s8norm2"))  return TexelFormat.S8Norm2;
		if (format.SequenceEqual("s8norm4"))  return TexelFormat.S8Norm4;
		if (format.SequenceEqual("u16norm"))  return TexelFormat.U16Norm;
		if (format.SequenceEqual("u16norm2")) return TexelFormat.U16Norm2;
		if (format.SequenceEqual("u16norm4")) return TexelFormat.U16Norm4;
		if (format.SequenceEqual("s16norm"))  return TexelFormat.S16Norm;
		if (format.SequenceEqual("s16norm2")) return TexelFormat.S16Norm2;
		if (format.SequenceEqual("s16norm4")) return TexelFormat.S16Norm4;
		if (format.SequenceEqual("int"))      return TexelFormat.Int;
		if (format.SequenceEqual("int2"))     return TexelFormat.Int2;
		if (format.SequenceEqual("int4"))     return TexelFormat.Int4;
		if (format.SequenceEqual("uint"))     return TexelFormat.UInt;
		if (format.SequenceEqual("uint2"))    return TexelFormat.UInt2;
		if (format.SequenceEqual("uint4"))    return TexelFormat.UInt4;
		if (format.SequenceEqual("float"))    return TexelFormat.Float;
		if (format.SequenceEqual("float2"))   return TexelFormat.Float2;
		if (format.SequenceEqual("float4"))   return TexelFormat.Float4;
		return null;
	}

	// Gets the (vsl, glsl) source name of the format
	internal static (string VSL, string GLSL) GetSourceName(this TexelFormat format) => format switch {
		TexelFormat.U8Norm   => ("u8norm",   "r8"),
		TexelFormat.U8Norm2  => ("u8norm2",  "rg8"),
		TexelFormat.U8Norm4  => ("u8norm4",  "rgba8"),
		TexelFormat.S8Norm   => ("s8norm",   "r8_snorm"),
		TexelFormat.S8Norm2  => ("s8norm2",  "rg8_snorm"),
		TexelFormat.S8Norm4  => ("s8norm4",  "rgba8_snorm"),
		TexelFormat.U16Norm  => ("u16norm",  "r16"),
		TexelFormat.U16Norm2 => ("u16norm2", "rg16"),
		TexelFormat.U16Norm4 => ("u16norm4", "rgba16"),
		TexelFormat.S16Norm  => ("s16norm",  "r16_snorm"),
		TexelFormat.S16Norm2 => ("s16norm2", "rg16_snorm"),
		TexelFormat.S16Norm4 => ("s16norm4", "rgba16_snorm"),
		TexelFormat.UInt     => ("int",      "r32ui"),
		TexelFormat.UInt2    => ("int2",     "rg32ui"),
		TexelFormat.UInt4    => ("int4",     "rgba32ui"),
		TexelFormat.Int      => ("uint",     "r32i"),
		TexelFormat.Int2     => ("uint2",    "rg32i"),
		TexelFormat.Int4     => ("uint4",    "rgba32i"),
		TexelFormat.Float    => ("float",    "r32f"),
		TexelFormat.Float2   => ("float2",   "rg32f"),
		TexelFormat.Float4   => ("float4",   "rgba32f"),
		_ => throw new ArgumentOutOfRangeException(nameof(format))
	};

	// Gets the prefix character for texel types using the given format in VSL source (glsl is just ToLower())
	internal static string GetSourcePrefix(this TexelFormat format) => format.GetDataType().Type switch {
		ShaderType.NumericType.Float    => String.Empty,
		ShaderType.NumericType.Signed   => "I",
		ShaderType.NumericType.Unsigned => "U",
		_ => throw new ArgumentOutOfRangeException(nameof(format))
	};
}
