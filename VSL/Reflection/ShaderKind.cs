﻿/*
 * Microsoft Public License (Ms-PL) - Copyright (c) 2022 Vega Authors
 * This file is subject to the terms and conditions of the Microsoft Public License, the text of which can be found in
 * the 'LICENSE' file at the root of this repository, or online at <https://opensource.org/licenses/MS-PL>.
 */

using System;

namespace VSL;


/// <summary>
/// Represents the different target data pipelines for a shader program.
/// </summary>
#if VSL_API_INTERNAL
internal
#else
public
#endif
enum ShaderKind : byte
{
	/// <summary>The shader targets the standard raster graphics pipeline.</summary>
	Graphics = 0
}
