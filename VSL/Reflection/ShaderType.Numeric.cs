﻿/*
 * Microsoft Public License (Ms-PL) - Copyright (c) 2022 Vega Authors
 * This file is subject to the terms and conditions of the Microsoft Public License, the text of which can be found in
 * the 'LICENSE' file at the root of this repository, or online at <https://opensource.org/licenses/MS-PL>.
 */

using System;

namespace VSL;


// Defines the numeric shader types
#if VSL_API_INTERNAL
internal
#else
public
#endif
abstract partial record ShaderType
{
	/// <summary>
	/// Represents the base value types that <see cref="Numeric"/> shader types can hold.
	/// </summary>
	public enum NumericType : byte
	{
		/// <summary>Boolean true/false values (stored as 32-bit integers).</summary>
		Boolean  = 0,
		/// <summary>Signed integer values.</summary>
		Signed   = 1,
		/// <summary>Unsigned integer values.</summary>
		Unsigned = 2,
		/// <summary>Floating point values.</summary>
		Float    = 3
	}


	/// <summary>
	/// Represents an abstract numeric type, which can be a scalar, vector, or matrix or value types.
	/// </summary>
	/// <param name="Type">The base value type contained in the numeric type.</param>
	/// <param name="Rows">The number of rows in the type (gives the component count for vectors).</param>
	/// <param name="Cols">The number of columns in the type (>1 only for matrices).</param>
	public abstract record Numeric(NumericType Type, uint Rows, uint Cols) : DataType
	{
		#region Fields
		/// <summary>Gets if the type is a scalar.</summary>
		public new bool IsScalar => Rows == 1 && Cols == 1;
		/// <summary>Gets if the type is a vector.</summary>
		public new bool IsVector => Rows > 1 && Cols == 1;
		/// <summary>Gets if the type is a matrix.</summary>
		public new bool IsMatrix => Rows > 1 && Cols > 1;

		/// <summary>Gets if the base value type is a boolean.</summary>
		public bool IsBoolean => Type == NumericType.Boolean;
		/// <summary>Gets if the base value type is a signed or unsigned integer.</summary>
		public bool IsInteger => Type == NumericType.Signed || Type == NumericType.Unsigned;
		/// <summary>Gets if the base value type is a signed integer.</summary>
		public bool IsSigned => Type == NumericType.Signed;
		/// <summary>Gets if the base value type is an unsigned integer.</summary>
		public bool IsUnsigned => Type == NumericType.Unsigned;
		/// <summary>Gets if the base value type is a float.</summary>
		public bool IsFloat => Type == NumericType.Float;

		public override uint Size => 4 * Rows * Cols;
		public override uint Alignment => 4; // Scalar layout (plus no non-4-byte types) means this is always 4
		public override uint SlotCount => Cols; // Scalars+Vectors are 1 slot, Matrices are Cols slots

		public override string Name => Type switch {
			NumericType.Boolean  => (Rows == 1) ? "bool" : $"bool{Rows}",
			NumericType.Signed   => (Rows == 1) ? "int"  : $"int{Rows}",
			NumericType.Unsigned => (Rows == 1) ? "uint" : $"uint{Rows}",
			NumericType.Float    => Cols switch {
				1 => (Rows == 1) ? "float" : $"float{Rows}",
				_ => $"float{Cols}x{Rows}"
			},
			_ => throw new ArgumentOutOfRangeException(nameof(Type), $"Invalid numeric type ({Type}, {Rows}, {Cols})")
		};

		internal override string GLName => Type switch {
			NumericType.Boolean  => (Rows == 1) ? "bool" : $"bvec{Rows}",
			NumericType.Signed   => (Rows == 1) ? "int"  : $"ivec{Rows}",
			NumericType.Unsigned => (Rows == 1) ? "uint" : $"uvec{Rows}",
			NumericType.Float    => Cols switch {
				1 => (Rows == 1) ? "float" : $"vec{Rows}",
				_ => $"mat{Cols}x{Rows}"
			},
			_ => throw new ArgumentOutOfRangeException(nameof(Type), $"Invalid numeric type ({Type}, {Rows}, {Cols})")
		};
		#endregion // Fields

		internal override bool CanAssignTo(ShaderType target) => (target is Numeric other) &&
			Type.CanAssignTo(other.Type) && (Rows == other.Rows) && (Cols == other.Cols);

		internal override (ShaderType Type, string Format)? GetIndex(Numeric numeric, out string? err)
		{
			if (numeric.IsInteger && numeric.IsScalar) {
				if (this is Vector v) {
					err = null;
					return (v.ScalarType, "$expr[$idx]");
				}
				else if (this is Matrix m) {
					err = null;
					return (m.VectorType, "$expr[$idx]");
				}
				err = "Scalar types cannot be indexed";
				return null;
			}
			err = "Numeric types can only be indexed with scalar integers";
			return null;
		}

		/// <summary>Gets the type associated with the given numeric type and sizes, or <c>null</c>.</summary>
		public static Numeric? Get(NumericType type, uint rows, uint cols)
		{
			if (rows < 1 || rows > 4 || cols < 1 || cols > 4) {
				return null;
			}

			if (rows == 1 && cols == 1) {
				return type switch {
					NumericType.Boolean  => Scalar.Bool,
					NumericType.Signed   => Scalar.Int,
					NumericType.Unsigned => Scalar.Uint,
					NumericType.Float    => Scalar.Float,
					_ => null
				};
			}
			else if (cols == 1) {
				return (type, rows) switch {
					(NumericType.Boolean, 2)  => Vector.Bool2,
					(NumericType.Boolean, 3)  => Vector.Bool3,
					(NumericType.Boolean, 4)  => Vector.Bool4,
					(NumericType.Signed, 2)   => Vector.Int2,
					(NumericType.Signed, 3)   => Vector.Int3,
					(NumericType.Signed, 4)   => Vector.Int4,
					(NumericType.Unsigned, 2) => Vector.Uint2,
					(NumericType.Unsigned, 3) => Vector.Uint3,
					(NumericType.Unsigned, 4) => Vector.Uint4,
					(NumericType.Float, 2)    => Vector.Float2,
					(NumericType.Float, 3)    => Vector.Float3,
					(NumericType.Float, 4)    => Vector.Float4,
					_ => null
				};
			}
			else {
				if (type != NumericType.Float) {
					return null;
				}
				return (rows, cols) switch { 
					(2, 2) => Matrix.Float2x2,
					(2, 3) => Matrix.Float3x2,
					(2, 4) => Matrix.Float4x2,
					(3, 2) => Matrix.Float2x3,
					(3, 3) => Matrix.Float3x3,
					(3, 4) => Matrix.Float4x3,
					(4, 2) => Matrix.Float2x4,
					(4, 3) => Matrix.Float3x4,
					(4, 4) => Matrix.Float4x4,
					_ => null
				};
			}
		}
	}


	/// <summary>
	/// Represents a scalar numeric (value) type.
	/// </summary>
	public sealed record Scalar(NumericType Type) : Numeric(Type, 1, 1)
	{
		/// <summary>Predefined <c>bool</c> type.</summary>
		public static readonly Scalar Bool  = new(NumericType.Boolean);
		/// <summary>Predefined <c>int</c> type.</summary>
		public static readonly Scalar Int   = new(NumericType.Signed);
		/// <summary>Predefined <c>uint</c> type.</summary>
		public static readonly Scalar Uint  = new(NumericType.Unsigned);
		/// <summary>Predefined <c>float</c> type.</summary>
		public static readonly Scalar Float = new(NumericType.Float);
	}


	/// <summary>
	/// Represents a vector numeric (value) type.
	/// </summary>
	public sealed record Vector(NumericType Type, uint Length) : Numeric(Type, Length, 1)
	{
		/// <summary>Gets the type describing the scalar components of the vector.</summary>
		public Scalar ScalarType => Type switch {
			NumericType.Boolean  => Scalar.Bool,
			NumericType.Signed   => Scalar.Bool,
			NumericType.Unsigned => Scalar.Bool,
			NumericType.Float    => Scalar.Bool,
			_ => throw new ArgumentOutOfRangeException(nameof(Type))
		};

		internal override (ShaderType Type, string Format)? GetMember(string name, bool write, out string? err)
		{
			if (!CheckSwizzle( name, write, out err)) {
				return null;
			}
			var type = Numeric.Get(this.Type, (uint)name.Length, 1);
			return (type!, $"$expr.{name}");
		}

		internal bool CheckSwizzle(string swizzle, bool write, out string err)
		{
			if (swizzle.Length > 4) {
				err = "Swizzles cannot be longer than 4 characters";
				return false;
			}

			// Validate the swizzle characters
			uint? swztype = null; // 0 = pos, 1 = color, 2 = uv
			Span<bool> seen = stackalloc bool[4];
			foreach (var swzch in swizzle) {
				(uint Type, uint Comp)? chinfo = swzch switch {
					'x' => (0, 1),
					'y' => (0, 2),
					'z' => (0, 3),
					'w' => (0, 4),
					'r' => (1, 1),
					'g' => (1, 2),
					'b' => (1, 3),
					'a' => (1, 4),
					's' => (2, 1),
					't' => (2, 2),
					'p' => (2, 3),
					'q' => (2, 4),
					_ => null
				};
				if (!chinfo.HasValue) {
					err = $"Invalid vector swizzle character '{swzch}'";
					return false;
				}
				swztype ??= chinfo!.Value.Type;
				if (swztype != chinfo.Value.Type) {
					err = $"Cannot mix vector swizzle character types";
					return false;
				}
				if (chinfo.Value.Comp > Rows) {
					err = $"Vector type {this} does not support swizzle character '{swzch}'";
					return false;
				}
				if (seen[(int)chinfo.Value.Comp - 1] && write) {
					err = $"Duplicate swizzle index {chinfo.Value.Comp} ('{swzch}') when writing to vector type";
					return false;
				}
				seen[(int)chinfo.Value.Comp - 1] = true;
			}

			err = default!;
			return true;
		}

		/// <summary>Predefined <c>bool2</c> type.</summary>
		public static readonly Vector Bool2  = new(NumericType.Boolean, 2);
		/// <summary>Predefined <c>bool3</c> type.</summary>
		public static readonly Vector Bool3  = new(NumericType.Boolean, 3);
		/// <summary>Predefined <c>bool4</c> type.</summary>
		public static readonly Vector Bool4  = new(NumericType.Boolean, 4);
		/// <summary>Predefined <c>int2</c> type.</summary>
		public static readonly Vector Int2   = new(NumericType.Signed, 2);
		/// <summary>Predefined <c>int3</c> type.</summary>
		public static readonly Vector Int3   = new(NumericType.Signed, 3);
		/// <summary>Predefined <c>int4</c> type.</summary>
		public static readonly Vector Int4   = new(NumericType.Signed, 4);
		/// <summary>Predefined <c>uint2</c> type.</summary>
		public static readonly Vector Uint2  = new(NumericType.Unsigned, 2);
		/// <summary>Predefined <c>uint3</c> type.</summary>
		public static readonly Vector Uint3  = new(NumericType.Unsigned, 3);
		/// <summary>Predefined <c>uint4</c> type.</summary>
		public static readonly Vector Uint4  = new(NumericType.Unsigned, 4);
		/// <summary>Predefined <c>float2</c> type.</summary>
		public static readonly Vector Float2 = new(NumericType.Float, 2);
		/// <summary>Predefined <c>float3</c> type.</summary>
		public static readonly Vector Float3 = new(NumericType.Float, 3);
		/// <summary>Predefined <c>float4</c> type.</summary>
		public static readonly Vector Float4 = new(NumericType.Float, 4);
	}


	/// <summary>
	/// Represents a matrix numeric (value) type.
	/// </summary>
	public sealed record Matrix(uint Rows, uint Cols) : Numeric(NumericType.Float, Rows, Cols)
	{
		/// <summary>Gets the type describing the scalar components of the vector.</summary>
		public Scalar ScalarType => Scalar.Float;
		/// <summary>Gets the vector type associated with the rows of the matrix.</summary>
		public Vector VectorType => Cols switch {
			2 => Vector.Float2,
			3 => Vector.Float3,
			4 => Vector.Float4,
			_ => throw new ArgumentOutOfRangeException(nameof(Cols))
		};

		/// <summary>Predefined <c>float2x2</c> type.</summary>
		public static readonly Matrix Float2x2 = new(2, 2);
		/// <summary>Predefined <c>float2x3</c> type.</summary>
		public static readonly Matrix Float2x3 = new(3, 2);
		/// <summary>Predefined <c>float2x4</c> type.</summary>
		public static readonly Matrix Float2x4 = new(4, 2);
		/// <summary>Predefined <c>float3x2</c> type.</summary>
		public static readonly Matrix Float3x2 = new(2, 3);
		/// <summary>Predefined <c>float3x3</c> type.</summary>
		public static readonly Matrix Float3x3 = new(3, 3);
		/// <summary>Predefined <c>float3x4</c> type.</summary>
		public static readonly Matrix Float3x4 = new(4, 3);
		/// <summary>Predefined <c>float4x2</c> type.</summary>
		public static readonly Matrix Float4x2 = new(2, 4);
		/// <summary>Predefined <c>float4x3</c> type.</summary>
		public static readonly Matrix Float4x3 = new(3, 4);
		/// <summary>Predefined <c>float4x4</c> type.</summary>
		public static readonly Matrix Float4x4 = new(4, 4);
	}
}


/// <summary>
/// Contains extensions for <see cref="ShaderType.NumericType"/> values.
/// </summary>
#if VSL_API_INTERNAL
internal
#else
public
#endif
static class NumericTypeExtensions
{
	/// <summary>Gets if the value type is the same as, or is implicitly converted to, the other type.</summary>
	/// <param name="from">The value type to assign/cast from.</param>
	/// <param name="to">The value type to assign/cast to.</param>
	public static bool CanAssignTo(this ShaderType.NumericType from, ShaderType.NumericType to) => (from == to) ||
		from switch {
			ShaderType.NumericType.Boolean  => false,                                // No implicit casts from bool
			ShaderType.NumericType.Signed   => to != ShaderType.NumericType.Boolean, // Signed -> not bool always works
			ShaderType.NumericType.Unsigned => to == ShaderType.NumericType.Float,   // Unsigned -> float allowed
			_ => false                                                               // Float -> float already handled
		};
}
