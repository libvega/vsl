﻿/*
 * Microsoft Public License (Ms-PL) - Copyright (c) 2022 Vega Authors
 * This file is subject to the terms and conditions of the Microsoft Public License, the text of which can be found in
 * the 'LICENSE' file at the root of this repository, or online at <https://opensource.org/licenses/MS-PL>.
 */

using System;
using System.Collections.Generic;

namespace VSL;


/// <summary>
/// The base record for all types that describe shader object types.
/// </summary>
#if VSL_API_INTERNAL
internal
#else
public
#endif
abstract partial record ShaderType
{
	#region Fields
	/// <summary>Gets if the type is an instance of <see cref="Void"/>.</summary>
	public bool IsVoid => this is Void;
	/// <summary>Gets if the type is an instance of <see cref="DataType"/>.</summary>
	public bool IsDataType => this is DataType;
	/// <summary>Gets if the type is an instance of <see cref="Numeric"/>.</summary>
	public bool IsNumeric => this is Numeric;
	/// <summary>Gets if the type is an instance of <see cref="Scalar"/>.</summary>
	public bool IsScalar => this is Scalar;
	/// <summary>Gets if the type is an instance of <see cref="Vector"/>.</summary>
	public bool IsVector => this is Vector;
	/// <summary>Gets if the type is an instance of <see cref="Matrix"/>.</summary>
	public bool IsMatrix => this is Matrix;
	/// <summary>Gets if the type is an instance of <see cref="ShaderType.Array"/>.</summary>
	public bool IsArray => this is Array;
	/// <summary>Gets if the type is an instance of <see cref="Struct"/>.</summary>
	public bool IsStruct => this is Struct;
	/// <summary>Gets if the type is an instance of <see cref="Binding"/>.</summary>
	public bool IsBinding => this is Binding;
	/// <summary>Gets if the type is an instance of <see cref="Sampler"/>.</summary>
	public bool IsSampler => this is Sampler;
	/// <summary>Gets if the type is an instance of <see cref="Image"/>.</summary>
	public bool IsImage => this is Image;
	/// <summary>Gets if the type is an instance of <see cref="TexelBuffer"/>.</summary>
	public bool IsTexelBuffer => this is TexelBuffer;
	/// <summary>Gets if the type is an instance of <see cref="Buffer"/>.</summary>
	public bool IsBuffer => this is Buffer;

	/// <summary>The name of the type as it appears in VSL source.</summary>
	public abstract string Name { get; }
	
	// The name of the type as it appears in GLSL source
	internal abstract string GLName { get; }
	#endregion // Fields

	#region Functions
	public sealed override string ToString() => Name;

	// Gets if the type can be indexed with the given type
	// Return=
	//    Type: The type produced by an index expression
	//    Format: The formatted string of the GLSL source for the index (uses $expr and $idx for replacement)
	internal virtual (ShaderType Type, string Format)? GetIndex(Numeric numeric, out string? err)
	{
		err = null;
		return null;
	}

	// Gets if the type can be accessed using the member text
	// write: if the member is being written to (matters for swizzles)
	// Return=
	//    Type: the type produced by the member expression
	//    Format: the formatted string of the GLSL source for the expression (uses $expr for replacement)
	internal virtual (ShaderType Type, string Format)? GetMember(string name, bool write, out string? err)
	{
		err = null;
		return null;
	}
	#endregion // Functions


	/// <summary>
	/// Represents the special <c>void</c> type.
	/// </summary>
	public sealed record Void : ShaderType
	{
		/// <summary>Pre-created instance of the void type.</summary>
		public static readonly Void Instance = new();

		public override string Name => "void";
		internal override string GLName => "void";
	}

	
	/// <summary>
	/// Special abstract type that represents numeric data types (limited to <see cref="Numeric"/>, 
	/// <see cref="Struct"/>, and <see cref="ShaderType.Array"/>).
	/// </summary>
	public abstract record DataType : ShaderType
	{
		/// <summary>The size of the data type, in bytes.</summary>
		public abstract uint Size { get; }
		/// <summary>The alignment rules for the data type, in bytes.</summary>
		public abstract uint Alignment { get; }
		/// <summary>The number of binding slots taken up by the type.</summary>
		/// <remarks>
		/// The general rule is scalars and vectors are 1 slot each, matrices take up the number of columns.
		/// </remarks>
		public abstract uint SlotCount { get; }

		// Gets if this data type can be assigned to the given data type
		internal abstract bool CanAssignTo(ShaderType target);
	}


	/// <summary>
	/// Represents an array of <see cref="DataType"/> values. Multi-dimensional arrays are implemented as nested
	/// <see cref="ShaderType.Array"/> types.
	/// </summary>
	/// <param name="ElementType">The type of the array elements.</param>
	/// <param name="Length">The number of elements in the array.</param>
	public sealed record Array(DataType ElementType, uint Length) : DataType
	{
		/// <summary>The maximum number of dimensions supported for array types.</summary>
		public const uint MAX_ARRAY_DEPTH = 4;

		#region Fields
		public override string Name => $"{ElementType}[{Length}]";
		internal override string GLName => ElementType.GLName;

		public override uint Size => ElementType.Size * Length;
		public override uint Alignment => ElementType.Alignment;
		public override uint SlotCount => ElementType.SlotCount * Length;

		/// <summary>Unwraps all levels of the array to get the base non-array element type.</summary>
		public DataType BaseElementType => (ElementType is Array arrType) ? arrType.BaseElementType : ElementType;

		/// <summary>Gets the total element count (length) of the array and all nested sub-arrays.</summary>
		public uint FullLength => (ElementType is Array arrType) ? Length * arrType.FullLength : Length;
		#endregion // Fields

		internal override bool CanAssignTo(ShaderType target) => false; // Array assignment not supported

		internal override (ShaderType Type, string Format)? GetIndex(Numeric numeric, out string? err)
		{
			if (numeric.IsInteger && numeric.IsScalar) {
				err = null;
				return (ElementType, "$expr[$idx]");
			}
			err = $"Array types can only be indexed with scalar integers";
			return null;
		}

		// Enumerates over this and all nested array lengths
		// Note: this enumeration is backwards from declaration order
		internal IEnumerable<uint> EnumerateLengths()
		{
			yield return Length;

			DataType curr = ElementType;
			while (curr is Array arrType) {
				yield return arrType.Length;
				curr = arrType.ElementType;
			}
		}
	}
}
