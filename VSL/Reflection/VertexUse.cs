﻿/*
 * Microsoft Public License (Ms-PL) - Copyright (c) 2022 Vega Authors
 * This file is subject to the terms and conditions of the Microsoft Public License, the text of which can be found in
 * the 'LICENSE' file at the root of this repository, or online at <https://opensource.org/licenses/MS-PL>.
 */

using System;
using System.Collections.Generic;
using System.Numerics;

namespace VSL;


/// <summary>
/// Supported vertex stage input usage semantics.
/// </summary>
#if VSL_API_INTERNAL
internal
#else
public
#endif
enum VertexUse : uint
{
	/// <summary>Position data (index 0).</summary>
	POS0     = 0,
	/// <summary>Position data (index 1).</summary>
	POS1     = 1,
	/// <summary>Color data (index 0).</summary>
	COLOR0   = 2,
	/// <summary>Color data (index 1).</summary>
	COLOR1   = 3,
	/// <summary>Color data (index 2).</summary>
	COLOR2   = 4,
	/// <summary>Color data (index 3).</summary>
	COLOR3   = 5,
	/// <summary>Normal data (index 0).</summary>
	NORM0    = 6,
	/// <summary>Normal data (index 1).</summary>
	NORM1    = 7,
	/// <summary>Tangent data (index 0).</summary>
	TAN0     = 8,
	/// <summary>Tangent data (index 1).</summary>
	TAN1     = 9,
	/// <summary>Binormal data (index 0).</summary>
	BINORM0  = 10,
	/// <summary>Binormal data (index 1).</summary>
	BINORM1  = 11,
	/// <summary>Texture coordinate (UV) data (index 0).</summary>
	UV0      = 12,
	/// <summary>Texture coordinate (UV) data (index 1).</summary>
	UV1      = 13,
	/// <summary>Texture coordinate (UV) data (index 2).</summary>
	UV2      = 14,
	/// <summary>Texture coordinate (UV) data (index 3).</summary>
	UV3      = 15,
	/// <summary>Blending weight data (index 0).</summary>
	BWEIGHT0 = 16,
	/// <summary>Blending weight data (index 1).</summary>
	BWEIGHT1 = 17,
	/// <summary>Blending weight data (index 2).</summary>
	BWEIGHT2 = 18,
	/// <summary>Blending weight data (index 3).</summary>
	BWEIGHT3 = 19,
	/// <summary>Blending index data (index 0).</summary>
	BINDEX0  = 20,
	/// <summary>Blending index data (index 1).</summary>
	BINDEX1  = 21,
	/// <summary>Blending index data (index 2).</summary>
	BINDEX2  = 22,
	/// <summary>Blending index data (index 3).</summary>
	BINDEX3  = 23,
	/// <summary>Extra user-defined data (index 0).</summary>
	EXTRA0   = 24,
	/// <summary>Extra user-defined data (index 1).</summary>
	EXTRA1   = 25,
	/// <summary>Instance rate data (index 0).</summary>
	INST0    = 26,
	/// <summary>Instance rate data (index 1).</summary>
	INST1    = 27,
	/// <summary>Instance rate data (index 2).</summary>
	INST2    = 28,
	/// <summary>Instance rate data (index 3).</summary>
	INST3    = 29,
	/// <summary>Instance rate data (index 4).</summary>
	INST4    = 30,
	/// <summary>Instance rate data (index 5).</summary>
	INST5    = 31
}


/// <summary>
/// Represents a mask of <see cref="VertexUse"/> values.
/// </summary>
#if VSL_API_INTERNAL
internal
#else
public
#endif
readonly record struct VertexUseMask
{
	#region Fields
	/// <summary>The raw mask value.</summary>
	public readonly uint Value;

	/// <summary>Gets the number of uses in the mask.</summary>
	public readonly uint Count => (uint)BitOperations.PopCount(Value);

	/// <summary>Gets if any uses are present in the mask.</summary>
	public readonly bool Any => Value != 0;
	#endregion // Fields

	/// <summary>Construct a default mask of no uses.</summary>
	public VertexUseMask() => Value = 0;
	/// <summary>Construct a mask with the given use.</summary>
	public VertexUseMask(VertexUse use) => Value = (1u << (int)use);
	internal VertexUseMask(uint value) => Value = value;

	public readonly override string ToString() => Value.ToString("X8");

	/// <summary>Gets if the use is present in the mask.</summary>
	public readonly bool Has(VertexUse use) => (Value & (1u << (int)use)) != 0;

	/// <summary>Enumerates over the uses present in the mask.</summary>
	public readonly IEnumerable<VertexUse> EnumerateUses()
	{
		for (uint shift = 0, mask = Value; mask != 0; ++shift, mask >>= 1) {
			if ((mask & 0x1) != 0) {
				yield return (VertexUse)shift;
			}
		}
	}

	#region Operators
	public static implicit operator VertexUseMask (VertexUse sem) => new(sem);

	public static VertexUseMask operator | (VertexUseMask l, VertexUse r) => new(l.Value | (1u << (int)r));

	public static VertexUseMask operator | (VertexUseMask l, VertexUseMask r) => new(l.Value | r.Value);

	public static VertexUseMask operator & (VertexUseMask l, VertexUseMask r) => new(l.Value & r.Value);
	#endregion // Operators
}


/// <summary>
/// Contains extensions for <see cref="VertexUse"/> values.
/// </summary>
#if VSL_API_INTERNAL
internal
#else
public
#endif
static class VertexUseExtensions
{
	/// <summary>Gets if the semantic defines instance-rate input data.</summary>
	public static bool IsInstance(this VertexUse semantic) =>
		(semantic >= VertexUse.INST0) && (semantic <= VertexUse.INST5);

	// Parses the semantic string (with the '#' prefix) (null if invalid)
	internal static VertexUse? FromSemanticString(string sem) => sem.ToUpperInvariant() switch {
		"#POS0"     => VertexUse.POS0,
		"#POS1"     => VertexUse.POS1,
		"#COLOR0"   => VertexUse.COLOR0,
		"#COLOR1"   => VertexUse.COLOR1,
		"#COLOR2"   => VertexUse.COLOR2,
		"#COLOR3"   => VertexUse.COLOR3,
		"#NORM0"    => VertexUse.NORM0,
		"#NORM1"    => VertexUse.NORM1,
		"#TAN0"     => VertexUse.TAN0,
		"#TAN1"     => VertexUse.TAN1,
		"#BINORM0"  => VertexUse.BINORM0,
		"#BINORM1"  => VertexUse.BINORM1,
		"#UV0"      => VertexUse.UV0,
		"#UV1"      => VertexUse.UV1,
		"#UV2"      => VertexUse.UV2,
		"#UV3"      => VertexUse.UV3,
		"#BWEIGHT0" => VertexUse.BWEIGHT0,
		"#BWEIGHT1" => VertexUse.BWEIGHT1,
		"#BWEIGHT2" => VertexUse.BWEIGHT2,
		"#BWEIGHT3" => VertexUse.BWEIGHT3,
		"#BINDEX0"  => VertexUse.BINDEX0,
		"#BINDEX1"  => VertexUse.BINDEX1,
		"#BINDEX2"  => VertexUse.BINDEX2,
		"#BINDEX3"  => VertexUse.BINDEX3,
		"#EXTRA0"   => VertexUse.EXTRA0,
		"#EXTRA1"   => VertexUse.EXTRA1,
		"#INST0"    => VertexUse.INST0,
		"#INST1"    => VertexUse.INST1,
		"#INST2"    => VertexUse.INST2,
		"#INST3"    => VertexUse.INST3,
		"#INST4"    => VertexUse.INST4,
		"#INST5"    => VertexUse.INST5,
		_ => null
	};
}
