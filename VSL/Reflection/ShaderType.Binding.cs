﻿/*
 * Microsoft Public License (Ms-PL) - Copyright (c) 2022 Vega Authors
 * This file is subject to the terms and conditions of the Microsoft Public License, the text of which can be found in
 * the 'LICENSE' file at the root of this repository, or online at <https://opensource.org/licenses/MS-PL>.
 */

using System;

namespace VSL;


// Defines the binding types
#if VSL_API_INTERNAL
internal
#else
public
#endif
abstract partial record ShaderType
{
	/// <summary>
	/// Base type for all types that appear only as bindings (all resource ref types).
	/// </summary>
	public abstract record Binding : ShaderType
	{
		// The name of the binding table as it appears in GLSL source
		internal abstract string TableName { get; }

		// Gets the GLSL source for accessing the binding variable
		internal abstract string GetAccessSource(string bindName, uint bindSlot);

		// Gets if the types are the same, minus a generic argument
		internal abstract bool IsSameGeneric(Binding other);
	}


	/// <summary>
	/// Represents a read-only sampled texture.
	/// </summary>
	/// <param name="Rank">The rank of the contained texel data.</param>
	/// <param name="Format">
	/// The format of the contained texel data. Always one of <see cref="TexelFormat.Float4"/>, 
	/// <see cref="TexelFormat.Int4"/>, or <see cref="TexelFormat.UInt4"/>.
	/// </param>
	public sealed record Sampler(TexelRank Rank, TexelFormat Format) : Binding
	{
		public override string Name => $"{Format.GetSourcePrefix()}Sampler{Rank.GetSourceName()}";
		internal override string GLName => 
			$"{Format.GetSourcePrefix().ToLowerInvariant()}sampler{Rank.GetSourceName()}";

		internal override string TableName => $"_{Name.ToUpperInvariant()}_TABLE_";

		internal override string GetAccessSource(string bindName, uint bindSlot) => $"{TableName}[_t{bindSlot}idx_]";

		internal override bool IsSameGeneric(Binding other) =>
			(other is Sampler samp) && (Rank == samp.Rank); // Not technically a "generic" but its the same concept

		internal override (ShaderType Type, string Format)? GetIndex(Numeric numeric, out string? err)
		{
			if (numeric.IsInteger) {
				if (numeric.Rows != Rank.GetDimensionCount()) {
					err = $"Invalid vector size for sampler access (expected {Rank.GetDimensionCount()}, " +
						$"got {numeric.Rows})";
					return null;
				}
				err = null;
				return (Format.GetDataType(), "texelFetch($expr, $idx, 0)");
			}
			err = $"Cannot fetch texel from sampler with non-integer index";
			return null;
		}

		/// <summary>Gets the type instance for the given arguments, or <c>null</c>.</summary>
		/// <param name="rank">The rank of the sampler type.</param>
		/// <param name="format">The format of the sampler type.</param>
		public static Sampler? Get(TexelRank rank, TexelFormat format) => (rank, format) switch {
			(TexelRank.E1D,      TexelFormat.Float4) => Sampler1D,
			(TexelRank.E2D,      TexelFormat.Float4) => Sampler2D,
			(TexelRank.E3D,      TexelFormat.Float4) => Sampler3D,
			(TexelRank.E2DArray, TexelFormat.Float4) => Sampler2DArray,
			(TexelRank.E1D,      TexelFormat.Int4)   => ISampler1D,
			(TexelRank.E2D,      TexelFormat.Int4)   => ISampler2D,
			(TexelRank.E3D,      TexelFormat.Int4)   => ISampler3D,
			(TexelRank.E2DArray, TexelFormat.Int4)   => ISampler2DArray,
			(TexelRank.E1D,      TexelFormat.UInt4)  => USampler1D,
			(TexelRank.E2D,      TexelFormat.UInt4)  => USampler2D,
			(TexelRank.E3D,      TexelFormat.UInt4)  => USampler3D,
			(TexelRank.E2DArray, TexelFormat.UInt4)  => USampler2DArray,
			_ => null
		};

		/// <summary>Pre-defined type for the 1D float sampler.</summary>
		public static readonly Sampler Sampler1D       = new(TexelRank.E1D, TexelFormat.Float4);
		/// <summary>Pre-defined type for the 2D float sampler.</summary>
		public static readonly Sampler Sampler2D       = new(TexelRank.E2D, TexelFormat.Float4);
		/// <summary>Pre-defined type for the 3D float sampler.</summary>
		public static readonly Sampler Sampler3D       = new(TexelRank.E3D, TexelFormat.Float4);
		/// <summary>Pre-defined type for the 2D array float sampler.</summary>
		public static readonly Sampler Sampler2DArray  = new(TexelRank.E2DArray, TexelFormat.Float4);
		/// <summary>Pre-defined type for the 1D signed int sampler.</summary>
		public static readonly Sampler ISampler1D      = new(TexelRank.E1D, TexelFormat.Int4);
		/// <summary>Pre-defined type for the 2D signed int sampler.</summary>
		public static readonly Sampler ISampler2D      = new(TexelRank.E2D, TexelFormat.Int4);
		/// <summary>Pre-defined type for the 3D signed int sampler.</summary>
		public static readonly Sampler ISampler3D      = new(TexelRank.E3D, TexelFormat.Int4);
		/// <summary>Pre-defined type for the 2D array signed int sampler.</summary>
		public static readonly Sampler ISampler2DArray = new(TexelRank.E2DArray, TexelFormat.Int4);
		/// <summary>Pre-defined type for the 1D unsigned int sampler.</summary>
		public static readonly Sampler USampler1D      = new(TexelRank.E1D, TexelFormat.UInt4);
		/// <summary>Pre-defined type for the 2D unsigned int sampler.</summary>
		public static readonly Sampler USampler2D      = new(TexelRank.E2D, TexelFormat.UInt4);
		/// <summary>Pre-defined type for the 3D unsigned int sampler.</summary>
		public static readonly Sampler USampler3D      = new(TexelRank.E3D, TexelFormat.UInt4);
		/// <summary>Pre-defined type for the 2D array float sampler.</summary>
		public static readonly Sampler USampler2DArray = new(TexelRank.E2DArray, TexelFormat.UInt4);
	}


	/// <summary>
	/// Represents a read/write image with a known texel format.
	/// </summary>
	/// <param name="Rank">The rank of the contained texel data.</param>
	/// <param name="Format">The format of the contained texel data.</param>
	public sealed record Image(TexelRank Rank, TexelFormat Format) : Binding
	{
		public override string Name => 
			$"{Format.GetSourcePrefix()}Image{Rank.GetSourceName()}<{Format.GetSourceName().VSL}>";
		internal override string GLName => 
			$"{Format.GetSourcePrefix().ToLowerInvariant()}image{Rank.GetSourceName()}";

		internal override string TableName => 
			$"_{GLName.ToUpperInvariant()}_{Format.GetSourceName().VSL.ToUpperInvariant()}_TABLE_";

		internal override string GetAccessSource(string bindName, uint bindSlot) => $"{TableName}[_t{bindSlot}idx_]";

		internal override bool IsSameGeneric(Binding other) => (other is Image img) && (Rank == img.Rank);

		internal override (ShaderType Type, string Format)? GetIndex(Numeric numeric, out string? err)
		{
			if (numeric.IsInteger) {
				if (numeric.Rows != Rank.GetDimensionCount()) {
					err = $"Invalid vector size for image access (expected {Rank.GetDimensionCount()}, " +
						$"got {numeric.Rows})";
					return null;
				}
				var texType = Format.GetDataType();
				var swizzle = texType.Rows switch {
					1 => ".x",
					2 => ".xy",
					_ => String.Empty
				};
				err = null;
				return (texType, $"(imageLoad($expr, $idx){swizzle})");
			}
			err = $"Cannot fetch texel from image with non-integer indexer";
			return null;
		}
	}


	/// <summary>
	/// Represents a 1D buffer of texel formatted data, optionally read-only.
	/// </summary>
	/// <param name="Format">The format of the contained texel data.</param>
	/// <param name="ReadOnly">If the texel data is read-only in the shader.</param>
	public sealed record TexelBuffer(TexelFormat Format, bool ReadOnly) : Binding
	{
		public override string Name => $"{(ReadOnly ? "const " : "")}TexelBuffer<{Format.GetSourceName().VSL}>";
		internal override string GLName => $"{Format.GetSourcePrefix().ToLowerInvariant()}imageBuffer";

		internal override string TableName => 
			$"_TEXELBUFFER_{Format.GetSourceName().VSL.ToUpperInvariant()}_TABLE_";

		internal override string GetAccessSource(string bindName, uint bindSlot) => $"{TableName}[_t{bindSlot}idx_]";

		internal override bool IsSameGeneric(Binding other) => other is TexelBuffer;

		internal override (ShaderType Type, string Format)? GetIndex(Numeric numeric, out string? err)
		{
			if (numeric.IsInteger && numeric.IsScalar) {
				err = null;
				var texType = Format.GetDataType();
				var swizzle = texType.Rows switch {
					1 => ".x",
					2 => ".xy",
					_ => String.Empty
				};
				return (texType, $"(imageLoad($expr, $idx){swizzle})");
			}
			err = "Texel buffer indexer must be a scalar integer";
			return null;
		}
	}


	/// <summary>
	/// Represents a 1D buffer of generic structured data, optionally read-only.
	/// </summary>
	/// <param name="ElementType">The data type at each buffer element.</param>
	/// <param name="ReadOnly">If the buffer data is read-only.</param>
	public sealed record Buffer(DataType ElementType, bool ReadOnly) : Binding
	{
		public override string Name => $"{(ReadOnly ? "const " : "")}Buffer<{ElementType}>";
		internal override string GLName => $"_BUFFER_{ElementType.GLName.ToUpperInvariant()}_ptr_";

		internal override string TableName => GLName;

		internal override string GetAccessSource(string bindName, uint bindSlot) => $"_buffer{bindSlot}_";

		internal override bool IsSameGeneric(Binding other) => (other is Buffer buf) && (ReadOnly == buf.ReadOnly);

		internal override (ShaderType Type, string Format)? GetIndex(Numeric numeric, out string? err)
		{
			if (numeric.IsInteger && numeric.IsScalar) {
				err = null;
				return (ElementType, "$expr[$idx].data");
			}
			err = "Buffer indexer must be a scalar integer";
			return null;
		}
	}
}
