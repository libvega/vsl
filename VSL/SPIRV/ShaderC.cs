﻿/*
 * Microsoft Public License (Ms-PL) - Copyright (c) 2022 Vega Authors
 * This file is subject to the terms and conditions of the Microsoft Public License, the text of which can be found in
 * the 'LICENSE' file at the root of this repository, or online at <https://opensource.org/licenses/MS-PL>.
 */

using System;
using System.Collections.Generic;
using System.IO;
using System.Runtime.InteropServices;
using System.Text;

namespace VSL;


/// <summary>
/// Interface with the native ShaderC library.
/// </summary>
public unsafe static class ShaderC
{
	#region Fields
	/// <summary>Gets if the Vulkan SDK is installed and was found on the current system.</summary>
	public static bool VulkanSDKFound { get; private set; } = false;
	/// <summary>Gets if the library was found in the Vulkan SDK and loaded.</summary>
	public static bool IsLoaded => LibraryHandle != IntPtr.Zero;

	// Handle to the ShaderC library
	internal static IntPtr LibraryHandle { get; private set; } = IntPtr.Zero;
	#endregion // Fields

	#region ShaderC API
	internal static IntPtr CompilerInitilize() => _shaderc_compiler_initialize();

	internal static void CompilerRelease(IntPtr ptr) => _shaderc_compiler_release(ptr);

	internal static IntPtr CompileOptionsInitialize() => _shaderc_compile_options_initialize();

	internal static void CompileOptionsRelease(IntPtr ptr) => _shaderc_compile_options_release(ptr);

	internal static void CompileOptionsSetSourceLanguage(IntPtr ptr, source_language lang) =>
		_shaderc_compile_options_set_source_language(ptr, lang);

	internal static void CompileOptionsSetOptimizationLevel(IntPtr ptr, optimization_level level) =>
		_shaderc_compile_options_set_optimization_level(ptr, level);

	internal static void CompileOptionsSetTargetEnv(IntPtr ptr, target_env env, env_version version) =>
		_shaderc_compile_options_set_target_env(ptr, env, version);

	internal static IntPtr CompileIntoSpv(IntPtr compiler, shader_kind kind, string source, string inputName,
		string entryPoint, IntPtr options) 
	{
		// Get the utf-8 versions of the strings (*4 is worst case scenario for utf-8, +1 for null terminator)
		// We do not use stackalloc for the glsl source as it can easily cause a stack overflow for long shaders
		var sourceUtf = Encoding.UTF8.GetBytes(source);
		var inputNameUtf = stackalloc byte[(inputName.Length + 1) * 4];
		var entryPointUtf = stackalloc byte[(entryPoint.Length + 1) * 4];
		var inputNameLen = Encoding.UTF8.GetBytes(inputName.AsSpan(), new(inputNameUtf, (inputName.Length + 1) * 4));
		var entryPointLen = Encoding.UTF8.GetBytes(entryPoint.AsSpan(), new(entryPointUtf, (entryPoint.Length + 1) * 4));
		inputNameUtf[inputNameLen] = 0;
		entryPointUtf[entryPointLen] = 0;

		// Call the compiler
		fixed (byte* sourcePtr = sourceUtf) {
			return _shaderc_compile_into_spv(
				compiler, sourcePtr, (ulong)sourceUtf.Length, kind, inputNameUtf, entryPointUtf, options
			);
		}
	}

	internal static void ResultRelease(IntPtr ptr) => _shaderc_result_release(ptr);

	internal static ulong ResultGetLength(IntPtr ptr) => _shaderc_result_get_length(ptr);

	internal static byte* ResultGetBytes(IntPtr ptr) => _shaderc_result_get_bytes(ptr);

	internal static compilation_status ResultGetCompilationStatus(IntPtr ptr) => _shaderc_result_get_compilation_status(ptr);

	internal static string ResultGetErrorMessage(IntPtr ptr)
	{
		var msg = _shaderc_result_get_error_message(ptr);
		return Marshal.PtrToStringUTF8(new IntPtr(msg))!;
	}
	#endregion // ShaderC API

	static ShaderC()
	{
		// Check for the Vulkan SDK (TODO: look into making this more exhaustive)
		var sdkPath =
			Environment.GetEnvironmentVariable("VULKAN_SDK") ??
			Environment.GetEnvironmentVariable("VK_SDK_PATH");
		if (sdkPath is null) {
			VulkanSDKFound = false;
			LibraryHandle = IntPtr.Zero;
			return;
		}
		VulkanSDKFound = true;

		// Try to load the library
		foreach (var libPath in GetLibraryCheckPaths(sdkPath)) {
			if (NativeLibrary.TryLoad(libPath, out var libHandle)) {
				LibraryHandle = libHandle;
				break;
			}
		}
		if (LibraryHandle == IntPtr.Zero) {
			return;
		}

		// Load the function handles
		_shaderc_compiler_initialize =
			(delegate* unmanaged<IntPtr>)
			NativeLibrary.GetExport(LibraryHandle, "shaderc_compiler_initialize").ToPointer();
		_shaderc_compiler_release =
			(delegate* unmanaged<IntPtr, void>)
			NativeLibrary.GetExport(LibraryHandle, "shaderc_compiler_release").ToPointer();
		_shaderc_compile_options_initialize =
			(delegate* unmanaged<IntPtr>)
			NativeLibrary.GetExport(LibraryHandle, "shaderc_compile_options_initialize").ToPointer();
		_shaderc_compile_options_release =
			(delegate* unmanaged<IntPtr, void>)
			NativeLibrary.GetExport(LibraryHandle, "shaderc_compile_options_release").ToPointer();
		_shaderc_compile_options_set_source_language =
			(delegate* unmanaged<IntPtr, source_language, void>)
			NativeLibrary.GetExport(LibraryHandle, "shaderc_compile_options_set_source_language").ToPointer();
		_shaderc_compile_options_set_optimization_level =
			(delegate* unmanaged<IntPtr, optimization_level, void>)
			NativeLibrary.GetExport(LibraryHandle, "shaderc_compile_options_set_optimization_level").ToPointer();
		_shaderc_compile_options_set_target_env =
			(delegate* unmanaged<IntPtr, target_env, env_version, void>)
			NativeLibrary.GetExport(LibraryHandle, "shaderc_compile_options_set_target_env").ToPointer();
		_shaderc_compile_into_spv =
			(delegate* unmanaged<IntPtr, byte*, ulong, shader_kind, byte*, byte*, IntPtr, IntPtr>)
			NativeLibrary.GetExport(LibraryHandle, "shaderc_compile_into_spv").ToPointer();
		_shaderc_result_release =
			(delegate* unmanaged<IntPtr, void>)
			NativeLibrary.GetExport(LibraryHandle, "shaderc_result_release").ToPointer();
		_shaderc_result_get_length =
			(delegate* unmanaged<IntPtr, ulong>)
			NativeLibrary.GetExport(LibraryHandle, "shaderc_result_get_length").ToPointer();
		_shaderc_result_get_bytes =
			(delegate* unmanaged<IntPtr, byte*>)
			NativeLibrary.GetExport(LibraryHandle, "shaderc_result_get_bytes").ToPointer();
		_shaderc_result_get_compilation_status =
			(delegate* unmanaged<IntPtr, compilation_status>)
			NativeLibrary.GetExport(LibraryHandle, "shaderc_result_get_compilation_status").ToPointer();
		_shaderc_result_get_error_message =
			(delegate* unmanaged<IntPtr, byte*>)
			NativeLibrary.GetExport(LibraryHandle, "shaderc_result_get_error_message").ToPointer();
	}

	private static IEnumerable<string> GetLibraryCheckPaths(string sdkPath)
	{
		// TODO: look into making this more exhaustive
		if (RuntimeInformation.IsOSPlatform(OSPlatform.Windows)) {
			yield return Path.Combine(sdkPath, "Bin", "shaderc_shared.dll");
			yield return Path.Combine(sdkPath, "Bin", "shaderc.dll");
		}
		else if (RuntimeInformation.IsOSPlatform(OSPlatform.Linux)) {
			yield return Path.Combine(sdkPath, "lib", "libshaderc_shared.so");
		}
		else {
			// TODO: Add support for the SDK on macOS
		}
	}

	#region ShaderC Functions and Types
	internal enum source_language : int
	{
		glsl = 0,
		hlsl = 1
	}

	internal enum optimization_level : int
	{
		zero = 0,
		size = 1,
		performance = 2
	}

	internal enum target_env : int
	{
		vulkan = 0
	}

	internal enum env_version : int
	{
		vulkan_1_0 = 1 << 22,
		vulkan_1_1 = (1 << 22) | (1 << 12),
		vulkan_1_2 = (1 << 22) | (2 << 12),
		vulkan_1_3 = (1 << 22) | (3 << 12)
	}

	internal enum shader_kind : int
	{
		vertex_shader = 0,
		fragment_shader = 1,
		compute_shader = 2,
		geometry_shader = 3,
		tess_control_shader = 4,
		tess_evaluation_shader = 5
	}

	internal enum compilation_status : int
	{
		success = 0,
		invalid_stage = 1,
		compilation_error = 2,
		internal_error = 3,
		null_result_object = 4,
		invalid_assembly = 5,
		validation_error = 6,
		transformation_error = 7,
		configuration_error = 8
	}

	private static readonly delegate* unmanaged<IntPtr> _shaderc_compiler_initialize;
	private static readonly delegate* unmanaged<IntPtr, void> _shaderc_compiler_release;

	private static readonly delegate* unmanaged<IntPtr> _shaderc_compile_options_initialize;
	private static readonly delegate* unmanaged<IntPtr, void> _shaderc_compile_options_release;
	private static readonly delegate* unmanaged<IntPtr, source_language, void> _shaderc_compile_options_set_source_language;
	private static readonly delegate* unmanaged<IntPtr, optimization_level, void> _shaderc_compile_options_set_optimization_level;
	private static readonly delegate* unmanaged<IntPtr, target_env, env_version, void> _shaderc_compile_options_set_target_env;

	private static readonly delegate* unmanaged<IntPtr, byte*, ulong, shader_kind, byte*, byte*, IntPtr, IntPtr> _shaderc_compile_into_spv;
	private static readonly delegate* unmanaged<IntPtr, void> _shaderc_result_release;
	private static readonly delegate* unmanaged<IntPtr, ulong> _shaderc_result_get_length;
	private static readonly delegate* unmanaged<IntPtr, byte*> _shaderc_result_get_bytes;
	private static readonly delegate* unmanaged<IntPtr, compilation_status> _shaderc_result_get_compilation_status;
	private static readonly delegate* unmanaged<IntPtr, byte*> _shaderc_result_get_error_message;
	#endregion // ShaderC Functions and Types
}
