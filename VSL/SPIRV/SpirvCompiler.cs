﻿/*
 * Microsoft Public License (Ms-PL) - Copyright (c) 2022 Vega Authors
 * This file is subject to the terms and conditions of the Microsoft Public License, the text of which can be found in
 * the 'LICENSE' file at the root of this repository, or online at <https://opensource.org/licenses/MS-PL>.
 */

using System;
using System.Diagnostics;

namespace VSL;

// Manages compiling the generated GLSL into SPIR-V bytecode
internal class SpirvCompiler : IDisposable
{
	#region Fields
	// Needed as a "tag" by ShaderC when compiling, file name or blank if compiled from source
	public readonly string InputName;

	// Unmanaged compiler and options objects
	private IntPtr _compilerHandle = IntPtr.Zero;
	private IntPtr _optionsHandle = IntPtr.Zero;
	#endregion // Fields

	public SpirvCompiler(string? inputName)
	{
		InputName = inputName ?? "source";

		// Create the unmanaged objects
		_compilerHandle = ShaderC.CompilerInitilize();
		_optionsHandle = ShaderC.CompileOptionsInitialize();

		// Setup default compiler options
		ShaderC.CompileOptionsSetSourceLanguage(_optionsHandle, ShaderC.source_language.glsl);
		ShaderC.CompileOptionsSetOptimizationLevel(_optionsHandle, ShaderC.optimization_level.performance);
		ShaderC.CompileOptionsSetTargetEnv(_optionsHandle, ShaderC.target_env.vulkan, ShaderC.env_version.vulkan_1_2);
	}
	~SpirvCompiler()
	{
		dispose(false);
	}

	// Converts the glsl into a utf-8 C-string and passes it to ShaderC to compile to bytecode
	public unsafe uint[] CompileGLSL(string glsl, ShaderStage stage)
	{
		// Get the shader type
		var kind = stage switch {
			ShaderStage.Vertex   => ShaderC.shader_kind.vertex_shader,
			ShaderStage.Fragment => ShaderC.shader_kind.fragment_shader,
			_ => throw new ArgumentOutOfRangeException(nameof(stage))
		};

		// Wrap since we are working with unmanaged resources
		IntPtr result = IntPtr.Zero;
		try {
			// Perform the compilation
			result = ShaderC.CompileIntoSpv(_compilerHandle, kind, glsl, InputName, "main", _optionsHandle);

			// Check result
			var rescode = ShaderC.ResultGetCompilationStatus(result);
			if (rescode != ShaderC.compilation_status.success) {
				var errMsg = ShaderC.ResultGetErrorMessage(result);
				errMsg = $"Invalid GLSL found (THIS IS A COMPILER BUG): [{rescode}] - {errMsg}";
				Trace.Fail(errMsg);
			}

			// Get the result data
			var byteCount = ShaderC.ResultGetLength(result);
			var codePtr = ShaderC.ResultGetBytes(result);
			var byteCode = new uint[byteCount / sizeof(uint)];
			fixed (uint* byteCodePtr = byteCode) {
				Buffer.MemoryCopy(codePtr, byteCodePtr, byteCount, byteCount);
			}

			// Return the raw bytecode
			return byteCode;
		}
		finally {
			if (result != IntPtr.Zero) {
				ShaderC.ResultRelease(result);
			}
		}
	}

	#region IDisposable
	public void Dispose()
	{
		dispose(true);
		GC.SuppressFinalize(this);
	}

	private void dispose(bool disposing)
	{
		if (_optionsHandle != IntPtr.Zero) {
			ShaderC.CompileOptionsRelease(_optionsHandle);
			_optionsHandle = IntPtr.Zero;
		}
		if (_compilerHandle != IntPtr.Zero) {
			ShaderC.CompilerRelease(_compilerHandle);
			_compilerHandle = IntPtr.Zero;
		}
	}
	#endregion // IDisposable
}
