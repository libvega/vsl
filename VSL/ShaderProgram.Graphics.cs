﻿/*
 * Microsoft Public License (Ms-PL) - Copyright (c) 2022 Vega Authors
 * This file is subject to the terms and conditions of the Microsoft Public License, the text of which can be found in
 * the 'LICENSE' file at the root of this repository, or online at <https://opensource.org/licenses/MS-PL>.
 */

using System;
using System.Collections.Generic;
using System.Linq;

namespace VSL;


// Implements the graphics shader specific form of a shader program
#if VSL_API_INTERNAL
internal
#else
public
#endif
abstract partial class ShaderProgram
{
	/// <summary>
	/// Represents a shader program with <see cref="ShaderKind.Graphics"/>.
	/// </summary>
	public sealed class Graphics : ShaderProgram
	{
		/// <summary>The maximum number of fragment stage outputs.</summary>
		public const uint MAX_FRAGMENT_OUTPUTS = 4;
		/// <summary>The maximum number of pixel inputs.</summary>
		public const uint MAX_PIXEL_INPUTS = 4;
		/// <summary>The maximum number of "slots" for vertex inputs.</summary>
		public const uint MAX_VERTEX_SLOTS = 16;
		/// <summary>The maximum number of "slots" for local variables.</summary>
		public const uint MAX_LOCAL_SLOTS = 16;

		#region Fields
		/// <summary>The list of inputs (vertex attributes), sorted by assigned slot.</summary>
		public IReadOnlyList<Input> Inputs => _inputs;
		private readonly List<Input> _inputs;
		/// <summary>The mask of vertex uses present in the shader.</summary>
		public readonly VertexUseMask InputMask;

		/// <summary>The list of outputs (fragment attachments), sorted by index.</summary>
		public IReadOnlyList<Output> Outputs => _outputs;
		private readonly List<Output> _outputs;

		/// <summary>The list of pixel inputs, sorted by index.</summary>
		public IReadOnlyList<PixelInput> PixelInputs => _pixelInputs;
		private readonly List<PixelInput> _pixelInputs;
		#endregion // Fields

		internal Graphics(
			Uniform? uniform,
			PushConstant? pc,
			List<Binding> bindings,
			List<Input> inputs,
			List<Output> outputs,
			List<PixelInput> pixelInputs,
			Dictionary<ShaderStage, uint[]> bytecode
		)
			: base(ShaderKind.Graphics, uniform, pc, bindings, bytecode)
		{
			_inputs = inputs;
			_inputs.Sort((l, r) => l.Slot.CompareTo(r.Slot));
			InputMask = _inputs.Aggregate(default(VertexUseMask), (mask, input) => mask | input.Use);

			_outputs = outputs;
			_outputs.Sort((l, r) => l.Index.CompareTo(r.Index));

			_pixelInputs = pixelInputs;
			_pixelInputs.Sort((l, r) => l.Index.CompareTo(r.Index));
		}

		#region Reflection
		/// <summary>Provides information about a graphics shader input (vertex attribute).</summary>
		/// <param name="Type">The vertex attribute type.</param>
		/// <param name="Use">The semantic use for the input.</param>
		/// <param name="Slot">The binding location assigned to the input.</param>
		public sealed record Input(ShaderType.DataType Type, VertexUse Use, uint Slot);

		/// <summary>Provides information about a graphics shader output (fragment output).</summary>
		/// <param name="Type">The numeric type used by the fragment output target.</param>
		/// <param name="Index">The output index.</param>
		/// <param name="Blend">The default blend state specified for the output.</param>
		public sealed record Output(ShaderType.Numeric Type, uint Index, BlendState Blend);

		/// <summary>Provides information about a graphics shader fragment stage pixel input.</summary>
		/// <param name="Type">The base numeric type read from the pixel input.</param>
		/// <param name="Index">The pixel input index.</param>
		public sealed record PixelInput(ShaderType.NumericType Type, uint Index);
		#endregion // Reflection
	}
}
