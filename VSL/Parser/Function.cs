﻿/*
 * Microsoft Public License (Ms-PL) - Copyright (c) 2022 Vega Authors
 * This file is subject to the terms and conditions of the Microsoft Public License, the text of which can be found in
 * the 'LICENSE' file at the root of this repository, or online at <https://opensource.org/licenses/MS-PL>.
 */

using System;
using System.Collections.Generic;
using System.Linq;
using Antlr4.Runtime;

namespace VSL;


// Describes a shader function with a name and collection of overloads
internal sealed partial record Function(string Name, Function.Entry[] Entries)
{
	// Describes the reference level/kind for a function parameter
	public enum RefType { In = 0, InOut = 1, Out = 2 }

	// Describes a specific overload of a named function
	public sealed record Entry(string GLName, ShaderType ReturnType, Param[] Params)
	{
		public bool IsTemplate => GLName.Contains('$');

		public Entry(string glslName, ShaderType retType, params ShaderType[] paramTypes)
			: this(glslName, retType, paramTypes.Select(pt => new Param(String.Empty, pt, false)).ToArray())
		{ }

		public Entry SetGeneric(uint index)
		{
			Params[index] = Params[index] with { Generic = true };
			return this;
		}

		public Entry SetRef(uint index, RefType @ref)
		{
			Params[index] = Params[index] with { Ref = @ref };
			return this;
		}
	}

	// Describes a typed function parameter
	public sealed record Param(string Name, ShaderType Type, bool Generic, RefType Ref = RefType.In);


	// Checks the function entries to see if any match the given set of argument types
	public Entry? FindEntry(IReadOnlyList<(ShaderType Type, RefType Ref)> argTypes)
	{
		// Find the entry that matches the most number of argument types exactly
		uint bestMatchCount = 0;
		Entry? bestEntry = null;
		foreach (var entry in Entries) {
			if (entry.Params.Length != argTypes.Count) {
				continue;
			}

			uint matchCount = 0, castCount = 0;
			for (int ai = 0; ai < entry.Params.Length; ++ai) {
				var param = entry.Params[ai];
				var arg = argTypes[ai];
				if (param.Ref != arg.Ref) {
					break;
				}

				if (param.Generic) {
					var paramBind = param.Type as ShaderType.Binding;
					var argBind = arg.Type as ShaderType.Binding;
					if (argBind is null) {
						break; // No possible match
					}

					if (paramBind!.IsSameGeneric(argBind)) {
						matchCount += 1;
					}
					else {
						break;
					}
				}
				else if (param.Type == arg.Type) {
					matchCount += 1;
				}
				else if (param.Type is ShaderType.Numeric paramNum) {
					if (arg.Type is not ShaderType.Numeric argNum) {
						break;
					}
					if (argNum.CanAssignTo(paramNum)) {
						castCount += 1;
					}
					else {
						break;
					}
				}
				else {
					break;
				}
			}
			if ((matchCount + castCount) != entry.Params.Length) {
				continue; // At least one arg didnt match, skip this entry
			}

			if (matchCount > bestMatchCount) {
				bestMatchCount = matchCount;
				bestEntry = entry;
			}
			else if (bestMatchCount == 0 && castCount > 0) {
				bestEntry = entry; // All args can be cast to the param types, so this one is a good option
			}
		}

		return bestEntry;
	}

	// Processes the function call into an expression
	public Expr.FunctionCall? ProcessEntry(IReadOnlyList<(Expr Expr, RefType Ref)> exprs)
	{
		// Find the entry that matches the expressions
		var entry = FindEntry(exprs.Select(e => (e.Expr.Type, e.Ref)).ToList());
		if (entry is null) {
			return null; // No matching entry
		}

		// Build the new expression
		if (entry.IsTemplate) {
			var template = entry.GLName;
			for (int ai = 0; ai < exprs.Count; ++ai) {
				template = template.Replace($"${ai}", exprs[ai].Expr.Source);
			}
			return new(template, entry, exprs.Select(e => e.Expr).ToArray(), null!);
		}
		else {
			return new($"{entry.GLName}({String.Join(", ", exprs.Select(e => e.Expr.Source))})", entry, 
				exprs.Select(e => e.Expr).ToArray(), null!);
		}
	}

	// Performs a function lookup and tries to find a matching entry to the expressions
	public static Expr ProcessFunctionCall(ShaderParser parser, IToken funcName, 
		IReadOnlyList<(Expr Expr, RefType Ref)> exprs)
	{
		if (_Builtins.TryGetValue(funcName.Text, out var func)) {
			var res = func.ProcessEntry(exprs);
			if (res is not null) {
				return res;
			}
			else {
				throw parser.Fatal(funcName, $"No overload of '{funcName.Text}' matches the given arguments");
			}
		}
		throw parser.Fatal(funcName, $"No function '{funcName.Text}' exists in the current shader");
	}

	// Performs the checks for a type constructor
	public static Expr.ConstructorCall ProcessConstructor(ShaderParser parser, ShaderType type, IToken typeName, 
		IReadOnlyList<Expr> argExprs)
	{
		// Can only construct numeric types
		if (type is not ShaderType.Numeric numType) {
			throw parser.Fatal(typeName, $"Cannot construct non-numeric type '{type}'");
		}
		var ctorSize = numType.Rows * numType.Cols;

		// Numeric ctor args must all be numeric themselves
		int compCount = 0;
		var argNumTypes = new ShaderType.Numeric[argExprs.Count];
		for (int ai = 0; ai < argExprs.Count; ++ai) {
			var arg = argExprs[ai];
			if (arg.Type is not ShaderType.Numeric argNumType) {
				throw parser.Fatal(arg.Context, $"Constructor argument {ai + 1} must be a numeric type");
			}
			compCount += (int)(argNumType.Rows * argNumType.Cols);
			argNumTypes[ai] = argNumType;
		}

		// If there is only one argument, then we might be dealing with a cast or single-value ctor
		var isCast = argExprs.Count == 1 && compCount == ctorSize;
		var isValueCtor = argExprs.Count == 1 && compCount == 1;
		if (isCast) {
			if (!argNumTypes[0].CanAssignTo(numType)) {
				parser.Fatal(argExprs[0].Context, $"No explicit cast exists for {argNumTypes[0]} to {numType}");
			}
			return new($"{numType.GLName}({argExprs[0].Source})", numType, argExprs.ToArray(), null!);
		}
		else if (isValueCtor) {
			var ctorScalar = ShaderType.Numeric.Get(numType.Type, 1, 1);
			var argScalar = ShaderType.Numeric.Get(argNumTypes[0].Type, 1, 1);
			if (!argScalar!.CanAssignTo(ctorScalar!)) {
				throw parser.Fatal(argExprs[0].Context, 
					$"No cast for argument type {argScalar} to ctor base type {ctorScalar}");
			}
			return new($"{numType.GLName}({argExprs[0].Source})", numType, argExprs.ToArray(), null!);
		}
		else { // Otherwise, we just need to make sure the argument types are castable and enough components exist
			if (compCount != ctorSize) {
				throw parser.Fatal(typeName, 
					$"Constructor component count mismatch ({ctorSize} expected, {compCount} actual)");
			}

			var ctorScalar = ShaderType.Numeric.Get(numType.Type, 1, 1);
			for (int ai = 0; ai < argNumTypes.Length; ++ai) {
				var argType = argNumTypes[ai];
				var argScalar = (argType as ShaderType.Scalar) ?? ShaderType.Numeric.Get(argType.Type, 1, 1);
				if (!argScalar!.CanAssignTo(ctorScalar!)) {
					throw parser.Fatal(argExprs[ai].Context, 
						$"No cast for argument type {argScalar} to ctor base type {ctorScalar}");
				}
			}

			return new($"{numType.GLName}({String.Join(", ", argExprs.Select(e => e.Source))})", numType, 
				argExprs.ToArray(), null!);
		}
	}
}
