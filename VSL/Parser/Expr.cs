﻿/*
 * Microsoft Public License (Ms-PL) - Copyright (c) 2022 Vega Authors
 * This file is subject to the terms and conditions of the Microsoft Public License, the text of which can be found in
 * the 'LICENSE' file at the root of this repository, or online at <https://opensource.org/licenses/MS-PL>.
 */

using System;
using Antlr4.Runtime;

namespace VSL;


// Contains information about a parsed expression
// Source: the glsl source string representing the expression value
// Type: the type of the expression value
// Mut: the mutability level of the expression value (used for ref-like `inout`/`out` parameters)
internal abstract record Expr(string Source, ShaderType Type, Expr.Mutability Mut, ParserRuleContext Context)
{
	// Mutability levels for expression values
	public enum Mutability { None, ReadWrite, WriteOnly };


	// Represents a named variable expression
	// Variable: the variable referenced by the expression
	public sealed record Var(Variable Variable, ParserRuleContext Context) 
		: Expr(Variable.GLName, GetVarType(Variable), GetMut(Variable), Context)
	{
		private static ShaderType GetVarType(Variable var) => var switch {
			Variable.PixelInput inp => inp.NumType,
			_ => var.Type
		};
		private static Mutability GetMut(Variable var) => var.Type switch {
			ShaderType.DataType => 
				(var.Access == Variable.RW.ReadOnly) ? Mutability.None :
				(var.Access == Variable.RW.ReadWrite) ? Mutability.ReadWrite : Mutability.WriteOnly,
			_ => Mutability.None
		};
	}


	// Represents a literal expression
	// ScalarType: the literal type
	public sealed record Literal(string Source, ShaderType.Scalar ScalarType, ParserRuleContext Context) 
		: Expr(Source, ScalarType, Mutability.None, Context);


	// Represents an expression that chains a previous one (indexes and members)
	// Parent: the parent expression that the chained expression is adding to
	public sealed record Chained(string Source, ShaderType Type, Expr Parent, ParserRuleContext Context) 
		: Expr(Source, Type, Parent.Mut, Context);


	// Represents an expression that results from a unary operator
	// Op: the operator used in the expression
	// NumType: the numeric result type for the expression
	// Parent: the expression being modified by the operator
	public sealed record UnaryOp(string Source, string Op, ShaderType.Numeric NumType, Expr Parent, 
		ParserRuleContext Context) : Expr(Source, NumType, Mutability.None, Context);


	// Represents an expression that is a compound of two subexpressions with a binary operator
	// Op: the operator used in the expression
	// NumType: the numeric type result of the expression
	// Left: the left expression of the operator
	// Right: the right expression of the operator
	public sealed record BinaryOp(string Source, string Op, ShaderType.Numeric NumType, Expr Left, Expr Right,
		ParserRuleContext Context) : Expr(Source, NumType, Mutability.None, Context);


	// An expression describing a ternary operator
	// Cond: the conditional expression
	// TExpr: the true result expression
	// FExpr: the false result expression
	public sealed record TernaryOp(ShaderType Type, Expr Cond, Expr TExpr, Expr FExpr, ParserRuleContext Context) 
		: Expr($"({Cond.Source} ? {TExpr.Source} : {FExpr.Source})", Type, Mutability.None, Context);


	// Represents an expression resulting from a function call
	// Function: the function entry being called by the expression
	// Args: the function call argument expressions
	public sealed record FunctionCall(string Source, Function.Entry Function, Expr[] Args, ParserRuleContext Context) 
		: Expr(Source, Function.ReturnType, Mutability.None, Context);


	// Represents a call to a type constructor
	// DataType: the type being constructed
	// Args: the constructor call argument expressions
	public sealed record ConstructorCall(string Source, ShaderType.DataType DataType, Expr[] Args, 
		ParserRuleContext Context) : Expr(Source, DataType, Mutability.None, Context);
}
