﻿/*
 * Microsoft Public License (Ms-PL) - Copyright (c) 2022 Vega Authors
 * This file is subject to the terms and conditions of the Microsoft Public License, the text of which can be found in
 * the 'LICENSE' file at the root of this repository, or online at <https://opensource.org/licenses/MS-PL>.
 */

using System;
using System.Globalization;
using Antlr4.Runtime;
using VSL.Grammar;
using static VSL.Grammar.VSL;

namespace VSL;


// Core shader parser/visitor that performs the AST walking and GLSL generation
internal sealed partial class ShaderParser : VSLBaseVisitor<object?>
{
	// Maximum length for identifiers
	public const uint MAX_IDENTIFIER_LENGTH = ShaderProgram.MAX_NAME_LENGTH;

	#region Fields
	// The compiler using this parser
	public readonly ShaderCompiler Compiler;

	// The generator for the GLSL source
	public readonly ShaderGenerator Generator;
	private StageGenerator? _currentGen = null; // The generator for the current stage

	// The internal reflection information for the shader
	public readonly ShaderInfo ShaderInfo;

	// The current error, if any
	public ParseError? Error { get; private set; } = null;
	public bool HasError => Error is not null;

	// The internal parser objects
	public readonly string Source;
	public CommonTokenStream? Tokens { get; private set; } = null;
	public readonly ShaderType.Cache TypeCache;
	private readonly ShaderType.Struct.Builder _structBuilder = new();
	private readonly ScopeManager _scopes = new();
	#endregion // Fields

	public ShaderParser(ShaderCompiler compiler, string source)
	{
		Compiler = compiler;
		Source = source;

		Generator = new();
		ShaderInfo = new();
		TypeCache = new(this);
	}

	// Entry point for parsing function
	public bool Parse()
	{
		Error = null;

		// Create the lexer and parser objects
		AntlrInputStream inStream = new(Source);
		VSLLexer lexer = new(inStream);
		Tokens = new(lexer);
		Grammar.VSL parser = new(Tokens);

		// Install error listeners
		ErrorListener listener = new(this);
		lexer.RemoveErrorListeners();
		parser.RemoveErrorListeners();
		lexer.AddErrorListener(listener);
		parser.AddErrorListener(listener);

		// Perform parsing
		var fileCtx = parser.file();
		if (listener.Error is not null) {
			Error = listener.Error;
			goto end_parse;
		}

		// Perform the visitor walk
		try {
			_ = VisitFile(fileCtx);
		}
		catch (ParseError pe) {
			Error = pe;
			goto end_parse;
		}
#if !DEBUG
		catch (Exception ex) {
			Error = new($"Unhandled exception: {ex})");
			goto end_parse;
		}
#endif

// Cleanup and exit
end_parse:
		Tokens = null;
		return !HasError;
	}

	#region Shared Utilities
	// Validates user-defined identifiers against reserved characters and length limits
	public string ValidateUserIdentifier(IToken ident)
	{
		var name = ident.Text;
		if (name.StartsWith("_")) {
			throw Fatal(ident, "Identifiers cannot start with the reserved character '_'");
		}
		if (name.StartsWith("gl_")) {
			throw Fatal(ident, "Identifiers cannot start with the reserved sequence 'gl_'");
		}
		if (name.StartsWith("$")) {
			throw Fatal(ident, "Identifiers cannot start with the reserved character '$'");
		}
		if (name.Length > MAX_IDENTIFIER_LENGTH) {
			throw Fatal(ident, $"Identifiers cannot be longer than {MAX_IDENTIFIER_LENGTH} characters");
		}
		return name;
	}

	// Processes and performs initial validation on a variable declaration
	public (string Name, ShaderType Type) ProcessVariable(VariableDeclarationContext ctx)
	{
		// Validate the name
		var name = ValidateUserIdentifier(ctx.name);

		// Parse type and validate
		var type = TypeCache.ParseType(ctx.baseType, ctx.typeArg, ctx.INTEGER_LITERAL());
		if (type is ShaderType.Void) {
			throw Fatal(ctx, "A variable cannot be declared with the type void");
		}

		// Return
		return (name, type);
	}

	// Process and parses an integer literal
	public (long Value, bool Unsigned) ParseIntegerLiteral(IToken literal)
	{
		var text = literal.Text.AsSpan();

		if (text.StartsWith("0x")) {
			if (!UInt64.TryParse(text.Slice(2), NumberStyles.HexNumber, null, out var result)) {
				throw Fatal(literal, $"Invalid hex literal '{text}'");
			}
			if (result > UInt32.MaxValue) {
				throw Fatal(literal, $"Integer literal '{result}' is out of range (too large)");
			}

			return ((long)result, true);
		}
		else {
			var isNeg = text[0] == '-';
			var isUnsigned = text[^1] == 'u' || text[^1] == 'U';
			if (isNeg && isUnsigned) {
				throw Fatal(literal, "Cannot negate an unsigned integer literal");
			}
			var parseSpan = text.Slice(isNeg ? 1 : 0, (isNeg || isUnsigned) ? text.Length - 1 : text.Length);
			if (!UInt32.TryParse(parseSpan, out var result)) {
				throw Fatal(literal, $"Invalid decimal literal '{text}'");
			}

			if (isNeg) {
				if (result >= Int32.MaxValue) {
					throw Fatal(literal, $"Integer literal '{text}' is out of range (too negative)");
				}
				return (-result, false);
			}
			else if (isUnsigned || (result > Int32.MaxValue)) {
				return (result, true);
			}
			else {
				return (result, false);
			}
		}
	}

	// Process and parse a float literal
	public double ParseFloatLiteral(IToken literal)
	{
		if (Double.TryParse(literal.Text, out var result)) {
			if (result < Single.MinValue || result > Single.MaxValue) {
				throw Fatal(literal, $"Floating point literal is outside of valid range ({result})");
			}
			return result;
		}
		else {
			throw Fatal(literal, $"Invalid floating point literal '{literal.Text}'");
		}
	}
	#endregion // Shared Utilities

	#region Error Functions
	public ParseError Fatal(IToken token, string msg)
	{
		var txt = token.Text;
		if (txt.Length > 15) {
			txt = txt[0..12] + "...";
		}
		return new ParseError(msg, (uint)token.Line, (uint)token.Column, txt);
	}

	public ParseError Fatal(RuleContext ctx, string msg)
	{
		var tk = Tokens!.Get(ctx.SourceInterval.a);
		var txt = tk.Text;
		if (txt.Length > 15) {
			txt = txt[0..12] + "...";
		}
		return new ParseError(msg, (uint)tk!.Line, (uint)tk!.Column, txt);
	}

	public ParseError Fatal(Antlr4.Runtime.Tree.ITerminalNode node, string msg)
	{
		var tk = Tokens!.Get(node.SourceInterval.a);
		var txt = tk.Text;
		if (txt.Length > 15) {
			txt = txt[0..12] + "...";
		}
		return new ParseError(msg, (uint)tk!.Line, (uint)tk!.Column, txt);
	}
	#endregion // Error Functions
}
