﻿/*
 * Microsoft Public License (Ms-PL) - Copyright (c) 2022 Vega Authors
 * This file is subject to the terms and conditions of the Microsoft Public License, the text of which can be found in
 * the 'LICENSE' file at the root of this repository, or online at <https://opensource.org/licenses/MS-PL>.
 */

using System;

namespace VSL;


// Exception type used to propogate parsing errors up the visitor stack
internal sealed class ParseError : Exception
{
	#region Fields
	// Error message
	public readonly new string Message;

	// Error position
	public readonly (uint Line, uint Character)? Position = null;

	// Bad source text (optional)
	public readonly string? BadSource = null;
	#endregion // Fields

	public ParseError(string msg) => Message = msg;

	public ParseError(string msg, uint line, uint @char, string? badSource = null)
	{
		Message = msg;
		Position = (line, @char);
		BadSource = badSource;
	}

	// Convers to the public API failure type
	public CompileResult.Failure ToFailure()
	{
		string msg;
		if (Position.HasValue) {
			msg = (BadSource is not null)
				? $"[at {Position.Value.Line}:{Position.Value.Character} ('{BadSource}')] {Message}"
				: $"[at {Position.Value.Line}:{Position.Value.Character}] {Message}";
		}
		else {
			msg = (BadSource is not null)
				? $"{Message} ('{BadSource}')"
				: Message;
		}
		return new(msg);
	}
}
