﻿/*
 * Microsoft Public License (Ms-PL) - Copyright (c) 2022 Vega Authors
 * This file is subject to the terms and conditions of the Microsoft Public License, the text of which can be found in
 * the 'LICENSE' file at the root of this repository, or online at <https://opensource.org/licenses/MS-PL>.
 */

using System;
using System.Collections.Generic;
using System.Text;
using Antlr4.Runtime;
using Antlr4.Runtime.Tree;

namespace VSL;


// Manages the internal caching of shared types (like bindings) and shader-specific types (structs, arrays, buffers)
// There are three "levels" of caching for types in VSL:
//   1. Non-Generic Types - populated at startup (void, numerics, samplers, RO texel buffers)
//   2. Generic Types - populated at runtime as they are encountered (images, RW texel buffers)
//   3. Program-Specific Types - populated at runtime, but stored per-shader instead of shared (structs, arrays, buffers)
public abstract partial record ShaderType
{
	#region Fields
	// Cache collection of shared types that are built at runtime (except for structs)
	private static readonly Dictionary<string, ShaderType> _SharedTypes = new();
	private static readonly object _SharedTypesLock = new object();
	#endregion // Fields

	// Performs a thread-safe lookup in the type cache
	private static ShaderType? LookupType(string typeName)
	{
		lock (_SharedTypesLock) {
			if (_SharedTypes.Count == 0) {
				InitializeBuiltins();
			}
			return _SharedTypes.TryGetValue(typeName, out var type) ? type : null;
		}
	}

	// Stores the type to the type cache (if not already present) and returns the same type
	private static ShaderType StoreType(string typeName, ShaderType type)
	{
		lock (_SharedTypesLock) {
			if (_SharedTypes.Count == 0) {
				InitializeBuiltins();
			}
			_SharedTypes.TryAdd(typeName, type);
			return type;
		}
	}


	// Performs the caching of shader-specific types (struct/array/buffers), and also controls access to the shared
	// type cache
	internal sealed class Cache
	{
		#region Fields
		// The parser using the cache
		public readonly ShaderParser Parser;

		// The cache of types
		private readonly Dictionary<string, ShaderType> _cache = new();

		// Used to build type names during parsing
		private readonly StringBuilder _nameBuilder = new(64);
		#endregion // Fields

		public Cache(ShaderParser parser) => Parser = parser;

		// Adds a predefined struct type to the cache
		public void AddStruct(Struct structType) => _cache.Add(structType.Name, structType);

		// Performs a lookup without attempting to parse or cache the type
		public ShaderType? GetType(string typeName)
		{
			// Perform a local lookup, then a global lookup
			if (_cache.TryGetValue(typeName, out var localType)) {
				return localType;
			}
			return LookupType(typeName);
		}

		// Performs a type lookup, and attempts to parse and cache the type if not found
		public ShaderType ParseType(IToken baseName, IToken? typeArg, ITerminalNode[]? arraySizes)
		{
			// Check for array-ness and generic-ness
			var isArray = arraySizes is not null && arraySizes.Length > 0;
			var isGeneric = typeArg is not null;
			if (isArray && isGeneric) {
				throw Parser.Fatal(baseName, $"Generic types cannot be the element type for an array");
			}

			// If there are array sizes, parse these first
			Span<uint> sizeLiterals = stackalloc uint[(int)Array.MAX_ARRAY_DEPTH];
			if (isArray) {
				for (uint si = 0; si < arraySizes!.Length; ++si) {
					var literal = arraySizes[si].Symbol;
					var size = Parser.ParseIntegerLiteral(literal);
					if (size.Value <= 0) {
						throw Parser.Fatal(literal, "Array size cannot be negative or zero");
					}
					sizeLiterals[(int)si] = (uint)size.Value;
				}
				sizeLiterals = sizeLiterals.Slice(0, arraySizes.Length);
			}

			// Build the type name
			var typeName = baseName.Text;
			if (isGeneric || isArray) {
				_nameBuilder.Clear();
				_nameBuilder.Append(baseName.Text);
				if (isGeneric) {
					_nameBuilder.Append('<');
					_nameBuilder.Append(typeArg!.Text);
					_nameBuilder.Append('>');
				}
				else if (isArray) {
					foreach (var size in sizeLiterals) {
						_nameBuilder.Append('[');
						_nameBuilder.Append(size);
						_nameBuilder.Append(']');
					}
				}
				typeName = _nameBuilder.ToString();
			}

			// Perform the lookup first
			if (GetType(typeName) is ShaderType foundType) {
				return foundType;
			}

			// All non-generic types are known at startup, so this must be a generic or array
			if (isGeneric) {
				var baseType = baseName.Text;
				var genType = typeArg!.Text;
				if (baseType.StartsWith("Image")) {
					// Parse the texel format
					var format = TexelFormatExtensions.FromVSLFormat(genType);
					if (!format.HasValue) {
						throw Parser.Fatal(typeArg, $"Unknown texel format specifier '{genType}'");
					}

					// Parse the rank
					var rank = TexelRankExtensions.FromSourceName(baseType.AsSpan().Slice("Image".Length));
					if (!rank.HasValue) {
						throw Parser.Fatal(baseName, $"Image type with unknown rank '{baseType}'");
					}

					// Create and global cache image types
					var newType = new Image(rank.Value, format.Value);
					StoreType(typeName, newType);
					return newType;
				}
				else if (baseType == "Buffer") {
					// Find the contained type
					if (GetType(genType) is not ShaderType storedType) {
						throw Parser.Fatal(typeArg, $"Unknown element type '{genType}' for buffer");
					}
					if (storedType is not DataType dataType) {
						throw Parser.Fatal(typeArg, "Buffer elements must be a numeric, struct, or array type");
					}

					// Cache buffer types locally (since they may rely on struct or array elements that are also local)
					var newType = new Buffer(dataType, false);
					_cache.Add(typeName, newType);
					return newType;
				}
				else if (baseType == "TexelBuffer") {
					// Parse the texel format
					var format = TexelFormatExtensions.FromVSLFormat(genType);
					if (!format.HasValue) {
						throw Parser.Fatal(typeArg, $"Unknown texel format specifier '{genType}'");
					}

					// Create and global cache texel buffer types
					var newType = new TexelBuffer(format.Value, false);
					StoreType(typeName, newType);
					return newType;
				}
				else {
					throw Parser.Fatal(baseName, $"Unknown generic type '{baseName.Text}'");
				}
			}
			else if (isArray) {
				// Lookup the element type for the array
				if (GetType(baseName.Text) is not ShaderType elementType) {
					throw Parser.Fatal(baseName, $"Unknown array element type '{baseName.Text}'");
				}
				if (elementType is not DataType dataType) {
					throw Parser.Fatal(baseName, 
						$"Array element type must be a numeric or struct type (actual {elementType})");
				}

				// Create the nested array types
				var nested = dataType;
				foreach (var size in sizeLiterals) {
					nested = new Array(nested, size);
				}
				_cache.Add(typeName, nested);
				return nested;
			}
			else {
				// Unknown non-generic type
				throw Parser.Fatal(baseName, $"Unknown type '{baseName.Text}'");
			}
		}
	}


	// Add the known non-generic types (just once)
	private static void InitializeBuiltins()
	{
		// void
		_SharedTypes.Add("void", Void.Instance);
		// scalars
		_SharedTypes.Add("bool",  Scalar.Bool);
		_SharedTypes.Add("int",   Scalar.Int);
		_SharedTypes.Add("uint",  Scalar.Uint);
		_SharedTypes.Add("float", Scalar.Float);
		// vectors
		_SharedTypes.Add("bool2",  Vector.Bool2);
		_SharedTypes.Add("bool3",  Vector.Bool3);
		_SharedTypes.Add("bool4",  Vector.Bool4);
		_SharedTypes.Add("int2",   Vector.Int2);
		_SharedTypes.Add("int3",   Vector.Int3);
		_SharedTypes.Add("int4",   Vector.Int4);
		_SharedTypes.Add("uint2",  Vector.Uint2);
		_SharedTypes.Add("uint3",  Vector.Uint3);
		_SharedTypes.Add("uint4",  Vector.Uint4);
		_SharedTypes.Add("float2", Vector.Float2);
		_SharedTypes.Add("float3", Vector.Float3);
		_SharedTypes.Add("float4", Vector.Float4);
		// matrices
		_SharedTypes.Add("float2x2", Matrix.Float2x2);
		_SharedTypes.Add("float2x3", Matrix.Float2x3);
		_SharedTypes.Add("float2x4", Matrix.Float2x4);
		_SharedTypes.Add("float3x2", Matrix.Float3x2);
		_SharedTypes.Add("float3x3", Matrix.Float3x3);
		_SharedTypes.Add("float3x4", Matrix.Float3x4);
		_SharedTypes.Add("float4x2", Matrix.Float4x2);
		_SharedTypes.Add("float4x3", Matrix.Float4x3);
		_SharedTypes.Add("float4x4", Matrix.Float4x4);
		// samplers
		_SharedTypes.Add("Sampler1D", Sampler.Sampler1D);
		_SharedTypes.Add("Sampler2D", Sampler.Sampler2D);
		_SharedTypes.Add("Sampler3D", Sampler.Sampler3D);
		_SharedTypes.Add("Sampler2DArray", Sampler.Sampler2DArray);
		_SharedTypes.Add("ISampler1D", Sampler.ISampler1D);
		_SharedTypes.Add("ISampler2D", Sampler.ISampler2D);
		_SharedTypes.Add("ISampler3D", Sampler.ISampler3D);
		_SharedTypes.Add("ISampler2DArray", Sampler.ISampler2DArray);
		_SharedTypes.Add("USampler1D", Sampler.USampler1D);
		_SharedTypes.Add("USampler2D", Sampler.USampler2D);
		_SharedTypes.Add("USampler3D", Sampler.USampler3D);
		_SharedTypes.Add("USampler2DArray", Sampler.USampler2DArray);
	}
}
