﻿/*
 * Microsoft Public License (Ms-PL) - Copyright (c) 2022 Vega Authors
 * This file is subject to the terms and conditions of the Microsoft Public License, the text of which can be found in
 * the 'LICENSE' file at the root of this repository, or online at <https://opensource.org/licenses/MS-PL>.
 */

using System;
using System.Linq;
using Antlr4.Runtime.Misc;
using VSL.Grammar;
using static VSL.Grammar.VSL;

namespace VSL;


// Core shader parser/visitor that performs the AST walking and GLSL generation
internal sealed partial class ShaderParser : VSLBaseVisitor<object?>
{
	public override object? VisitFile([NotNull] FileContext context)
	{
		// Get the shader kind
		var kindCtx = context.shaderTypeStatement().IDENTIFIER();
		var kindName = kindCtx.GetText();
		ShaderInfo.Kind = kindName switch {
			"graphics" => ShaderKind.Graphics,
			"compute"  => throw Fatal(kindCtx, "Compute shaders are not yet supported"),
			"ray"      => throw Fatal(kindCtx, "Ray shaders are not yet supported"),
			"library"  => throw Fatal(kindCtx, "Shader libraries are not yet supported"),
			_          => throw Fatal(kindCtx, $"Unknown shader kind '{kindName}'")
		};

		// Perform a first pass over stage functions to get the stage mask
		foreach (var tls in context.shaderTopLevelStatement()) {
			if (tls.shaderStageFunction() is ShaderStageFunctionContext fnctx) {
				var stage = ShaderStageExtensions.FromStageCode(fnctx.stage.Text.Substring(1));
				if (!ShaderInfo.TryAddStage(stage!.Value, out var addErr)) {
					throw Fatal(fnctx.stage, addErr);
				}
			}
		}

		// Visit all statements that aren't the stage functions
		foreach (var tls in context.shaderTopLevelStatement()) {
			if (tls.shaderStageFunction() is null) {
				_ = Visit(tls);
			}
		}

		// Perform validation on the shader info now that it is complete
		if (!ShaderInfo.Validate(out var validateErr)) {
			throw Fatal(context, validateErr);
		}

		// Perform final generation required before the shader stage functions
		Generator.EmitPushConstants(ShaderInfo.Bindings, ShaderInfo.PushConstantInfo);

		// Visit the stage functions
		foreach (var tls in context.shaderTopLevelStatement()) {
			if (tls.shaderStageFunction() is ShaderStageFunctionContext fnctx) {
				_ = VisitShaderStageFunction(fnctx);
			}
		}

		return null;
	}

	public override object? VisitStructDefinition([NotNull] StructDefinitionContext context)
	{
		// Validate the name
		var typeName = ValidateUserIdentifier(context.name);
		if (TypeCache.GetType(typeName) is not null) {
			throw Fatal(context.name, $"A type with the name '{typeName}' already exists in the shader");
		}

		// Visit and validate the fields
		foreach (var field in context._fields) {
			var decl = ProcessVariable(field);
			try {
				_structBuilder.AddField(decl.Name, decl.Type);
			}
			catch (ArgumentException ex) {
				throw Fatal(field, ex.Message);
			}
		}
		var structType = _structBuilder.Build(typeName);

		// Cache and emit type
		TypeCache.AddStruct(structType);
		Generator.EmitStructDefinition(structType);

		return null;
	}

	public override object? VisitBlendStateDefinition([NotNull] BlendStateDefinitionContext context)
	{
		// Validate the name
		var blendName = ValidateUserIdentifier(context.name);

		// Parse the blend components
		var srcc = BlendState.ParseFactor(context.srcc.Text);
		if (!srcc.HasValue) {
			throw Fatal(context.srcc, $"Invalid blend factor '{context.srcc.Text}'");
		}
		var dstc = BlendState.ParseFactor(context.dstc.Text);
		if (!dstc.HasValue) {
			throw Fatal(context.dstc, $"Invalid blend factor '{context.dstc.Text}'");
		}
		var cop  = BlendState.ParseOp(context.cop.Text);
		if (!cop.HasValue) {
			throw Fatal(context.cop, $"Invalid blend op '{context.cop.Text}'");
		}
		var srca = BlendState.ParseFactor(context.srca.Text);
		if (!srca.HasValue) {
			throw Fatal(context.srca, $"Invalid blend factor '{context.srca.Text}'");
		}
		var dsta = BlendState.ParseFactor(context.dsta.Text);
		if (!dsta.HasValue) {
			throw Fatal(context.dsta, $"Invalid blend factor '{context.dsta.Text}'");
		}
		var aop  = BlendState.ParseOp(context.aop.Text);
		if (!aop.HasValue) {
			throw Fatal(context.aop, $"Invalid blend op '{context.aop.Text}'");
		}

		// Add the blend state
		BlendState newState = new(srcc.Value, dstc.Value, cop.Value, srca.Value, dsta.Value, aop.Value);
		if (!ShaderInfo.TryAddBlend(blendName, newState, out var addErr)) {
			throw Fatal(context, addErr);
		}

		return null;
	}

	public override object? VisitBindingDeclaration([NotNull] BindingDeclarationContext context)
	{
		// Check the format of the binding slot
		var slotName = context.slot.Text;
		if (!Char.IsLetter(slotName[0]) || !slotName.Skip(1).All(c => Char.IsDigit(c))) {
			throw Fatal(context.slot, "Invalid binding slot name");
		}
		if (slotName[0] != 't' && slotName[0] != 'b') {
			throw Fatal(context.slot, $"Unknown binding space '{slotName[0]}'");
		}

		// Check the binding index and declaration
		bool isTexel = slotName[0] == 't';
		if (!UInt64.TryParse(slotName.AsSpan(1), out var bindIndex)) {
			throw Fatal(context.slot, "Invalid binding slot index");
		}
		if (isTexel) {
			if (bindIndex >= ShaderProgram.MAX_TEXEL_BINDINGS) {
				throw Fatal(context.slot, 
					$"Texel binding slot too high (max: {ShaderProgram.MAX_TEXEL_BINDINGS - 1})");
			}
		}
		else if (bindIndex >= ShaderProgram.MAX_BUFFER_BINDINGS) {
			throw Fatal(context.slot, $"Buffer binding slot too high (max: {ShaderProgram.MAX_BUFFER_BINDINGS - 1})");
		}

		// Process the declaration
		bool isConst = context.KW_CONST() is not null;
		var decl = ProcessVariable(context.variableDeclaration());
		if (decl.Type is not ShaderType.Binding) {
			throw Fatal(context, $"Non-binding type '{decl.Type}' cannot be used as binding");
		}
		if (isConst) {
			if (decl.Type is ShaderType.Buffer bufType) {
				decl.Type = bufType with { ReadOnly = true };
			}
			else if (decl.Type is ShaderType.TexelBuffer tbufType) {
				decl.Type = tbufType with { ReadOnly = true };
			}
			else {
				throw Fatal(context.KW_CONST(), $"The 'const' modifier is not valid for type '{decl.Type}'");
			}
		}
		if (decl.Type is ShaderType.Buffer && isTexel) {
			throw Fatal(context, $"Buffer binding '{decl.Name}' cannot use texel binding slot t{bindIndex}");
		}
		if (decl.Type is not ShaderType.Buffer && !isTexel) {
			throw Fatal(context, $"Texel binding '{decl.Name}' cannot use buffer binding slot b{bindIndex}");
		}

		// Process the binding
		if (!ShaderInfo.TryAddBinding(decl.Name, decl.Type, (uint)bindIndex, out var addErr)) {
			throw Fatal(context, addErr);
		}
		Generator.EmitBinding(decl.Name, ShaderInfo.FindBinding(isTexel, (uint)bindIndex)!);
		_scopes.AddGlobal(new Variable.Binding(decl.Name, ShaderInfo.Bindings[^1]));

		return null;
	}

	public override object? VisitUniformDeclaration([NotNull] UniformDeclarationContext context)
	{
		var decl = ProcessVariable(context.variableDeclaration());

		// Process the uniform
		if (!ShaderInfo.TrySetUniform(decl.Name, decl.Type, out var setErr)) {
			throw Fatal(context, setErr);
		}
		var structType = (decl.Type as ShaderType.Struct)!;
		Generator.EmitUniform(decl.Name, structType);
		_scopes.AddGlobal(new Variable.Uniform(decl.Name, structType));

		return null;
	}

	public override object? VisitPushConstantDeclaration([NotNull] PushConstantDeclarationContext context)
	{
		var decl = ProcessVariable(context.variableDeclaration());

		// Process the push constant (emit happens later)
		var structType = (decl.Type as ShaderType.Struct)!;
		if (!ShaderInfo.TrySetPushConstant(decl.Name, decl.Type, out var setErr)) {
			throw Fatal(context, setErr);
		}
		_scopes.AddGlobal(new Variable.Uniform(decl.Name, structType));

		return null;
	}

	public override object? VisitPixelInputDeclaration([NotNull] PixelInputDeclarationContext context)
	{
		// Check the index and declaration
		var index = ParseIntegerLiteral(context.index);
		if (index.Value < 0) {
			throw Fatal(context.index, "Pixel input index cannot be a negative number");
		}
		var decl = ProcessVariable(context.variableDeclaration());

		// Process the pixel input (emit and variable added later)
		if (!ShaderInfo.TryAddPixelInput(decl.Name, decl.Type, (uint)index.Value, out var addErr)) {
			throw Fatal(context, addErr);
		}

		return null;
	}

	public override object? VisitVertInputDeclaration([NotNull] VertInputDeclarationContext context)
	{
		// Check the use and declaration
		var use = VertexUseExtensions.FromSemanticString(context.semantic.Text);
		if (!use.HasValue) {
			throw Fatal(context.semantic, $"Unknown vertex usage semantic '{context.semantic.Text}'");
		}
		var decl = ProcessVariable(context.variableDeclaration());

		// Process the input (emit and variable added later)
		if (!ShaderInfo.TryAddInput(decl.Name, decl.Type, use.Value, out var slot, out var addErr)) {
			throw Fatal(context, addErr);
		}

		return null;
	}

	public override object? VisitFragOutputDeclaration([NotNull] FragOutputDeclarationContext context)
	{
		// Check the index and declaration
		var index = ParseIntegerLiteral(context.index);
		if (index.Value < 0) {
			throw Fatal(context.index, "Fragment output index cannot be a negative number");
		}
		var decl = ProcessVariable(context.variableDeclaration());

		// Get the blend mode
		var blendName = context.blend?.Text ?? "None";
		if (!ShaderInfo.BlendStates.TryGetValue(blendName, out var blend)) {
			throw Fatal(context.blend!, $"No blend state exists with name '{blendName}'");
		}

		// Process the output (emit and variable added later)
		if (!ShaderInfo.TryAddOutput(decl.Name, decl.Type, (uint)index.Value, blend, out var addErr)) {
			throw Fatal(context, addErr);
		}

		return null;
	}

	public override object? VisitLocalDeclaration([NotNull] LocalDeclarationContext context)
	{
		// Check the declaration and "flat-ness"
		var isFlat = context.KW_FLAT() is not null;
		var decl = ProcessVariable(context.variableDeclaration());

		// Process the local (emit and variable added later)
		if (!ShaderInfo.TryAddLocal(decl.Name, decl.Type, isFlat, out var addErr)) {
			throw Fatal(context, addErr);
		}

		return null;
	}

	public override object? VisitShaderStageFunction([NotNull] ShaderStageFunctionContext context)
	{
		// Get the current stage code
		var stageName = context.stage.Text.Substring(1);
		var stage = ShaderStageExtensions.FromStageCode(stageName);

		// Push a new variable scope and generator, and add variables specific to the stage
		_scopes.PushScope(ScopeManager.ScopeKind.Function);
		_currentGen = Generator.GetOrCreateStage(stage!.Value);
		if (stage == ShaderStage.Vertex) {
			_scopes.AddVariable(Variable.GLBuiltin.VertexIndex);
			_scopes.AddVariable(Variable.GLBuiltin.InstanceIndex);
			_scopes.AddVariable(Variable.GLBuiltin.DrawIndex);
			_scopes.AddVariable(Variable.GLBuiltin.VertexBase);
			_scopes.AddVariable(Variable.GLBuiltin.InstanceBase);
			_scopes.AddVariable(Variable.GLBuiltin.Position);
			_scopes.AddVariable(Variable.GLBuiltin.PointSize);
			foreach (var input in ShaderInfo.Inputs) {
				var io = new Variable.ShaderIO(input.Name, input.Type, Variable.IO.In);
				_scopes.AddVariable(io);
				_currentGen.EmitInput(input.Slot, input.Type, io.GLName, false);
			}
			foreach (var local in ShaderInfo.Locals) {
				var io = new Variable.ShaderIO(local.Name, local.Type, Variable.IO.Out);
				_scopes.AddVariable(io);
				_currentGen.EmitOutput(local.Slot, local.Type, io.GLName, local.Flat);
			}
		}
		else if (stage == ShaderStage.Fragment) {
			_scopes.AddVariable(Variable.GLBuiltin.FragCoord);
			_scopes.AddVariable(Variable.GLBuiltin.FrontFacing);
			_scopes.AddVariable(Variable.GLBuiltin.PointCoord);
			_scopes.AddVariable(Variable.GLBuiltin.PrimitiveID);
			foreach (var input in ShaderInfo.PixelInputs) {
				var inputVar = new Variable.PixelInput(input.Name, input.Type, input.Index);
				_scopes.AddVariable(inputVar);
				_currentGen.EmitPixelInput(input.Index, input.Type.Type, input.Name);
			}
			foreach (var output in ShaderInfo.Outputs) {
				var io = new Variable.ShaderIO(output.Name, output.Type, Variable.IO.Out);
				_scopes.AddVariable(io);
				_currentGen.EmitOutput(output.Index, output.Type, io.GLName, false);
			}
			foreach (var local in ShaderInfo.Locals) {
				var io = new Variable.ShaderIO(local.Name, local.Type, Variable.IO.In);
				_scopes.AddVariable(io);
				_currentGen.EmitInput(local.Slot, local.Type, io.GLName, local.Flat);
			}
		}

		// Visit the function statements
		foreach (var statement in context.statementBlock().statement()) {
			VisitStatement(statement);
		}
		_currentGen = null;

		// Pop the function scope
		_scopes.PopScope();
			
		return null;
	}
}
