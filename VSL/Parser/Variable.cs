﻿/*
 * Microsoft Public License (Ms-PL) - Copyright (c) 2022 Vega Authors
 * This file is subject to the terms and conditions of the Microsoft Public License, the text of which can be found in
 * the 'LICENSE' file at the root of this repository, or online at <https://opensource.org/licenses/MS-PL>.
 */

using System;

namespace VSL;


// Describes a named variable within a scope of a shader program
// Name: the name of the variable as it appears in the VSL source
// Type: the type of the variable (used in expression calculations)
// Access: the read/write access levels for the variable
internal abstract record Variable(string Name, ShaderType Type, Variable.RW Access)
{
	public enum IO { In, Out }
	public enum RW { ReadOnly, WriteOnly, ReadWrite };

	#region Fields
	// The variable name as it appears in the generated GLSL source
	public abstract string GLName { get; }
	#endregion // Fields


	// Describes a global binding variable
	public sealed record Binding(string Name, ShaderInfo.Binding BindingInfo) 
		: Variable(Name, BindingInfo.Type, GetAccessLevel(BindingInfo.Type))
	{
		public override string GLName => BindingInfo.Type.GetAccessSource(Name, BindingInfo.Slot);

		private static RW GetAccessLevel(ShaderType.Binding type) => type switch {
			ShaderType.Sampler        => RW.ReadOnly,
			ShaderType.Image          => RW.ReadWrite,
			ShaderType.Buffer b       => b.ReadOnly ? RW.ReadOnly : RW.ReadWrite,
			ShaderType.TexelBuffer tb => tb.ReadOnly ? RW.ReadOnly : RW.ReadWrite,
			_ => throw new ArgumentOutOfRangeException(nameof(type))
		};
	}


	// Describes a uniform struct type (includes user uniforms, $G, and push constants)
	public sealed record Uniform(string Name, ShaderType.Struct StructType) 
		: Variable(Name, StructType, RW.ReadOnly)
	{
		public const string GlobalNameVSL = "$G";
		public const string GlobalNameGL = "_G_";
		public static readonly Uniform Globals = new(GlobalNameVSL, ShaderType.Struct.GLOBAL_TYPE);

		public override string GLName => (Name != GlobalNameVSL) ? Name : GlobalNameGL;
	}


	// Describes a shader IO
	public sealed record ShaderIO(string Name, ShaderType.DataType DataType, IO InOut) 
		: Variable(Name, DataType, (InOut == IO.In) ? RW.ReadOnly : RW.WriteOnly)
	{
		public override string GLName => (InOut == IO.In) ? $"_in_{Name}" : $"_out_{Name}";
	}


	// Describes a pixel input
	public sealed record PixelInput(string Name, ShaderType.Numeric NumType, uint Index)
		: Variable(Name, NumType, RW.ReadOnly)
	{
		public override string GLName => Name;
	}


	// Describes a variable that is local to a function scope
	public sealed record Private(string Name, ShaderType.DataType DataType, RW Access)
		: Variable(Name, DataType, Access)
	{
		public override string GLName => Name;
	}


	// Describes a builtin GLSL pipeline variable
	public sealed record GLBuiltin(string Name, string GenName, ShaderType.Numeric DataType, RW Access) 
		: Variable(Name, DataType, Access)
	{
		public override string GLName => GenName;

		// Vertex
		public static readonly GLBuiltin VertexIndex = 
			new("$VertexIndex", "gl_VertexIndex", ShaderType.Scalar.Int, RW.ReadOnly);
		public static readonly GLBuiltin InstanceIndex =
			new("$InstanceIndex", "gl_InstanceIndex", ShaderType.Scalar.Int, RW.ReadOnly);
		public static readonly GLBuiltin DrawIndex =
			new("$DrawIndex", "gl_DrawIndex", ShaderType.Scalar.Int, RW.ReadOnly);
		public static readonly GLBuiltin VertexBase =
			new("$VertexBase", "gl_BaseVertex", ShaderType.Scalar.Int, RW.ReadOnly);
		public static readonly GLBuiltin InstanceBase =
			new("$InstanceBase", "gl_BaseInstance", ShaderType.Scalar.Int, RW.ReadOnly);
		public static readonly GLBuiltin Position =
			new("$Position", "gl_Position", ShaderType.Vector.Float4, RW.WriteOnly);
		public static readonly GLBuiltin PointSize =
			new("$PointSize", "gl_PointSize", ShaderType.Scalar.Float, RW.WriteOnly);

		// Fragment
		public static readonly GLBuiltin FragCoord =
			new("$FragCoord", "gl_FragCoord", ShaderType.Vector.Float4, RW.ReadOnly);
		public static readonly GLBuiltin FrontFacing =
			new("$FrontFacing", "gl_FrontFacing", ShaderType.Scalar.Bool, RW.ReadOnly);
		public static readonly GLBuiltin PointCoord =
			new("$PointCoord", "gl_PointCoord", ShaderType.Vector.Float2, RW.ReadOnly);
		public static readonly GLBuiltin PrimitiveID =
			new("$PrimitiveID", "gl_PrimitiveID", ShaderType.Scalar.Int, RW.ReadOnly);
		// TODO: Add gl_FragDepth once we add a way to specify the any/greater/less/unchanged qualifiers
	}
}
