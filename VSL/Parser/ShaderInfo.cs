﻿/*
 * Microsoft Public License (Ms-PL) - Copyright (c) 2022 Vega Authors
 * This file is subject to the terms and conditions of the Microsoft Public License, the text of which can be found in
 * the 'LICENSE' file at the root of this repository, or online at <https://opensource.org/licenses/MS-PL>.
 */

using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.Linq;

namespace VSL;


// Tracks overall information about a shader program as it is being compiled
// Note: this is not a public reflection API type
internal sealed class ShaderInfo
{
	#region Fields
	// The kind of shader and mask of stages
	public ShaderKind Kind = ShaderKind.Graphics;
	public ShaderStageMask Stages { get; private set; } = ShaderStageMask.None;

	// Uniform and push constant
	public Uniform? UniformInfo { get; private set; } = null;
	public PushConstant? PushConstantInfo { get; private set; } = null;

	// Bindings
	public IReadOnlyList<Binding> Bindings => _bindings;
	private readonly List<Binding> _bindings = new();
	public Binding? MaxBinding => _bindings.MaxBy(b => b.Slot);

	// Inputs
	public IReadOnlyList<Input> Inputs => _inputs;
	private readonly List<Input> _inputs = new();
	public uint InputSlotCount { get; private set; } = 0;
	public VertexUseMask InputUseMask { get; private set; } = new();

	// Outputs
	public IReadOnlyList<Output> Outputs => _outputs;
	private readonly List<Output> _outputs = new();

	// Pixel inputs
	public IReadOnlyList<PixelInput> PixelInputs => _pixelInputs;
	private readonly List<PixelInput> _pixelInputs = new();

	// Locals
	public IReadOnlyList<Local> Locals => _locals;
	private readonly List<Local> _locals = new();
	public uint LocalSlotCount { get; private set; } = 0;

	// Tracker for all names
	private readonly Dictionary<string, object> _nameMap = new();

	// Blend States
	public IReadOnlyDictionary<string, BlendState> BlendStates => _blendStates;
	private readonly Dictionary<string, BlendState> _blendStates = new();
	#endregion // Fields

	public ShaderInfo()
	{
		// Populate builtin blend states
		_blendStates["None"]   = BlendState.None;
		_blendStates["Off"]    = BlendState.None;
		_blendStates["Opaque"] = BlendState.None;
		_blendStates["Alpha"]  = BlendState.Alpha;
		_blendStates["Mix"]    = BlendState.Alpha;
		_blendStates["Add"]    = BlendState.Add;
		_blendStates["Mul"]    = BlendState.Mul;
		_blendStates["Max"]    = BlendState.Max;
		_blendStates["Min"]    = BlendState.Min;
	}

	public object? Find(string name) => _nameMap.TryGetValue(name, out var item) ? item : null;

	public Binding? FindBinding(bool texel, uint slot) => 
		_bindings.Find(b => (b.Slot == slot) && (b.Type is ShaderType.Buffer != texel));

	public Input? FindInput(VertexUse use) => InputUseMask.Has(use) ? _inputs.Find(i => i.Use == use) : null;

	public Output? FindOutput(uint index) => _outputs.Find(o => o.Index == index);

	public PixelInput? FindPixelInput(uint index) => _pixelInputs.Find(i => i.Index == index);

	// Perform validation on the current set of shader info
	public bool Validate([NotNullWhen(false)] out string? err)
	{
		// Check required stages
		if (Kind == ShaderKind.Graphics) {
			if (!Stages.Has(ShaderStage.Vertex)) {
				err = "Shader is missing the required vertex stage";
				return false;
			}
			if (!Stages.Has(ShaderStage.Fragment)) {
				err = "Shader is missing the required fragment stage";
				return false;
			}
		}

		// Check outputs (at least one)
		if (Kind == ShaderKind.Graphics) {
			if (_outputs.Count == 0) {
				err = "Graphics shaders must have at least one fragment output";
				return false;
			}
			var maxOut = _outputs.Max(o => o.Index);
			if ((maxOut + 1) != _outputs.Count) {
				err = "Fragment outputs must be contiguously indexed from zero";
				return false;
			}
		}

		// No errors
		err = null;
		return true;
	}

	#region Add
	public bool TryAddStage(ShaderStage stage, [NotNullWhen(false)] out string? err)
	{
		// Validate
		if (Stages.Has(stage)) {
			err = $"The stage {stage} already exists in the shader";
			return false;
		}
		if (stage.GetShaderKind() != Kind) {
			err = $"The stage {stage} is invalid for shader kind {Kind}";
			return false;
		}

		// Update
		Stages |= stage;
		err = null;
		return true;
	}

	public bool TrySetUniform(string name, ShaderType type, [NotNullWhen(false)] out string? err)
	{
		// Validate
		if (UniformInfo is not null) {
			err = "The shader already has a uniform defined";
			return false;
		}
		if (type is not ShaderType.Struct structType) {
			err = "Uniforms must be struct types";
			return false;
		}
		if (Find(name) is not null) {
			err = $"A variable with the name '{name}' already exists in the shader";
			return false;
		}

		// Update
		UniformInfo = new(name, structType);
		_nameMap.Add(name, UniformInfo);
		err = null;
		return true;
	}

	public bool TrySetPushConstant(string name, ShaderType type, [NotNullWhen(false)] out string? err)
	{
		const uint MAX_SIZE = ShaderProgram.MAX_PUSH_CONSTANT_SIZE;

		// Validate
		if (PushConstantInfo is not null) {
			err = "The shader already has a push constant defined";
			return false;
		}
		if (type is not ShaderType.Struct structType) {
			err = "Push constants must be struct types";
			return false;
		}
		if (structType.Size > MAX_SIZE) {
			err = $"Push constant struct exceeds size limit ({structType.Size} > {MAX_SIZE})";
			return false;
		}
		if (Find(name) is not null) {
			err = $"A variable with the name '{name}' already exists in the shader";
			return false;
		}

		// Update
		PushConstantInfo = new(name, structType);
		_nameMap.Add(name, PushConstantInfo);
		err = null;
		return true;
	}

	public bool TryAddBinding(string name, ShaderType type, uint slot, [NotNullWhen(false)] out string? err)
	{
		var isTexel = type is not ShaderType.Buffer;

		// Validate
		if (type is not ShaderType.Binding bindType) {
			err = $"Shader bindings cannot be type {type}";
			return false;
		}
		if (Find(name) is not null) {
			err = $"A variable with the name '{name}' already exists in the shader";
			return false;
		}
		if (FindBinding(isTexel, slot) is Binding found) {
			err = $"The binding slot {slot} is already occpied by '{found.Name}'";
			return false;
		}

		// Update
		var newBind = new Binding(name, bindType, slot);
		_bindings.Add(newBind);
		_nameMap.Add(name, newBind);
		err = null;
		return true;
	}

	public bool TryAddInput(string name, ShaderType type, VertexUse use, out uint slot, [NotNullWhen(false)] out string? err)
	{
		const uint MAX_SLOTS = ShaderProgram.Graphics.MAX_VERTEX_SLOTS;
		slot = 0;

		// Validate 
		if (Find(name) is not null) {
			err = $"A variable with the name '{name}' already exists in the shader";
			return false;
		}
		if (FindInput(use) is Input found) {
			err = $"The vertex usage {use} is already claimed by '{found.Name}'";
			return false;
		}
		if (type is not ShaderType.DataType dataType) {
			err = $"Vertex inputs cannot be non-data type '{type}'";
			return false;
		}
		if (dataType is ShaderType.Struct) {
			err = "Vertex inputs cannot be struct types";
			return false;
		}

		// Calculate new slot info
		var newSlot = InputSlotCount + dataType.SlotCount;
		if (newSlot > MAX_SLOTS) {
			err = $"Vertex input exceeds maximum input slot count ({newSlot} >= {MAX_SLOTS})";
			return false;
		}

		// Update
		slot = InputSlotCount;
		var newInput = new Input(name, dataType, use, InputSlotCount);
		_inputs.Add(newInput);
		_nameMap.Add(name, newInput);
		InputSlotCount = newSlot;
		InputUseMask |= use;
		err = null;
		return true;
	}

	public bool TryAddOutput(string name, ShaderType type, uint index, BlendState blend, 
		[NotNullWhen(false)] out string? err)
	{
		const uint MAX_INDEX = ShaderProgram.Graphics.MAX_FRAGMENT_OUTPUTS - 1;

		// Validate
		if (Find(name) is not null) {
			err = $"A variable with the name '{name}' already exists in the shader";
			return false;
		}
		if (FindOutput(index) is Output found) {
			err = $"The output index {index} is already claimed by '{found.Name}'";
			return false;
		}
		if (type is not ShaderType.Numeric numType) {
			err = $"Fragment outputs cannot be non-numeric type '{type}'";
			return false;
		}
		if (numType.IsBoolean) {
			err = $"Fragment outputs cannot be boolean types";
			return false;
		}
		if (numType.IsMatrix) {
			err = "Fragment outputs cannot be matrix types";
			return false;
		}
		if (index > MAX_INDEX) {
			err = $"Fragment index is outside of supported range ({index} > {MAX_INDEX})";
			return false;
		}

		// Update
		var newOutput = new Output(name, numType, index, blend);
		_outputs.Add(newOutput);
		_nameMap.Add(name, newOutput);
		err = null;
		return true;
	}

	public bool TryAddPixelInput(string name, ShaderType type, uint index, [NotNullWhen(false)] out string? err)
	{
		const uint MAX_INDEX = ShaderProgram.Graphics.MAX_PIXEL_INPUTS - 1;

		// Validate
		if (Find(name) is not null) {
			err = $"A variable with the name '{name}' already exists in the shader";
			return false;
		}
		if (FindPixelInput(index) is PixelInput found) {
			err = $"The pixel input index {index} is already claimed by '{found.Name}'";
			return false;
		}
		if (type is not ShaderType.Vector numType || numType.Rows != 4) {
			err = $"Pixel inputs must be a 4-component vector type (actual {type})";
			return false;
		}
		if (index > MAX_INDEX) {
			err = $"Pixel input index is outside of supported range ({index} > {MAX_INDEX})";
			return false;
		}

		// Update
		var newInput = new PixelInput(name, numType, index);
		_pixelInputs.Add(newInput);
		_nameMap.Add(name, newInput);
		err = null;
		return true;
	}

	public bool TryAddLocal(string name, ShaderType type, bool flat, [NotNullWhen(false)] out string? err)
	{
		const uint MAX_SLOTS = ShaderProgram.Graphics.MAX_LOCAL_SLOTS;

		// Validate
		if (Find(name) is not null) {
			err = $"A variable with the name '{name}' already exists in the shader";
			return false;
		}
		if (type is not ShaderType.Numeric numType) {
			err = $"Locals cannot be non-numeric type '{type}'";
			return false;
		}
		if (numType.IsMatrix) {
			err = "Locals cannot be matrix types";
			return false;
		}

		// Calculate new slot info
		var newSlot = LocalSlotCount + numType.SlotCount;
		if (newSlot >= MAX_SLOTS) {
			err = $"Local exceeds maximum slot count ({newSlot} >= {MAX_SLOTS})";
			return false;
		}

		// Update
		var newLocal = new Local(name, numType, LocalSlotCount, flat || !numType.IsFloat);
		_locals.Add(newLocal);
		_nameMap.Add(name, newLocal);
		LocalSlotCount = newSlot;
		err = null;
		return true;
	}

	public bool TryAddBlend(string name, BlendState state, [NotNullWhen(false)] out string? err)
	{
		if (_blendStates.TryGetValue(name, out _)) {
			err = $"A blend state with the name '{name}' already exists";
			return false;
		}
		_blendStates.Add(name, state);
		err = null;
		return true;
	}
	#endregion // Add


	#region Types
	public sealed record Binding(string Name, ShaderType.Binding Type, uint Slot)
	{
		public ShaderProgram.Binding ToAPIType() => new(Name, Type, Slot);
	}

	public sealed record Uniform(string Name, ShaderType.Struct Type)
	{
		public ShaderProgram.Uniform ToAPIType() => new(Type.TypeSize);
	}

	public sealed record PushConstant(string Name, ShaderType.Struct Type)
	{
		public ShaderProgram.PushConstant ToAPIType() => new(Type.TypeSize);
	}

	public sealed record Input(string Name, ShaderType.DataType Type, VertexUse Use, uint Slot)
	{
		public ShaderProgram.Graphics.Input ToAPIType() => new(Type, Use, Slot);
	}

	public sealed record Output(string Name, ShaderType.Numeric Type, uint Index, BlendState Blend)
	{
		public ShaderProgram.Graphics.Output ToAPIType() => new(Type, Index, Blend);
	}

	public sealed record PixelInput(string Name, ShaderType.Numeric Type, uint Index)
	{
		public ShaderProgram.Graphics.PixelInput ToAPIType() => new(Type.Type, Index);
	}

	public sealed record Local(string Name, ShaderType.Numeric Type, uint Slot, bool Flat);
	#endregion // Types
}
