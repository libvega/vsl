﻿/*
 * Microsoft Public License (Ms-PL) - Copyright (c) 2022 Vega Authors
 * This file is subject to the terms and conditions of the Microsoft Public License, the text of which can be found in
 * the 'LICENSE' file at the root of this repository, or online at <https://opensource.org/licenses/MS-PL>.
 */

using System;
using System.Collections.Generic;
using System.Linq;
using Antlr4.Runtime.Misc;
using VSL.Grammar;

namespace VSL;


// Core shader parser/visitor that performs the AST walking and GLSL generation
internal sealed partial class ShaderParser : VSLBaseVisitor<object?>
{
	public override object VisitAtomExpr([NotNull] Grammar.VSL.AtomExprContext context) => 
		(Visit(context.atom()) as Expr)!;

	public override object VisitNegateExpr([NotNull] Grammar.VSL.NegateExprContext context)
	{
		var expr = Visit(context.expression()) as Expr;
		return Operator.ProcessUnary(this, context.op, expr!);
	}

	public override object VisitMulDivModExpr([NotNull] Grammar.VSL.MulDivModExprContext context)
	{
		var left = Visit(context.left) as Expr;
		var right = Visit(context.right) as Expr;
		return Operator.ProcessBinary(this, context.op, left!, right!);
	}

	public override object VisitAddSubExpr([NotNull] Grammar.VSL.AddSubExprContext context)
	{
		var left = Visit(context.left) as Expr;
		var right = Visit(context.right) as Expr;
		return Operator.ProcessBinary(this, context.op, left!, right!);
	}

	public override object VisitShiftExpr([NotNull] Grammar.VSL.ShiftExprContext context)
	{
		var left = Visit(context.left) as Expr;
		var right = Visit(context.right) as Expr;
		return Operator.ProcessBinary(this, context.op, left!, right!);
	}

	public override object VisitRelationExpr([NotNull] Grammar.VSL.RelationExprContext context)
	{
		var left = Visit(context.left) as Expr;
		var right = Visit(context.right) as Expr;
		return Operator.ProcessBinary(this, context.op, left!, right!);
	}

	public override object VisitEqualityExpr([NotNull] Grammar.VSL.EqualityExprContext context)
	{
		var left = Visit(context.left) as Expr;
		var right = Visit(context.right) as Expr;
		return Operator.ProcessBinary(this, context.op, left!, right!);
	}

	public override object VisitBitwiseExpr([NotNull] Grammar.VSL.BitwiseExprContext context)
	{
		var left = Visit(context.left) as Expr;
		var right = Visit(context.right) as Expr;
		return Operator.ProcessBinary(this, context.op, left!, right!);
	}

	public override object VisitLogicalExpr([NotNull] Grammar.VSL.LogicalExprContext context)
	{
		var left = Visit(context.left) as Expr;
		var right = Visit(context.right) as Expr;
		return Operator.ProcessBinary(this, context.op, left!, right!);
	}

	public override object VisitTernaryExpr([NotNull] Grammar.VSL.TernaryExprContext context)
	{
		var cond = Visit(context.cond) as Expr;
		var texpr = Visit(context.texpr) as Expr;
		var fexpr = Visit(context.fexpr) as Expr;
		return Operator.ProcessTernary(this, cond!, texpr!, fexpr!);
	}

	public override object VisitGroupAtom([NotNull] Grammar.VSL.GroupAtomContext context)
	{
		var expr = Visit(context.expression()) as Expr;
		return new Expr.Chained($"({expr!.Source})", expr.Type, expr, context);
	}

	public override object VisitIndexAtom([NotNull] Grammar.VSL.IndexAtomContext context)
	{
		// Parse the index
		var idxExpr = Visit(context.index) as Expr;
		if (idxExpr!.Type is not ShaderType.Numeric idxNumType) {
			throw Fatal(context.index, $"Only numeric types can be used as index expressions (actual {idxExpr.Type})");
		}

		// Visit the indexed expression and check
		var targExpr = Visit(context.atom()) as Expr;
		var resType = targExpr!.Type.GetIndex(idxNumType, out var idxErr);
		if (resType is null) {
			throw Fatal(context.atom(), idxErr!);
		}
		return new Expr.Chained(
			resType.Value.Format.Replace("$expr", targExpr.Source).Replace("$idx", idxExpr.Source),
			resType.Value.Type, targExpr, context
		);
	}

	public override object VisitMemberAtom([NotNull] Grammar.VSL.MemberAtomContext context)
	{
		// Visit the target expression and check
		var targExpr = Visit(context.atom()) as Expr;
		var resType = targExpr!.Type.GetMember(context.IDENTIFIER().Symbol.Text, false, out var memErr);
		if (resType is null) {
			throw Fatal(context.atom(), memErr!);
		}
		return new Expr.Chained(resType.Value.Format.Replace("$expr", targExpr.Source), resType.Value.Type, 
			targExpr, context);
	}

	public override object VisitFunctionCall([NotNull] Grammar.VSL.FunctionCallContext context)
	{
		// Parse the argument expressions
		List<(Expr Expr, Function.RefType Ref)> args = new();
		foreach (var argCtx in context._args) {
			var expr = Visit(argCtx.expression()) as Expr;
			var refKind = argCtx.@ref?.Text switch {
				"inout" => Function.RefType.InOut,
				"out"   => Function.RefType.Out,
				_       => Function.RefType.In
			};
			args.Add((expr!, refKind));
		}

		// Check if a type matches the call, then revert to functions
		if (TypeCache.GetType(context.name.Text) is ShaderType ctorType) {
			return Function.ProcessConstructor(this, ctorType, context.name, args.Select(p => p.Expr).ToList()) with {
				Context = context
			};
		}
		else {
			return Function.ProcessFunctionCall(this, context.name, args) with { Context = context };
		}
	}

	public override object VisitScalarLiteral([NotNull] Grammar.VSL.ScalarLiteralContext context)
	{
		if (context.INTEGER_LITERAL() is not null) {
			var lit = ParseIntegerLiteral(context.INTEGER_LITERAL().Symbol);
			return new Expr.Literal(lit.Value.ToString(), lit.Unsigned ? ShaderType.Scalar.Uint : ShaderType.Scalar.Int,
				context);
		}
		else if (context.FLOAT_LITERAL() is not null) {
			var lit = ParseFloatLiteral(context.FLOAT_LITERAL().Symbol);
			return new Expr.Literal(lit.ToString(), ShaderType.Scalar.Float, context);
		}
		else {
			var value = context.BOOLEAN_LITERAL().Symbol.Text == "true";
			return new Expr.Literal(value ? "true" : "false", ShaderType.Scalar.Bool, context);
		}
	}

	public override object VisitNameAtom([NotNull] Grammar.VSL.NameAtomContext context)
	{
		// Perform variable lookup and check it
		var varName = context.IDENTIFIER().Symbol.Text;
		var @var = _scopes.FindVariable(varName);
		if (@var is null) {
			throw Fatal(context.IDENTIFIER(), $"No variable exists with the name '{varName}'");
		}
		if (@var.Access == Variable.RW.WriteOnly) {
			throw Fatal(context.IDENTIFIER(), $"Write-only variable '{varName}' cannot be read in an expression");
		}

		// Some extra logic is needed based on the variable type
		if (@var is Variable.Binding binding) {
			_currentGen!.EmitBindingLoad(binding);
		}
		else if (@var is Variable.PixelInput input) {
			_currentGen!.EmitPixelInputLoad(input);
		}

		// Return a variable expression
		return new Expr.Var(@var, context);
	}
}
