﻿/*
 * Microsoft Public License (Ms-PL) - Copyright (c) 2022 Vega Authors
 * This file is subject to the terms and conditions of the Microsoft Public License, the text of which can be found in
 * the 'LICENSE' file at the root of this repository, or online at <https://opensource.org/licenses/MS-PL>.
 */

using System;
using Antlr4.Runtime;

namespace VSL;


// Manages the definitions and checks for operators
internal static class Operator
{
	public static Expr ProcessUnary(ShaderParser parser, IToken op, Expr expr)
	{
		// Check expression
		if (expr.Type is not ShaderType.Numeric numType) {
			throw parser.Fatal(expr.Context, $"Non-numeric type {expr.Type} does not support unary operators");
		} 

		// Switch on operator
		var optxt = op.Text;
		if (optxt == "-") {
			if (!numType.IsSigned && !numType.IsFloat) {
				throw parser.Fatal(expr.Context, 
					$"Unary '-' can only be applied to float or signed types (actual {numType})");
			}
			return new Expr.UnaryOp($"-{expr.Source}", "-", numType, expr, null!);
		}
		else if (optxt == "!") {
			if (!numType.IsBoolean) {
				throw parser.Fatal(expr.Context, $"Unary '!' can only be applied to bool types (actual {numType})");
			}
			return numType.Rows switch {
				1 => new Expr.UnaryOp($"!{expr.Source}", "!", numType, expr, null!),
				_ => new Expr.UnaryOp($"not({expr.Source})", "!", numType, expr, null!)
			};
		}
		else if (optxt == "~") {
			if (!numType.IsInteger) {
				throw parser.Fatal(expr.Context, $"Unary '~' can only be applied to integer types (actual {numType})");
			}
			return new Expr.UnaryOp($"~{expr.Source}", "~", numType, expr, null!);
		}
		else {
			throw new NotImplementedException($"Unknown unary operator '{optxt}'");
		}
	}

	public static Expr ProcessBinary(ShaderParser parser, IToken op, Expr left, Expr right)
	{
		// Compound assign check
		var optxt = op.Text;
		if (optxt[^1] == '=' && optxt != "==" && optxt != "!=") {
			optxt = optxt[0..^1];
		}

		// Check numeric types
		if (left.Type is not ShaderType.Numeric leftNum) {
			throw parser.Fatal(left.Context, $"Non-numeric type {left.Type} cannot appear with operator '{optxt}'");
		}
		if (right.Type is not ShaderType.Numeric rightNum) {
			throw parser.Fatal(right.Context, $"Non-numeric type {right.Type} cannot appear with operator '{optxt}'");
		}
		var defaultSrc = $"({left.Source} {op.Text} {right.Source})";
		var anyFloat = leftNum.IsFloat || rightNum.IsFloat;
		var anyUint = leftNum.IsUnsigned || rightNum.IsUnsigned;
		var outNumBase = anyFloat ? ShaderType.NumericType.Float :
			anyUint ? ShaderType.NumericType.Unsigned : ShaderType.NumericType.Signed;

		// Switch on the operator
		if (optxt == "*" || optxt == "/") {
			if (leftNum.IsBoolean) {
				throw parser.Fatal(left.Context, $"Boolean types cannot appear with operator '{optxt}'");
			}
			if (rightNum.IsBoolean) {
				throw parser.Fatal(right.Context, $"Boolean types cannot appear with operator '{optxt}'");
			}
			var isDiv = optxt == "/";
			var sameSize = leftNum.Rows == rightNum.Rows && leftNum.Cols == rightNum.Cols;

			// Switch on the operand types
			if (leftNum.IsScalar) {
				if (rightNum.IsScalar) {
					var resType = ShaderType.Numeric.Get(outNumBase, 1, 1);
					return new Expr.BinaryOp(defaultSrc, optxt, resType!, left, right, null!);
				}
				else if (rightNum.IsVector) {
					if (isDiv) {
						throw parser.Fatal(right.Context, "Cannot divide a scalar by a vector");
					}
					var resType = ShaderType.Numeric.Get(outNumBase, rightNum.Rows, 1);
					return new Expr.BinaryOp(defaultSrc, optxt, resType!, left, right, null!);
				}
				else { // rightNum.IsMatrix
					if (isDiv) {
						throw parser.Fatal(right.Context, "Cannot divide a scalar by a matrix");
					}
					var resType = ShaderType.Numeric.Get(outNumBase, rightNum.Rows, rightNum.Cols);
					return new Expr.BinaryOp(defaultSrc, optxt, resType!, left, right, null!);
				}
			}
			else if (leftNum.IsVector) {
				if (rightNum.IsScalar) {
					var resType = ShaderType.Numeric.Get(outNumBase, leftNum.Rows, 1);
					return new Expr.BinaryOp(defaultSrc, optxt, resType!, left, right, null!);
				}
				else if (rightNum.IsVector) {
					if (!sameSize) {
						throw parser.Fatal(right.Context, "Can only mul/div vectors with the same size");
					}
					var resType = ShaderType.Numeric.Get(outNumBase, leftNum.Rows, 1);
					return new Expr.BinaryOp(defaultSrc, optxt, resType!, left, right, null!);
				}
				else { // rightNum.IsMatrix
					parser.Fatal(right.Context, $"Cannot mul/div a vector by a matrix");
				}
			}
			else { // leftNum.IsMatrix
				if (rightNum.IsScalar) {
					var resType = ShaderType.Numeric.Get(outNumBase, leftNum.Rows, leftNum.Cols);
					return new Expr.BinaryOp(defaultSrc, optxt, resType!, left, right, null!);
				}
				else if (rightNum.IsVector) {
					if (isDiv) {
						throw parser.Fatal(right.Context, "Cannot divide a matrix by a vector");
					}
					if (leftNum.Cols != rightNum.Rows) {
						throw parser.Fatal(right.Context, "Component count mismatch for matrix * vector");
					}
					var resType = ShaderType.Numeric.Get(outNumBase, leftNum.Rows, 1);
					return new Expr.BinaryOp(defaultSrc, optxt, resType!, left, right, null!);
				}
				else { // rightNum.IsMatrix
					if (isDiv) {
						throw parser.Fatal(right.Context, "Cannot divide matrices");
					}
					if (leftNum.Cols != rightNum.Rows) {
						throw parser.Fatal(right.Context, "Component count mismatch for matrix * matrix");
					}
					var resType = ShaderType.Numeric.Get(outNumBase, leftNum.Rows, 1);
					return new Expr.BinaryOp(defaultSrc, optxt, resType!, left, right, null!);
				}
			}
		}
		else if (optxt == "%") {
			if (leftNum.IsBoolean) {
				throw parser.Fatal(left.Context, "Boolean types cannot be used for the modulo operator");
			}
			if (rightNum.IsBoolean) {
				throw parser.Fatal(right.Context, "Boolean types cannot be used for the modulo operator");
			}
			if (leftNum.IsMatrix) {
				throw parser.Fatal(left.Context, "Matrix types cannot be used for the modulo operator");
			}
			if (rightNum.IsMatrix) {
				throw parser.Fatal(right.Context, "Matrix types cannot be used for the modulo operator");
			}
			var canCast = leftNum.CanAssignTo(rightNum) || rightNum.CanAssignTo(leftNum);
			if (!canCast) {
				throw parser.Fatal(right.Context, $"Cannot perform modulo on types {leftNum} and {rightNum}");
			}

			var source = anyFloat ? $"mod({left.Source}, {right.Source})" : defaultSrc;
			var resType = ShaderType.Numeric.Get(outNumBase, leftNum.Rows, 1);
			return new Expr.BinaryOp(source, optxt, resType!, left, right, null!);
		}
		else if (optxt == "+" || optxt == "-") {
			var canCast = leftNum.CanAssignTo(rightNum) || rightNum.CanAssignTo(leftNum);
			if (!canCast) {
				throw parser.Fatal(right.Context, $"Cannot perform add/sub on types {leftNum} and {rightNum}");
			}
			var resType = leftNum.CanAssignTo(rightNum) ? rightNum : leftNum;
			return new Expr.BinaryOp(defaultSrc, optxt, resType, left, right, null!);
		}
		else if (optxt == "<<" || optxt == ">>") {
			if (!leftNum.IsInteger) {
				throw parser.Fatal(left.Context, "Non-integer types cannot be used in bitshift operators");
			}
			if (!rightNum.IsInteger) {
				throw parser.Fatal(right.Context, "Non-integer types cannot be used in bitshift operators");
			}
			if (leftNum.IsMatrix) {
				throw parser.Fatal(left.Context, "Matrix types cannot be used in bitshift operators");
			}
			if (rightNum.IsMatrix) {
				throw parser.Fatal(right.Context, "Matrix types cannot be used in bitshift operators");
			}
			if (rightNum.Rows != leftNum.Rows && rightNum.Rows != 1) {
				throw parser.Fatal(right.Context, "Right side of bitshift operator must be scalar or match left size");
			}
			return new Expr.BinaryOp(defaultSrc, optxt, leftNum, left, right, null!);
		}
		else if (optxt == "<" || optxt == ">" || optxt == "<=" || optxt == ">=") {
			if (leftNum.IsBoolean) {
				throw parser.Fatal(left.Context, "Boolean types cannot be used in inequality operators");
			}
			if (rightNum.IsBoolean) {
				throw parser.Fatal(right.Context, "Boolean types cannot be used in inequality operators");
			}
			if (leftNum.IsMatrix) {
				throw parser.Fatal(left.Context, "Matrix types cannot be used in inequality operators");
			}
			if (rightNum.IsMatrix) {
				throw parser.Fatal(right.Context, "Matrix types cannot be used in inequality operators");
			}
			var canCast = leftNum.CanAssignTo(rightNum) || rightNum.CanAssignTo(leftNum);
			if (!canCast) {
				throw parser.Fatal(right.Context, $"Cannot compare types {leftNum} and {rightNum} for inequality");
			}

			var source = leftNum.IsScalar
				? defaultSrc
				: optxt switch {
					"<"  => $"lessThan({left.Source}, {right.Source})",
					">"  => $"greaterThan({left.Source}, {right.Source})",
					"<=" => $"lessThanEqual({left.Source}, {right.Source})",
					">=" => $"greaterThanEqual({left.Source}, {right.Source})",
					_ => throw new NotImplementedException()
				};
			var resType = ShaderType.Numeric.Get(ShaderType.NumericType.Boolean, leftNum.Rows, 1);
			return new Expr.BinaryOp(source, optxt, resType!, left, right, null!);
		}
		else if (optxt == "==" || optxt == "!=") {
			var canCast = leftNum.CanAssignTo(rightNum) || rightNum.CanAssignTo(leftNum);
			if (!canCast) {
				throw parser.Fatal(right.Context, $"Cannot check for equality between types {leftNum} and {rightNum}");
			}
			return new Expr.BinaryOp(defaultSrc, optxt, ShaderType.Scalar.Bool, left, right, null!);
		}
		else if (optxt == "&" || optxt == "|" || optxt == "^") {
			if (!leftNum.IsInteger) {
				throw parser.Fatal(left.Context, $"Expression for operator '{optxt}' must be an integer type");
			}
			if (!rightNum.IsInteger) {
				throw parser.Fatal(right.Context, $"Expression for operator '{optxt}' must be an integer type");
			}
			if (leftNum != rightNum) {
				throw parser.Fatal(right.Context, $"Types must match exactly for operator '{optxt}'");
			}
			return new Expr.BinaryOp(defaultSrc, optxt, leftNum, left, right, null!);
		}
		else if (optxt == "||" || optxt == "&&" || optxt == "^^") {
			if (leftNum != ShaderType.Scalar.Bool) {
				throw parser.Fatal(left.Context, $"Expressions for operator '{optxt}' must be a scalar boolean");
			}
			if (rightNum != ShaderType.Scalar.Bool) {
				throw parser.Fatal(right.Context, $"Expressions for operator '{optxt}' must be a scalar boolean");
			}
			return new Expr.BinaryOp(defaultSrc, optxt, ShaderType.Scalar.Bool, left, right, null!);
		}
	
		throw new NotImplementedException($"Unknown binary operator '{optxt}'");
	}

	public static Expr ProcessTernary(ShaderParser parser, Expr cond, Expr texpr, Expr fexpr)
	{
		// Check condition types
		if (cond.Type != ShaderType.Scalar.Bool) {
			throw parser.Fatal(cond.Context, 
				$"Condition expression in ternary ?: must be a scalar bool (actual {cond.Type})");
		}

		// Check result types
		if (texpr.Type != fexpr.Type) {
			if (texpr.Type is not ShaderType.DataType tNumType) {
				throw parser.Fatal(texpr.Context, "Non-data types in ternary expressions must match exactly");
			}
			if (fexpr.Type is not ShaderType.DataType fNumType) {
				throw parser.Fatal(fexpr.Context, "Non-data types in ternary expressions must match exactly");
			}
			if (!fNumType.CanAssignTo(tNumType)) {
				throw parser.Fatal(fexpr.Context, 
					$"False result type {fNumType} cannot implicitly cast to true result type {tNumType}");
			}
		}

		return new Expr.TernaryOp(texpr.Type, cond, texpr, fexpr, null!);
	}
}
