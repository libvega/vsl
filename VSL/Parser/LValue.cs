﻿/*
 * Microsoft Public License (Ms-PL) - Copyright (c) 2022 Vega Authors
 * This file is subject to the terms and conditions of the Microsoft Public License, the text of which can be found in
 * the 'LICENSE' file at the root of this repository, or online at <https://opensource.org/licenses/MS-PL>.
 */

using System;
using Antlr4.Runtime;

namespace VSL;


// Represents an expression that can appear on the left side of an assignment
// Context: the parser context associated with the LValue
internal abstract record LValue(ParserRuleContext Context)
{
	// Perform the check if the operator with the expression is valid, and return the GLSL source if so
	public abstract string CreateSource(ShaderParser parser, Expr expr, string op);

	// Map the lvalue with an index applied
	public abstract LValue WithIndex(ShaderParser parser, Expr index);

	// Map the lvalue with a member applied
	public abstract LValue WithMember(ShaderParser parser, IToken member);


	// Represents an assignment to a private (function-local) variable (which is known to be a numeric or struct type)
	// Source: The GLSL source representing the lvalue
	// DataType: The type of the lvalue being assigned to
	// Parent: The parent assignment (for member/index chains), if present
	public sealed record PrivateAssign(string Source, ShaderType.DataType DataType, PrivateAssign? Parent,
		ParserRuleContext Context) : LValue(Context)
	{
		public override string CreateSource(ShaderParser parser, Expr expr, string op)
		{
			if (DataType is ShaderType.Array) {
				throw parser.Fatal(Context, $"Cannot assign to an array variable directly");
			}
			if (expr.Type is not ShaderType.DataType dataType || !dataType.CanAssignTo(DataType)) {
				throw parser.Fatal(expr.Context, $"Cannot assign type {expr.Type} to {DataType}");
			}
			// TODO: Also check for compound operators
			return $"{Source} {op} {expr.Source}";
		}

		public override LValue WithIndex(ShaderParser parser, Expr index)
		{
			if (index.Type is not ShaderType.Numeric numType) {
				throw parser.Fatal(index.Context, $"Non numeric type {index.Type} cannot be used as an index");
			}
			var idxResult = DataType.GetIndex(numType, out var idxErr);
			if (idxResult is null) {
				throw parser.Fatal(index.Context, idxErr!);
			}
			return new PrivateAssign(
				idxResult.Value.Format.Replace("$expr", Source).Replace("$idx", index.Source),
				(idxResult.Value.Type as ShaderType.DataType)!, this, null!
			);
		}

		public override LValue WithMember(ShaderParser parser, IToken member)
		{
			var memResult = DataType.GetMember(member.Text, true, out var memErr);
			if (memResult is null) {
				throw parser.Fatal(member, memErr!);
			}
			return new PrivateAssign(
				memResult.Value.Format.Replace("$expr", Source), (memResult.Value.Type as ShaderType.DataType)!,
				this, null!
			);
		}
	}


	// Represents a write to an image or texel buffer binding
	// Binding: the image or texel buffer being written to
	// Index: the store coordinates, null if not yet known
	public sealed record TexelStore(Variable.Binding Binding, Expr? Index, ParserRuleContext Context) 
		: LValue(Context)
	{
		public override string CreateSource(ShaderParser parser, Expr expr, string op)
		{
			var imgType = Binding.Type as ShaderType.Image;
			var tbType = Binding.Type as ShaderType.TexelBuffer;

			// General validation
			if (op != "=") {
				throw parser.Fatal(Context, "Cannot use compound assignment operators on texel stores");
			}
			if (Index is null) {
				throw parser.Fatal(Context, "Cannot write to a texel object without specifying the coordinates");
			}
			if (expr.Type is not ShaderType.Numeric numType) {
				throw parser.Fatal(expr.Context, $"Cannot store non-numeric type {expr.Type} to a texel object");
			}

			// Type-specific validation
			var texType = imgType?.Format.GetDataType() ?? tbType!.Format.GetDataType();
			if (!numType.CanAssignTo(texType)) {
				throw parser.Fatal(expr.Context, $"Cannot store type {numType} to texel data with format {texType}");
			}

			// Create the source
			var exprArgs = numType.Rows switch {
				1 => $"{expr.Source}, 0, 0, 0",
				2 => $"{expr.Source}, 0, 0",
				_ => expr.Source
			};
			return $"imageStore({Binding.GLName}, {texType.GLName}({exprArgs}))";
		}

		public override LValue WithIndex(ShaderParser parser, Expr index)
		{
			if (Index is not null) {
				throw parser.Fatal(index.Context, $"Cannot index texel data components in a texel store");
			}
			if (index.Type is not ShaderType.Numeric numType || !numType.IsInteger) {
				throw parser.Fatal(index.Context, $"Index must be an integer numeric type (actual {index.Type})");
			}
			var rankSize = (Binding.Type as ShaderType.Image)?.Rank.GetDimensionCount() ?? 1;
			if (numType.Rows != rankSize) {
				throw parser.Fatal(index.Context,
					$"Invalid component count for indexer (expected {rankSize}, actual {numType.Rows})");
			}
			return new TexelStore(Binding, index, null!);
		}

		public override LValue WithMember(ShaderParser parser, IToken member) =>
			throw parser.Fatal(member, $"Texel type {Binding.Type} does not support .<member> access");
	}


	// Represents a write to a buffer binding
	// Source: the current access source for the lvalue
	// DataType: the current type being targeted in the lvalue
	// Binding: the buffer being written to
	// Parent: the optional parent buffer store lvalue for chaining (null if an index not yet given)
	public sealed record BufferStore(string Source, ShaderType.DataType DataType, Variable.Binding Binding, 
		BufferStore? Parent, ParserRuleContext Context) : LValue(Context)
	{
		public override string CreateSource(ShaderParser parser, Expr expr, string op)
		{
			if (op != "=") {
				throw parser.Fatal(Context, "Cannot use compound assignment operators on buffer stores");
			}
			if (Parent is null) {
				throw parser.Fatal(Context, "Cannot assign directly to a buffer binding variable");
			}
			if (expr.Type is not ShaderType.DataType exprDataType || !exprDataType.CanAssignTo(DataType)) {
				throw parser.Fatal(expr.Context, $"Cannot assign expression of type {expr.Type} to type {DataType}");
			}

			return $"{Source} {op} {expr.Source}";
		}

		public override LValue WithIndex(ShaderParser parser, Expr index)
		{
			if (Parent is null) {
				if (index.Type is not ShaderType.Scalar scType || !scType.IsInteger) {
					throw parser.Fatal(index.Context, 
						$"Buffer indexers must be scalar integer types (actual {index.Type})");
				}
				return new BufferStore($"({Source}[{index.Source}].data)", (Binding.Type as ShaderType.Buffer)!.ElementType, 
					Binding, this, null!);
			}
			else {
				if (index.Type is not ShaderType.Numeric numType) {
					throw parser.Fatal(index.Context, $"Non numeric type {index.Type} cannot be used as an index");
				}
				var idxResult = DataType.GetIndex(numType, out var idxErr);
				if (idxResult is null) {
					throw parser.Fatal(index.Context, idxErr!);
				}
				return new BufferStore(
					idxResult.Value.Format.Replace("$expr", Source).Replace("$idx", index.Source),
					(idxResult.Value.Type as ShaderType.DataType)!, Binding, this, null!
				);
			}
		}

		public override LValue WithMember(ShaderParser parser, IToken member)
		{
			if (Parent is null) {
				throw parser.Fatal(member, "Buffer bindings do not support direct .<member> access");
			}
			var memResult = DataType.GetMember(member.Text, true, out var memErr);
			if (memResult is null) {
				throw parser.Fatal(member, memErr!);
			}
			return new BufferStore(
				memResult.Value.Format.Replace("$expr", Source), (memResult.Value.Type as ShaderType.DataType)!,
				Binding, this, null!
			);
		}
	}
}
