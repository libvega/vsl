﻿/*
 * Microsoft Public License (Ms-PL) - Copyright (c) 2022 Vega Authors
 * This file is subject to the terms and conditions of the Microsoft Public License, the text of which can be found in
 * the 'LICENSE' file at the root of this repository, or online at <https://opensource.org/licenses/MS-PL>.
 */

using System;
using Antlr4.Runtime.Misc;
using VSL.Grammar;
using static VSL.Grammar.VSL;

namespace VSL;


// Core shader parser/visitor that performs the AST walking and GLSL generation
internal sealed partial class ShaderParser : VSLBaseVisitor<object?>
{
	public override object? VisitVariableDeclaration([NotNull] Grammar.VSL.VariableDeclarationContext context)
	{
		// Process the declaration
		var decl = ProcessVariable(context);
		if (decl.Type is not ShaderType.DataType dataType) {
			throw Fatal(context.baseType, $"Private function variables cannot be non-datatype {decl.Type}");
		}
		if (dataType is ShaderType.Array) {
			throw Fatal(context.arraySize, "Private function variables cannot be arrays");
		}
		if (_scopes.FindVariable(decl.Name) is not null) {
			throw Fatal(context.name, $"A variable with the name '{decl.Name}' already exists in the current scope");
		}

		// Add the variable and emit
		_scopes.AddVariable(new Variable.Private(decl.Name, dataType, Variable.RW.ReadWrite));
		_currentGen!.EmitDeclaration(decl.Name, dataType);

		return null;
	}

	public override object? VisitVariableDefinition([NotNull] Grammar.VSL.VariableDefinitionContext context)
	{
		// Process the declaration
		var declCtx = context.variableDeclaration();
		var decl = ProcessVariable(declCtx);
		if (decl.Type is not ShaderType.DataType dataType) {
			throw Fatal(declCtx.baseType, $"Private function variables cannot be non-datatype {decl.Type}");
		}
		if (dataType is ShaderType.Array) {
			throw Fatal(declCtx.arraySize, "Private function variables cannot be arrays");
		}
		if (_scopes.FindVariable(decl.Name) is not null) {
			throw Fatal(declCtx.name, $"A variable with the name '{decl.Name}' already exists in the current scope");
		}
		var isConst = context.KW_CONST() is not null;

		// Process the expression
		var expr = Visit(context.expression()) as Expr;
		if (expr!.Type is not ShaderType.DataType exprDataType || !exprDataType.CanAssignTo(dataType)) {
			throw Fatal(context.expression(), $"Cannot assign type {expr.Type} to type {decl.Type}");
		}

		// Add the variable and emit
		_scopes.AddVariable(
			new Variable.Private(decl.Name, dataType, isConst ? Variable.RW.ReadOnly : Variable.RW.ReadWrite));
		_currentGen!.EmitDefinition(decl.Name, dataType, expr);

		return null;
	}

	public override object? VisitVariableAssignment([NotNull] Grammar.VSL.VariableAssignmentContext context)
	{
		// Visit the lvalue and expression
		var lvalue = Visit(context.lvalue()) as LValue;
		var expr = Visit(context.expression()) as Expr;

		// Emit source
		var source = lvalue!.CreateSource(this, expr!, context.op.Text);
		_currentGen!.EmitRawBody(source);

		return null;
	}

	#region LValues
	public override object VisitGroupLValue([NotNull] Grammar.VSL.GroupLValueContext context) => 
		Visit(context.lvalue()) switch {
			LValue.PrivateAssign pa => new LValue.PrivateAssign($"({pa.Source})", pa.DataType, pa, context),
			LValue.BufferStore bs => new LValue.BufferStore($"({bs.Source})", bs.DataType, bs.Binding, bs, context),
			LValue.TexelStore ts => ts,
			_ => throw new NotImplementedException()
		};

	public override object VisitMemberLValue([NotNull] Grammar.VSL.MemberLValueContext context) =>
		(Visit(context.lvalue()) as LValue)!.WithMember(this, context.member) with { Context = context };

	public override object VisitIndexLValue([NotNull] Grammar.VSL.IndexLValueContext context) =>
		(Visit(context.lvalue()) as LValue)!.WithIndex(this, (Visit(context.index) as Expr)!) with { Context = context };

	public override object VisitNameLValue([NotNull] Grammar.VSL.NameLValueContext context)
	{
		// Perform a lookup of the variable
		var vrblName = context.name.Text;
		var vrbl = _scopes.FindVariable(vrblName);
		if (vrbl is null) {
			throw Fatal(context.name, $"No variable with the name '{vrblName}' exists in the current scope");
		}

		// Validate the variable
		if (vrbl.Access == Variable.RW.ReadOnly) {
			throw Fatal(context.name, 
				$"The read-only variable '{vrblName}' cannot appear as the target of an assignment");
		}

		// Create the lvalue based on the variable type
		if (vrbl is Variable.Private privVar) {
			return new LValue.PrivateAssign(privVar.GLName, privVar.DataType, null, context);
		}
		else if (vrbl is Variable.ShaderIO sio) {
			return new LValue.PrivateAssign(sio.GLName, sio.DataType, null, context);
		}
		else if (vrbl is Variable.GLBuiltin builtin) {
			return new LValue.PrivateAssign(builtin.GLName, builtin.DataType, null, context);
		}
		else if (vrbl.Type is ShaderType.Image) {
			_currentGen!.EmitBindingLoad((vrbl as Variable.Binding)!);
			return new LValue.TexelStore((vrbl as Variable.Binding)!, null, context);
		}
		else if (vrbl.Type is ShaderType.TexelBuffer) {
			_currentGen!.EmitBindingLoad((vrbl as Variable.Binding)!);
			return new LValue.TexelStore((vrbl as Variable.Binding)!, null, context);
		}
		else if (vrbl.Type is ShaderType.Buffer) {
			_currentGen!.EmitBindingLoad((vrbl as Variable.Binding)!);
			return new LValue.BufferStore(vrbl.GLName, null!, (vrbl as Variable.Binding)!, null, context);
		}
		else {
			throw Fatal(context.name, $"The type {vrbl.Type} cannot appear as the target of an assignment");
		}
	}
	#endregion // LValues

	public override object? VisitIfStatement([NotNull] Grammar.VSL.IfStatementContext context)
	{
		// Process if statement
		{
			// Check condition
			var cond = Visit(context.cond) as Expr;
			if (cond!.Type != ShaderType.Scalar.Bool) {
				throw Fatal(context.cond, $"If statement condition must be scalar boolean (actual {cond.Type})");
			}

			// Manage scopes and visit statements
			_scopes.PushScope(ScopeManager.ScopeKind.Conditional);
			_currentGen!.EmitBlockStart($"if ({cond.Source})");
			foreach (var stmt in context.statementBlock().statement()) {
				Visit(stmt);
			}
			_currentGen.EmitBlockClose();
			_scopes.PopScope();
		}

		// Process any elif statements
		foreach (var elif in context.elifStatement()) {
			// Check condition
			var cond = Visit(elif.cond) as Expr;
			if (cond!.Type != ShaderType.Scalar.Bool) {
				throw Fatal(elif.cond, $"Elif statement condition must be scalar boolean (actual {cond.Type})");
			}

			// Manage scopes and visit statements
			_scopes.PushScope(ScopeManager.ScopeKind.Conditional);
			_currentGen!.EmitBlockStart($"else if ({cond.Source})");
			foreach (var stmt in elif.statementBlock().statement()) {
				Visit(stmt);
			}
			_currentGen.EmitBlockClose();
			_scopes.PopScope();
		}

		// Process the else statement
		if (context.elseStatement() is ElseStatementContext elseCtx) {
			_scopes.PushScope(ScopeManager.ScopeKind.Conditional);
			_currentGen!.EmitBlockStart("else");
			foreach (var stmt in elseCtx.statementBlock().statement()) {
				Visit(stmt);
			}
			_currentGen.EmitBlockClose();
			_scopes.PopScope();
		}

		return null;
	}
}
