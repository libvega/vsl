﻿/*
 * Microsoft Public License (Ms-PL) - Copyright (c) 2022 Vega Authors
 * This file is subject to the terms and conditions of the Microsoft Public License, the text of which can be found in
 * the 'LICENSE' file at the root of this repository, or online at <https://opensource.org/licenses/MS-PL>.
 */

using System;
using System.Collections.Generic;

namespace VSL;


// Defines the builtin functions
internal sealed partial record Function
{
	private static readonly Dictionary<string, Function> _Builtins = new();

	static Function()
	{
		var float1 = ShaderType.Scalar.Float;
		var float2 = ShaderType.Vector.Float2;
		var float3 = ShaderType.Vector.Float3;
		var float4 = ShaderType.Vector.Float4;
		var float2x2 = ShaderType.Matrix.Float2x2;
		var float2x3 = ShaderType.Matrix.Float2x3;
		var float2x4 = ShaderType.Matrix.Float2x4;
		var float3x2 = ShaderType.Matrix.Float3x2;
		var float3x3 = ShaderType.Matrix.Float3x3;
		var float3x4 = ShaderType.Matrix.Float3x4;
		var float4x2 = ShaderType.Matrix.Float4x2;
		var float4x3 = ShaderType.Matrix.Float4x3;
		var float4x4 = ShaderType.Matrix.Float4x4;
		var int1 = ShaderType.Scalar.Int;
		var int2 = ShaderType.Vector.Int2;
		var int3 = ShaderType.Vector.Int3;
		var int4 = ShaderType.Vector.Int4;
		var uint1 = ShaderType.Scalar.Uint;
		var uint2 = ShaderType.Vector.Uint2;
		var uint3 = ShaderType.Vector.Uint3;
		var uint4 = ShaderType.Vector.Uint4;
		var bool1 = ShaderType.Scalar.Bool;
		var bool2 = ShaderType.Vector.Bool2;
		var bool3 = ShaderType.Vector.Bool3;
		var bool4 = ShaderType.Vector.Bool4;
		ShaderType
			genImage1D = new ShaderType.Image(TexelRank.E1D, TexelFormat.Float),
			genImage2D = new ShaderType.Image(TexelRank.E2D, TexelFormat.Float),
			genImage3D = new ShaderType.Image(TexelRank.E3D, TexelFormat.Float),
			genImage2DArray = new ShaderType.Image(TexelRank.E2DArray, TexelFormat.Float),
			genTexelBuffer = new ShaderType.TexelBuffer(TexelFormat.Float, false);

		#region Trig
		_Builtins["acos"] = new Function("acos", new Entry[] {
			new("acos", float1, float1),
			new("acos", float2, float2),
			new("acos", float3, float3),
			new("acos", float4, float4)
		});
		_Builtins["acosh"] = new Function("acosh", new Entry[] {
			new("acosh", float1, float1),
			new("acosh", float2, float2),
			new("acosh", float3, float3),
			new("acosh", float4, float4)
		});
		_Builtins["asin"] = new Function("asin", new Entry[] {
			new("asin", float1, float1),
			new("asin", float2, float2),
			new("asin", float3, float3),
			new("asin", float4, float4)
		});
		_Builtins["asinh"] = new Function("asinh", new Entry[] {
			new("asinh", float1, float1),
			new("asinh", float2, float2),
			new("asinh", float3, float3),
			new("asinh", float4, float4)
		});
		_Builtins["atan"] = new Function("atan", new Entry[] {
			new("atan", float1, float1),
			new("atan", float2, float2),
			new("atan", float3, float3),
			new("atan", float4, float4)
		});
		_Builtins["atan2"] = new Function("atan2", new Entry[] {
			new("atan2", float1, float1, float1),
			new("atan2", float2, float2, float2),
			new("atan2", float3, float3, float3),
			new("atan2", float4, float4, float4)
		});
		_Builtins["atanh"] = new Function("atanh", new Entry[] {
			new("atanh", float1, float1),
			new("atanh", float2, float2),
			new("atanh", float3, float3),
			new("atanh", float4, float4)
		});
		_Builtins["cos"] = new Function("cos", new Entry[] {
			new("cos", float1, float1),
			new("cos", float2, float2),
			new("cos", float3, float3),
			new("cos", float4, float4)
		});
		_Builtins["cosh"] = new Function("cosh", new Entry[] {
			new("cosh", float1, float1),
			new("cosh", float2, float2),
			new("cosh", float3, float3),
			new("cosh", float4, float4)
		});
		_Builtins["sin"] = new Function("sin", new Entry[] {
			new("sin", float1, float1),
			new("sin", float2, float2),
			new("sin", float3, float3),
			new("sin", float4, float4)
		});
		_Builtins["sinh"] = new Function("sinh", new Entry[] {
			new("sinh", float1, float1),
			new("sinh", float2, float2),
			new("sinh", float3, float3),
			new("sinh", float4, float4)
		});
		_Builtins["tan"] = new Function("tan", new Entry[] {
			new("tan", float1, float1),
			new("tan", float2, float2),
			new("tan", float3, float3),
			new("tan", float4, float4)
		});
		_Builtins["tanh"] = new Function("tanh", new Entry[] {
			new("tanh", float1, float1),
			new("tanh", float2, float2),
			new("tanh", float3, float3),
			new("tanh", float4, float4)
		});
		_Builtins["deg2rad"] = new Function("deg2rad", new Entry[] {
			new("radians", float1, float1),
			new("radians", float2, float2),
			new("radians", float3, float3),
			new("radians", float4, float4)
		});
		_Builtins["rad2deg"] = new Function("rad2deg", new Entry[] {
			new("degrees", float1, float1),
			new("degrees", float2, float2),
			new("degrees", float3, float3),
			new("degrees", float4, float4)
		});
		#endregion Trig

		#region General Math
		_Builtins["abs"] = new Function("abs", new Entry[] {
			new("abs", int1, int1),
			new("abs", int2, int2),
			new("abs", int3, int3),
			new("abs", int4, int4),
			new("abs", float1, float1),
			new("abs", float2, float2),
			new("abs", float3, float3),
			new("abs", float4, float4)
		});
		_Builtins["ceil"] = new Function("ceil", new Entry[] {
			new("ceil", float1, float1),
			new("ceil", float2, float2),
			new("ceil", float3, float3),
			new("ceil", float4, float4)
		});
		_Builtins["clamp"] = new Function("clamp", new Entry[] {
			new("clamp", int1, int1, int1, int1),
			new("clamp", int2, int2, int2, int1),
			new("clamp", int2, int2, int2, int2),
			new("clamp", int3, int3, int3, int1),
			new("clamp", int3, int3, int3, int3),
			new("clamp", int4, int4, int4, int1),
			new("clamp", int4, int4, int4, int4),
			new("clamp", uint1, uint1, uint1, uint1),
			new("clamp", uint2, uint2, uint2, uint1),
			new("clamp", uint2, uint2, uint2, uint2),
			new("clamp", uint3, uint3, uint3, uint1),
			new("clamp", uint3, uint3, uint3, uint3),
			new("clamp", uint4, uint4, uint4, uint1),
			new("clamp", uint4, uint4, uint4, uint4),
			new("clamp", float1, float1, float1, float1),
			new("clamp", float2, float2, float2, float1),
			new("clamp", float2, float2, float2, float2),
			new("clamp", float3, float3, float3, float1),
			new("clamp", float3, float3, float3, float3),
			new("clamp", float4, float4, float4, float1),
			new("clamp", float4, float4, float4, float4)
		});
		// dFdx, dFdy
		_Builtins["exp"] = new Function("exp", new Entry[] {
			new("exp", float1, float1),
			new("exp", float2, float2),
			new("exp", float3, float3),
			new("exp", float4, float4)
		});
		_Builtins["exp2"] = new Function("exp2", new Entry[] {
			new("exp2", float1, float1),
			new("exp2", float2, float2),
			new("exp2", float3, float3),
			new("exp2", float4, float4)
		});
		_Builtins["floor"] = new Function("floor", new Entry[] {
			new("floor", float1, float1),
			new("floor", float2, float2),
			new("floor", float3, float3),
			new("floor", float4, float4)
		});
		_Builtins["fma"] = new Function("fma", new Entry[] {
			new("fma", float1, float1, float1, float1),
			new("fma", float2, float2, float2, float2),
			new("fma", float3, float3, float3, float3),
			new("fma", float4, float4, float4, float4)
		});
		_Builtins["fract"] = new Function("fract", new Entry[] {
			new("fract", float1, float1),
			new("fract", float2, float2),
			new("fract", float3, float3),
			new("fract", float4, float4)
		});
		// fwidth
		_Builtins["isqrt"] = new Function("isqrt", new Entry[] {
			new("inverseSqrt", float1, float1),
			new("inverseSqrt", float2, float2),
			new("inverseSqrt", float3, float3),
			new("inverseSqrt", float4, float4)
		});
		_Builtins["isinf"] = new Function("isinf", new Entry[] {
			new("isinf", bool1, float1),
			new("isinf", bool2, float2),
			new("isinf", bool3, float3),
			new("isinf", bool4, float4)
		});
		_Builtins["isnan"] = new Function("isnan", new Entry[] {
			new("isnan", bool1, float1),
			new("isnan", bool2, float2),
			new("isnan", bool3, float3),
			new("isnan", bool4, float4)
		});
		_Builtins["log"] = new Function("log", new Entry[] {
			new("log", float1, float1),
			new("log", float2, float2),
			new("log", float3, float3),
			new("log", float4, float4)
		});
		_Builtins["log2"] = new Function("log2", new Entry[] {
			new("log2", float1, float1),
			new("log2", float2, float2),
			new("log2", float3, float3),
			new("log2", float4, float4)
		});
		_Builtins["max"] = new Function("max", new Entry[] {
			new("max", int1, int1, int1),
			new("max", int2, int1, int1),
			new("max", int2, int2, int2),
			new("max", int3, int1, int1),
			new("max", int3, int3, int3),
			new("max", int4, int1, int1),
			new("max", int4, int4, int4),
			new("max", uint1, uint1, uint1),
			new("max", uint2, uint1, uint1),
			new("max", uint2, uint2, uint2),
			new("max", uint3, uint1, uint1),
			new("max", uint3, uint3, uint3),
			new("max", uint4, uint1, uint1),
			new("max", uint4, uint4, uint4),
			new("max", float1, float1, float1),
			new("max", float2, float1, float1),
			new("max", float2, float2, float2),
			new("max", float3, float1, float1),
			new("max", float3, float3, float3),
			new("max", float4, float1, float1),
			new("max", float4, float4, float4)
		});
		_Builtins["min"] = new Function("min", new Entry[] {
			new("min", int1, int1, int1),
			new("min", int2, int1, int1),
			new("min", int2, int2, int2),
			new("min", int3, int1, int1),
			new("min", int3, int3, int3),
			new("min", int4, int1, int1),
			new("min", int4, int4, int4),
			new("min", uint1, uint1, uint1),
			new("min", uint2, uint1, uint1),
			new("min", uint2, uint2, uint2),
			new("min", uint3, uint1, uint1),
			new("min", uint3, uint3, uint3),
			new("min", uint4, uint1, uint1),
			new("min", uint4, uint4, uint4),
			new("min", float1, float1, float1),
			new("min", float2, float1, float1),
			new("min", float2, float2, float2),
			new("min", float3, float1, float1),
			new("min", float3, float3, float3),
			new("min", float4, float1, float1),
			new("min", float4, float4, float4)
		});
		_Builtins["mix"] = new Function("mix", new Entry[] {
			new("mix", float1, float1, float1, float1),
			new("mix", float2, float2, float2, float1),
			new("mix", float2, float2, float2, float2),
			new("mix", float3, float3, float3, float1),
			new("mix", float3, float3, float3, float3),
			new("mix", float4, float4, float4, float1),
			new("mix", float4, float4, float4, float4),
			new("mix", bool1, bool1, bool1, bool1),
			new("mix", bool2, bool2, bool2, bool2),
			new("mix", bool3, bool3, bool3, bool3),
			new("mix", bool4, bool4, bool4, bool4),
			new("mix", int1, int1, int1, bool1),
			new("mix", int2, int2, int2, bool2),
			new("mix", int3, int3, int3, bool3),
			new("mix", int4, int4, int4, bool4),
			new("mix", uint1, uint1, uint1, bool1),
			new("mix", uint2, uint2, uint2, bool2),
			new("mix", uint3, uint3, uint3, bool3),
			new("mix", uint4, uint4, uint4, bool4),
			new("mix", float1, float1, float1, bool1),
			new("mix", float2, float2, float2, bool2),
			new("mix", float3, float3, float3, bool3),
			new("mix", float4, float4, float4, bool4)
		});
		_Builtins["mod"] = new Function("mod", new Entry[] {
			new("mod", float1, float1, float1),
			new("mod", float2, float2, float1),
			new("mod", float2, float2, float2),
			new("mod", float3, float3, float1),
			new("mod", float3, float3, float3),
			new("mod", float4, float4, float1),
			new("mod", float4, float4, float4)
		});
		_Builtins["modf"] = new Function("modf", new Entry[] {
			new Entry("modf", float1, float1, float1).SetRef(1, RefType.Out),
			new Entry("modf", float2, float2, float2).SetRef(1, RefType.Out),
			new Entry("modf", float3, float3, float3).SetRef(1, RefType.Out),
			new Entry("modf", float4, float4, float4).SetRef(1, RefType.Out)
		});
		// noise
		_Builtins["pow"] = new Function("pow", new Entry[] {
			new("pow", float1, float1, float1),
			new("pow", float2, float2, float2),
			new("pow", float3, float3, float3),
			new("pow", float4, float4, float4)
		});
		_Builtins["round"] = new Function("round", new Entry[] {
			new("round", float1, float1),
			new("round", float2, float2),
			new("round", float3, float3),
			new("round", float4, float4)
		});
		_Builtins["roundEven"] = new Function("roundEven", new Entry[] {
			new("roundEven", float1, float1),
			new("roundEven", float2, float2),
			new("roundEven", float3, float3),
			new("roundEven", float4, float4)
		});
		_Builtins["sign"] = new Function("sign", new Entry[] {
			new("sign", int1, int1),
			new("sign", int2, int2),
			new("sign", int3, int3),
			new("sign", int4, int4),
			new("sign", float1, float1),
			new("sign", float2, float2),
			new("sign", float3, float3),
			new("sign", float4, float4)
		});
		_Builtins["smoothStep"] = new Function("smoothStep", new Entry[] {
			new("smoothStep", float1, float1, float1, float1),
			new("smoothStep", float2, float1, float1, float2),
			new("smoothStep", float3, float1, float1, float3),
			new("smoothStep", float4, float1, float1, float4),
			new("smoothStep", float2, float2, float2, float2),
			new("smoothStep", float3, float3, float3, float3),
			new("smoothStep", float4, float4, float4, float4)
		});
		_Builtins["sqrt"] = new Function("sqrt", new Entry[] {
			new("sqrt", float1, float1),
			new("sqrt", float2, float2),
			new("sqrt", float3, float3),
			new("sqrt", float4, float4)
		});
		_Builtins["step"] = new Function("step", new Entry[] {
			new("step", float1, float1, float1),
			new("step", float2, float1, float2),
			new("step", float3, float1, float3),
			new("step", float4, float1, float4),
			new("step", float2, float2, float2),
			new("step", float3, float3, float3),
			new("step", float4, float4, float4)
		});
		_Builtins["trunc"] = new Function("trunc", new Entry[] {
			new("trunc", float1, float1),
			new("trunc", float2, float2),
			new("trunc", float3, float3),
			new("trunc", float4, float4)
		});
		#endregion // General Math

		#region Floating Point
		_Builtins["bitCastInt"] = new Function("bitCastInt", new Entry[] {
			new("floatBitsToInt", int1, float1),
			new("floatBitsToInt", int2, float2),
			new("floatBitsToInt", int3, float3),
			new("floatBitsToInt", int4, float4)
		});
		_Builtins["bitCastUint"] = new Function("bitCastUint", new Entry[] {
			new("floatBitsToUint", uint1, float1),
			new("floatBitsToUint", uint2, float2),
			new("floatBitsToUint", uint3, float3),
			new("floatBitsToUint", uint4, float4)
		});
		_Builtins["frexp"] = new Function("frexp", new Entry[] { 
			new Entry("frexp", float1, float1, uint1).SetRef(1, RefType.Out),
			new Entry("frexp", float2, float2, uint2).SetRef(1, RefType.Out),
			new Entry("frexp", float3, float3, uint3).SetRef(1, RefType.Out),
			new Entry("frexp", float4, float4, uint4).SetRef(1, RefType.Out)
		});
		_Builtins["bitCastFloat"] = new Function("bitCastFloat", new Entry[] {
			new("intBitsToFloat", float1, int1),
			new("intBitsToFloat", float2, int2),
			new("intBitsToFloat", float3, int3),
			new("intBitsToFloat", float4, int4),
			new("uintBitsToFloat", float1, uint1),
			new("uintBitsToFloat", float2, uint2),
			new("uintBitsToFloat", float3, uint3),
			new("uintBitsToFloat", float4, uint4)
		});
		_Builtins["ldexp"] = new Function("ldexp", new Entry[] {
			new("ldexp", float1, float1, int1),
			new("ldexp", float2, float2, int2),
			new("ldexp", float3, float3, int3),
			new("ldexp", float4, float4, int4)
		});
		// packing and unpacking functions
		#endregion // Floating Point

		#region Integers
		_Builtins["bitCount"] = new Function("bitCount", new Entry[] {
			new("bitCount", int1, int1),
			new("bitCount", int2, int2),
			new("bitCount", int3, int3),
			new("bitCount", int4, int4),
			new("bitCount", int1, uint1),
			new("bitCount", int2, uint2),
			new("bitCount", int3, uint3),
			new("bitCount", int4, uint4)
		});
		// bitfieldExtract, bitfieldInsert, bitfieldReverse
		_Builtins["findLSB"] = new Function("findLSB", new Entry[] {
			new("findLSB", int1, int1),
			new("findLSB", int2, int2),
			new("findLSB", int3, int3),
			new("findLSB", int4, int4),
			new("findLSB", int1, uint1),
			new("findLSB", int2, uint2),
			new("findLSB", int3, uint3),
			new("findLSB", int4, uint4)
		});
		_Builtins["findMSB"] = new Function("findMSB", new Entry[] {
			new("findMSB", int1, int1),
			new("findMSB", int2, int2),
			new("findMSB", int3, int3),
			new("findMSB", int4, int4),
			new("findMSB", int1, uint1),
			new("findMSB", int2, uint2),
			new("findMSB", int3, uint3),
			new("findMSB", int4, uint4)
		});
		// uaddCarry, umulExtent, usubBorrow
		#endregion // Integers

		#region Vector
		_Builtins["all"] = new Function("all", new Entry[] {
			new("all", bool1, bool1),
			new("all", bool1, bool2),
			new("all", bool1, bool3),
			new("all", bool1, bool4)
		});
		_Builtins["any"] = new Function("any", new Entry[] {
			new("any", bool1, bool1),
			new("any", bool1, bool2),
			new("any", bool1, bool3),
			new("any", bool1, bool4)
		});
		// greaterThan, greaterThanEqual, lessThan, lessThanEqual, not are all replaced with operators
		_Builtins["cross"] = new Function("cross", new Entry[] {
			new("cross", float3, float3, float3)
		});
		_Builtins["distance"] = new Function("distance", new Entry[] {
			new("distance", float1, float1, float1),
			new("distance", float1, float2, float2),
			new("distance", float1, float3, float3),
			new("distance", float1, float4, float4)
		});
		_Builtins["dot"] = new Function("dot", new Entry[] {
			new("dot", float1, float1, float1),
			new("dot", float1, float2, float2),
			new("dot", float1, float3, float3),
			new("dot", float1, float4, float4)
		});
		_Builtins["compEq"] = new Function("compEq", new Entry[] {
			new("equal", bool2, float2, float2),
			new("equal", bool3, float3, float3),
			new("equal", bool4, float4, float4)
		});
		_Builtins["faceForward"] = new Function("faceForward", new Entry[] {
			new("faceForward", float1, float1, float1, float1),
			new("faceForward", float2, float2, float2, float2),
			new("faceForward", float3, float3, float3, float3),
			new("faceForward", float4, float4, float4, float4)
		});
		_Builtins["length"] = new Function("length", new Entry[] {
			new("length", float1, float1),
			new("length", float1, float2),
			new("length", float1, float3),
			new("length", float1, float4)
		});
		_Builtins["normalize"] = new Function("normalize", new Entry[] {
			new("normalize", float1, float1),
			new("normalize", float2, float2),
			new("normalize", float3, float3),
			new("normalize", float4, float4)
		});
		_Builtins["compNeq"] = new Function("compNeq", new Entry[] {
			new("notEqual", bool2, float2, float2),
			new("notEqual", bool3, float3, float3),
			new("notEqual", bool4, float4, float4)
		});
		_Builtins["reflect"] = new Function("reflect", new Entry[] {
			new("reflect", float1, float1, float1),
			new("reflect", float2, float2, float2),
			new("reflect", float3, float3, float3),
			new("reflect", float4, float4, float4)
		});
		_Builtins["refract"] = new Function("refract", new Entry[] {
			new("refract", float1, float1, float1, float1),
			new("refract", float2, float2, float2, float1),
			new("refract", float3, float3, float3, float1),
			new("refract", float4, float4, float4, float1)
		});
		#endregion // Vector

		#region Matrix
		_Builtins["determinant"] = new Function("determinant", new Entry[] {
			new("determinant", float1, float2x2),
			new("determinant", float1, float3x3),
			new("determinant", float1, float4x4)
		});
		_Builtins["inverse"] = new Function("inverse", new Entry[] {
			new("inverse", float2x2, float2x2),
			new("inverse", float3x3, float3x3),
			new("inverse", float4x4, float4x4)
		});
		_Builtins["matCompMul"] = new Function("matCompMul", new Entry[] {
			new("matrixCompMult", float2x2, float2x2, float2x2),
			new("matrixCompMult", float2x3, float2x3, float2x3),
			new("matrixCompMult", float2x4, float2x4, float2x4),
			new("matrixCompMult", float3x2, float3x2, float3x2),
			new("matrixCompMult", float3x3, float3x3, float3x3),
			new("matrixCompMult", float3x4, float3x4, float3x4),
			new("matrixCompMult", float4x2, float4x2, float4x2),
			new("matrixCompMult", float4x3, float4x3, float4x3),
			new("matrixCompMult", float4x4, float4x4, float4x4)
		});
		_Builtins["outerProd"] = new Function("outerProd", new Entry[] {
			new("outerProduct", float2x2, float2, float2),
			new("outerProduct", float2x3, float3, float2),
			new("outerProduct", float2x4, float4, float2),
			new("outerProduct", float3x2, float2, float3),
			new("outerProduct", float3x3, float3, float3),
			new("outerProduct", float3x4, float4, float3),
			new("outerProduct", float4x2, float2, float4),
			new("outerProduct", float4x3, float3, float4),
			new("outerProduct", float4x4, float4, float4)
		});
		_Builtins["transpose"] = new Function("transpose", new Entry[] {
			new("transpose", float2x2, float2x2),
			new("transpose", float2x3, float3x2),
			new("transpose", float2x4, float4x2),
			new("transpose", float3x2, float2x3),
			new("transpose", float3x3, float3x3),
			new("transpose", float3x4, float4x3),
			new("transpose", float4x2, float2x4),
			new("transpose", float4x3, float3x4),
			new("transpose", float4x4, float4x4)
		});
		#endregion // Matrix

		#region Texture/Image
		_Builtins["sample"] = new Function("sample", new Entry[] {
			new("texture", float4, ShaderType.Sampler.Sampler1D, float1),
			new("texture", float4, ShaderType.Sampler.Sampler2D, float2),
			new("texture", float4, ShaderType.Sampler.Sampler3D, float3),
			new("texture", float4, ShaderType.Sampler.Sampler2DArray, float3),
			new("texture", int4, ShaderType.Sampler.ISampler1D, float1),
			new("texture", int4, ShaderType.Sampler.ISampler2D, float2),
			new("texture", int4, ShaderType.Sampler.ISampler3D, float3),
			new("texture", int4, ShaderType.Sampler.ISampler2DArray, float3),
			new("texture", uint4, ShaderType.Sampler.USampler1D, float1),
			new("texture", uint4, ShaderType.Sampler.USampler2D, float2),
			new("texture", uint4, ShaderType.Sampler.USampler3D, float3),
			new("texture", uint4, ShaderType.Sampler.USampler2DArray, float3)
		});
		// textureQueryLevels
		_Builtins["sizeOf"] = new Function("sizeOf", new Entry[] {
			new("textureSize($0, 0)", int1, ShaderType.Sampler.Sampler1D),
			new("textureSize($0, 0)", int2, ShaderType.Sampler.Sampler2D),
			new("textureSize($0, 0)", int3, ShaderType.Sampler.Sampler3D),
			new("textureSize($0, 0)", int3, ShaderType.Sampler.Sampler2DArray),
			new("textureSize($0, 0)", int1, ShaderType.Sampler.ISampler1D),
			new("textureSize($0, 0)", int2, ShaderType.Sampler.ISampler2D),
			new("textureSize($0, 0)", int3, ShaderType.Sampler.ISampler3D),
			new("textureSize($0, 0)", int3, ShaderType.Sampler.ISampler2DArray),
			new("textureSize($0, 0)", int1, ShaderType.Sampler.USampler1D),
			new("textureSize($0, 0)", int2, ShaderType.Sampler.USampler2D),
			new("textureSize($0, 0)", int3, ShaderType.Sampler.USampler3D),
			new("textureSize($0, 0)", int3, ShaderType.Sampler.USampler2DArray),
			new Entry("imageSize", int1, genImage1D).SetGeneric(0),
			new Entry("imageSize", int2, genImage2D).SetGeneric(0),
			new Entry("imageSize", int3, genImage3D).SetGeneric(0),
			new Entry("imageSize", int3, genImage2DArray).SetGeneric(0),
			new Entry("imageSize", int1, genTexelBuffer).SetGeneric(0)
		});
		#endregion // Texture/Image
	}
}
