﻿/*
 * Microsoft Public License (Ms-PL) - Copyright (c) 2022 Vega Authors
 * This file is subject to the terms and conditions of the Microsoft Public License, the text of which can be found in
 * the 'LICENSE' file at the root of this repository, or online at <https://opensource.org/licenses/MS-PL>.
 */

using System;
using System.Collections.Generic;
using System.Linq;

namespace VSL;


// Manages the stack of variable scopes while parsing a shader program
internal sealed class ScopeManager
{
	// Scope usage kinds
	public enum ScopeKind : int { Root, Function, Loop, Conditional }

	#region Fields
	// The root scope that contains the shared global variables
	private readonly Scope _rootScope;

	// The stack of scopes
	private readonly Stack<Scope> _scopes = new();
	private Scope _currScope => _scopes.Peek();

	// Gets if the scope stack is current in a loop scope or child of a loop scope
	public bool IsInLoop => _loopDepth > 0;
	private uint _loopDepth = 0;
	#endregion // Fields

	public ScopeManager()
	{
		_rootScope = new(ScopeKind.Root);
		_scopes.Push(_rootScope);

		AddGlobal(Variable.Uniform.Globals);
	}

	// Add a global variable
	public void AddGlobal(Variable @var) => _rootScope.Variables.Add(@var.Name, @var);

	// Lookup a variable with the given name
	public Variable? FindVariable(string name)
	{
		foreach (var scope in _scopes.Reverse()) {
			if (scope.Variables.TryGetValue(name, out var @var)) {
				return @var;
			}
		}
		return null;
	}

	// Push a new scope
	public void PushScope(ScopeKind kind)
	{
		if (kind == ScopeKind.Loop) {
			_loopDepth += 1;
		}
		_scopes.Push(new(kind));
	}

	// Pop the current bottom-most scope
	public void PopScope()
	{
		var pop = _scopes.Pop();
		if (pop.Kind == ScopeKind.Loop) {
			_loopDepth -= 1;
		}
	}

	// Add a variable to the current scope
	public void AddVariable(Variable @var) => _currScope.Variables.Add(@var.Name, @var);

	// Manages a specific scope in the stack
	private sealed class Scope
	{
		// The scope kind
		public readonly ScopeKind Kind;
		// The set of scope variables
		public readonly Dictionary<string, Variable> Variables = new();

		public Scope(ScopeKind kind) => Kind = kind;
	}
}
