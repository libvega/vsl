﻿/*
 * Microsoft Public License (Ms-PL) - Copyright (c) 2022 Vega Authors
 * This file is subject to the terms and conditions of the Microsoft Public License, the text of which can be found in
 * the 'LICENSE' file at the root of this repository, or online at <https://opensource.org/licenses/MS-PL>.
 */

using System;
using System.IO;
using Antlr4.Runtime;

namespace VSL;


// Error listener for the ANTLR parsing and lexing process
internal sealed class ErrorListener : BaseErrorListener, IAntlrErrorListener<int>
{
	#region Fields
	// The parser using the listener instance
	public readonly ShaderParser Parser;

	// The error, if any, that the listener encountered
	public ParseError? Error { get; private set; } = null;
	#endregion // Fields

	public ErrorListener(ShaderParser parser)
	{
		Parser = parser;
	}

	public override void SyntaxError(TextWriter output, IRecognizer recognizer, IToken offendingSymbol, int line,
		int charPositionInLine, string msg, RecognitionException e)
	{
		// Extract extra error information
		RuleContext? ctx = null;
		var badText = offendingSymbol?.Text;
		if (e is not null) {
			ctx = e.Context;
			if (badText is null) {
				badText = e.OffendingToken?.Text;
			}
		}
		var ruleIdx = ctx?.RuleIndex;

		// Create customized error text (TODO: expand this over time to exhaustively cover known errors)
		string errorMsg;
		if (msg.Contains("expecting '@shader'")) {
			errorMsg = "Shader must start with a '@shader' statement";
		}
		else {
			errorMsg = $"(Rule {(ruleIdx.HasValue ? recognizer.RuleNames[ruleIdx.Value] : "none")}) - {msg}";
		}

		// Set the error
		Error = new(errorMsg, (uint)line, (uint)charPositionInLine, badText);
	}

	public void SyntaxError(TextWriter output, IRecognizer recognizer, int offendingSymbol, int line,
		int charPositionInLine, string msg, RecognitionException e) =>
		SyntaxError(output, recognizer, e.OffendingToken, line, charPositionInLine, msg, e);
}
