This directory contains the generated output of the ANTLR grammar and tool.

Because the generation is deterministic, the generated files can be created on-demand, and do not need to be version controlled. Because of this, this README is the only file tracked in this directory.

However, this requires that the development system building VSL requires a Java runtime.
