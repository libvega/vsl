﻿/*
 * Microsoft Public License (Ms-PL) - Copyright (c) 2022 Vega Authors
 * This file is subject to the terms and conditions of the Microsoft Public License, the text of which can be found in
 * the 'LICENSE' file at the root of this repository, or online at <https://opensource.org/licenses/MS-PL>.
 */

using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.IO;
using System.Linq;

namespace VSL;


/// <summary>
/// Represents a compiled shader program with reflection information, subclasses based on the shader kind.
/// </summary>
#if VSL_API_INTERNAL
internal
#else
public
#endif
abstract partial class ShaderProgram
{
	/// <summary>The maximum number of resource bindings supported in a shader program.</summary>
	public const uint MAX_BINDINGS = 16;
	/// <summary>The maximum size (in bytes) that a push constant struct type can be.</summary>
	public const uint MAX_PUSH_CONSTANT_SIZE = 64;
	/// <summary>The maximum length of any identifier name in a shader.</summary>
	public const uint MAX_NAME_LENGTH = 32;
	/// <summary>The maximum number of buffer resource bindings supported in a shader.</summary>
	public const uint MAX_BUFFER_BINDINGS = 4;
	/// <summary>The maximum number of texel resource bindings supported in a shader.</summary>
	public const uint MAX_TEXEL_BINDINGS = 10;

	#region Fields
	/// <summary>The kind of the shader.</summary>
	public readonly ShaderKind Kind;
	/// <summary>The mask of stages present in the shader.</summary>
	public readonly ShaderStageMask StageMask;

	/// <summary>Information about the shader uniform, or <c>null</c> if the shader has no uniform.</summary>
	public readonly Uniform? UniformInfo = null;
	/// <summary>Information about the shader push constant, or <c>null</c> if the shader has no push constant.</summary>
	public readonly PushConstant? PushConstantInfo = null;

	/// <summary>A list of the resource bindings in the shader, ordered by their slot/index.</summary>
	public IReadOnlyList<Binding> Bindings => _bindings;
	protected readonly List<Binding> _bindings = new();

	/// <summary>The map of shader stages to their SPIR-V bytecode.</summary>
	public IReadOnlyDictionary<ShaderStage, uint[]> Bytecode => _bytecode;
	protected readonly Dictionary<ShaderStage, uint[]> _bytecode = new();
	#endregion // Fields

	private protected ShaderProgram(
		ShaderKind kind, 
		Uniform? uniform, 
		PushConstant? pc, 
		List<Binding> bindings,
		Dictionary<ShaderStage, uint[]> bytecode
	)
	{
		Kind = kind;
		StageMask = bytecode.Keys.Aggregate(ShaderStageMask.None, (mask, stage) => mask | stage);

		UniformInfo = uniform;
		PushConstantInfo = pc;

		_bindings = bindings;
		_bindings.Sort((l, r) => l.Slot.CompareTo(r.Slot));

		_bytecode = bytecode;
	}

	/// <summary>Attempts to save the shader program to a Vega Bytecode (.vbc) file.</summary>
	/// <param name="path">The path to save the program to.</param>
	/// <param name="error">The error that occured, if any.</param>
	/// <returns>If the file write was a success.</returns>
	public bool TrySaveToFile(string path, [NotNullWhen(false)] out string? error)
	{
		// Wrap in a try to handle any IO exceptions at this level
		try {
			// Write using the most recent writer version
			using var writer = new VBCWriter();
			writer.WriteShader(this, path);
		}
		catch (Exception ex) {
			error = $"Failed to write file:  {ex.Message}";
			return false;
		}

		error = null;
		return true;
	}

	/// <summary>Attempts to load a shader program from the given file path.</summary>
	/// <param name="path">The path to the shader to load.</param>
	/// <param name="program">The loaded program, or <c>null</c> if the load failed.</param>
	/// <param name="fileVersion">The file version of the loaded program.</param>
	/// <param name="error">The load error, or <c>null</c> if the load succeeded.</param>
	/// <returns>If the load was successful.</returns>
	public static bool TryLoadFromFile(string path, [NotNullWhen(true)] out ShaderProgram? program, 
		out byte fileVersion, [NotNullWhen(false)] out string? error)
	{
		// Wrap in a type to handle IO exceptions
		try {
			using var stream = File.Open(path, FileMode.Open, FileAccess.Read, FileShare.Read);
			(program, fileVersion) = VBCReader.LoadShader(stream);
			error = null;
			return true;
		}
		catch (Exception ex) {
			program = null;
			fileVersion = default;
			error = $"Failed to load file:  {ex.Message}";
			return false;
		}
	}


#region Reflection
	/// <summary>Provides information about a shader uniform block.</summary>
	/// <param name="Size">The size of the uniform data, in bytes.</param>
	public sealed record Uniform(uint Size);

	/// <summary>Provides information about a shader push constant.</summary>
	/// <param name="Size">The size of the push constant data, in bytes.</param>
	public sealed record PushConstant(uint Size);

	/// <summary>Provides information about a shader resource binding.</summary>
	/// <param name="Name">The name of the resource binding.</param>
	/// <param name="Type">The type of the resource binding.</param>
	/// <param name="Slot">The slot/index used by the binding.</param>
	public sealed record Binding(string Name, ShaderType.Binding Type, uint Slot);
#endregion // Reflection
}
