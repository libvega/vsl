﻿/*
 * Microsoft Public License (Ms-PL) - Copyright (c) 2022 Vega Authors
 * This file is subject to the terms and conditions of the Microsoft Public License, the text of which can be found in
 * the 'LICENSE' file at the root of this repository, or online at <https://opensource.org/licenses/MS-PL>.
 */

using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace VSL;


/// <summary>
/// Manages the compilation of VSL shader files, supporting resource sharing and re-use across tasks.
/// </summary>
/// <remarks>
/// A single <see cref="ShaderCompiler"/> instance can, and should, be used for multiple input tasks since certain
/// runtime resources can be shared between compile tasks.
/// </remarks>
#if VSL_API_INTERNAL
internal
#else
public
#endif
sealed class ShaderCompiler
{
	/// <summary>Callback type for receiving the intermediate GLSL source generated during compilation.</summary>
	/// <param name="compiler">The compiler generating the source.</param>
	/// <param name="fileName">The file name (if specified) being compiled.</param>
	/// <param name="stage">The stage that the GLSL source belongs to.</param>
	/// <param name="glsl">The generated GLSL source code.</param>
	public delegate void GLSLEmitCallback(ShaderCompiler compiler, string? fileName, ShaderStage stage, string glsl);
	/// <summary>Callback type for receiving the intermediate SPIR-V bytecode generated during compilation.</summary>
	/// <param name="compiler">The compiler generating the source.</param>
	/// <param name="fileName">The file name (if specified) being compiled.</param>
	/// <param name="stage">The stage that the SPIR-V bytecode belongs to.</param>
	/// <param name="spirv">The generated SPIR-V bytecode.</param>
	public delegate void SPIRVEmitCallback(ShaderCompiler compiler, string? fileName, ShaderStage stage, 
		in ReadOnlySpan<uint> spirv);

	#region Fields
	/// <summary>If non-<c>null</c>, this will be called when intermediate GLSL is generated.</summary>
	public GLSLEmitCallback? GLSLEmit = null;
	/// <summary>If non-<c>null</c>, this will be called when intermediate SPIR-V is generated.</summary>
	public SPIRVEmitCallback? SPIRVEmit = null;
	#endregion // Fields

	/// <summary>Creates a new compiler instance with a blank shared state and default settings.</summary>
	public ShaderCompiler()
	{
		if (!ShaderC.IsLoaded) {
			throw new PlatformNotSupportedException("ShaderC install not found, cannot compile shaders");
		}
	}

	/// <summary>Loads and compiles the VSL source file at the given path.</summary>
	/// <param name="path">The path to the VSL source file.</param>
	/// <returns>The result of the compilation.</returns>
	public CompileResult CompileFile(string path)
	{
		// Load the file contents
		string source;
		string fileName;
		try {
			var info = new FileInfo(path);
			using (var reader = info.OpenText()) {
				source = reader.ReadToEnd();
			}
			fileName = info.Name;
		}
		catch (Exception ex) {
			return new CompileResult.Failure($"Failed to read file '{path}':  {ex.Message}");
		}

		// Compile
		return compile(source, fileName);
	}

	/// <summary>Compiles the VSL source.</summary>
	/// <param name="source">The raw VSL source to compile.</param>
	/// <returns>The result of the compilation.</returns>
	public CompileResult CompileSource(string source) => compile(source, null);

	// Performs the actual compilation
	private CompileResult compile(string source, string? fileName)
	{
#if !DEBUG
		try {
#endif // !DEBUG

		// Perform the parsing and generation
		var parser = new ShaderParser(this, source);
		if (!parser.Parse()) {
			return parser.Error!.ToFailure();
		}

		// Load the generated GLSL (and emit if callback is present)
		var sourceMap = new Dictionary<ShaderStage, string>();
		foreach (var stage in parser.ShaderInfo.Stages.EnumerateStages()) {
			var fullSource = parser.Generator.GetFullSource(stage);
			sourceMap.Add(stage, fullSource);
			GLSLEmit?.Invoke(this, fileName, stage, fullSource);
		}

		// Perform the SPIR-V compilation (and emit if callback is present)
		var bytecodeMap = new Dictionary<ShaderStage, uint[]>();
		using (var spvcomp = new SpirvCompiler(fileName)) {
			foreach ((var stage, var glsl) in sourceMap) {
				var spirv = spvcomp.CompileGLSL(glsl, stage);
				bytecodeMap.Add(stage, spirv);
				SPIRVEmit?.Invoke(this, fileName, stage, spirv.AsSpan());
			}
		}

		// Create the program
		var si = parser.ShaderInfo;
		ShaderProgram.Graphics shader = new(
			si.UniformInfo?.ToAPIType(),
			si.PushConstantInfo?.ToAPIType(),
			si.Bindings.Select(bind => bind.ToAPIType()).ToList(),
			si.Inputs.Select(i => i.ToAPIType()).ToList(),
			si.Outputs.Select(o => o.ToAPIType()).ToList(),
			si.PixelInputs.Select(i => i.ToAPIType()).ToList(),
			bytecodeMap
		);

		return new CompileResult.Success(shader);

#if !DEBUG
		}
		catch (Exception ex) {
			return new CompileResult.Failure($"Unhandled exception ({ex.GetType().Name}):  {ex.Message}");
		}
#endif // !DEBUG
	}
}
